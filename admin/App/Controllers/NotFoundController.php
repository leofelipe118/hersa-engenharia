<?php
/**
 * Created by PhpStorm.
 * User: cleversonr
 * Date: 29/05/2017
 * Time: 14:36
 */

namespace App\Controllers;


use CRO\Controller\Action;

class NotFoundController extends Action
{
    public function index()
    {
        $this->render("notFound");
    }

}