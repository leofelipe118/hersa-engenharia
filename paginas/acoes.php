﻿<?php
session_start();
include("../integracao.php");

$protocolo = $_SERVER['SERVER_PORT'] == "443" ? "https" : "http";
$linkcanonical = $protocolo . "://".$_SERVER['HTTP_HOST'] . @array_shift( @explode( "index.php", $_SERVER['SCRIPT_NAME'] ) );
$linkAnexo = str_replace("paginas/acoes.php","anexos/",$linkcanonical);
$linkApresentacao = str_replace("paginas/acoes.php","imagens/apresentacao_comercial.pdf",$linkcanonical);

$define['acoes-obrigado'] = 'Obrigado pela mensagem!';
$define['acoes-ola'] = 'Olá';
$define['acoes-recebemos'] = 'Recebemos a sua mensagem e em breve vamos retornar seu contato.<br>Segue abaixo uma cópia das informações que você enviou para nós.';
$define['acoes-duvida'] = 'Tem alguma outra dúvida e quer falar com a gente? <br>É só responder a este e-mail, ou acesse nosso site';

$op = @$_GET['op'];

$corbarraemail = "#A60000";
$linkface = "";
$linkinsta = "";
$linklinkedin = "";
$linksite = "http://hersa.com.br/";
$linkvisivel = "hersa.com.br";
$logosite = "logo_email.png";
$logo_svg = '<svg xmlns="http://www.w3.org/2000/svg" width="184.282" height="54.901" viewBox="0 0 184.282 54.901" style="margin: 0 auto; display: block; clear: both; width: 200px;">
	<g id="Logo_color" data-name="Logo color" transform="translate(-40.24 -46.69)">
	<g id="Grupo_24" data-name="Grupo 24" transform="translate(40.24 46.69)">
		<g id="Grupo_22" data-name="Grupo 22" transform="translate(78.301 42.397)">
		<path id="Caminho_12" data-name="Caminho 12" d="M252.553,168.818a.513.513,0,0,1-.5.409l-3.536.569a2.2,2.2,0,0,0-.011.569,1.182,1.182,0,0,0,1.285,1.164,2.246,2.246,0,0,0,1.592-.868.591.591,0,0,1,.44-.208.336.336,0,0,1,.33.322,2.834,2.834,0,0,1-2.85,2.168,2.326,2.326,0,0,1-2.471-2.5,4.7,4.7,0,0,1,1.126-3.142,3.289,3.289,0,0,1,2.509-1.088,2.037,2.037,0,0,1,2.179,2.016,2.334,2.334,0,0,1-.091.591Zm-2.376-1.649c-.857,0-1.383.9-1.581,1.732l2.339-.383a2.816,2.816,0,0,0,.049-.421C250.988,167.578,250.764,167.169,250.177,167.169Z" transform="translate(-246.83 -163.307)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_13" data-name="Caminho 13" d="M281.73,168.233l-.917,4.309a.3.3,0,0,1-.281.224h-1.273a.212.212,0,0,1-.231-.235.467.467,0,0,1,.011-.125l.906-4.234a1.482,1.482,0,0,0,.023-.311c0-.409-.246-.7-.845-.7-.8,0-.978.36-1.054.731l-.993,4.654a.283.283,0,0,1-.28.224h-1.273a.213.213,0,0,1-.231-.235.467.467,0,0,1,.011-.125l1.065-5.052a.951.951,0,0,1,.686-.792,6.149,6.149,0,0,1,2.24-.371c1.554,0,2.471.6,2.471,1.622A1.95,1.95,0,0,1,281.73,168.233Z" transform="translate(-264.503 -163.294)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_14" data-name="Caminho 14" d="M311.66,167.546l-1,4.779c-.489,2.3-1.565,3.479-3.635,3.479-1.615,0-2.471-.83-2.471-1.562a.806.806,0,0,1,.834-.8.86.86,0,0,1,.785.508,1.088,1.088,0,0,0,1.065.732c.61,0,1.3-.682,1.516-1.781l.121-.618a2.661,2.661,0,0,1-1.69.618,1.889,1.889,0,0,1-1.861-2.1,5.017,5.017,0,0,1,1.345-3.491,3.751,3.751,0,0,1,2.74-1.1,3.925,3.925,0,0,1,1.971.421.63.63,0,0,1,.318.58,1.507,1.507,0,0,1-.034.345Zm-2.448-.383a1.686,1.686,0,0,0-1.273.8,4.814,4.814,0,0,0-.758,2.562c0,.8.379,1.186.917,1.186a1.67,1.67,0,0,0,.966-.36l.758-3.589a1.429,1.429,0,0,0,.023-.186C309.849,167.348,309.689,167.163,309.212,167.163Z" transform="translate(-282.673 -163.3)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_15" data-name="Caminho 15" d="M343.153,168.818a.513.513,0,0,1-.5.409l-3.536.569a2.206,2.206,0,0,0-.011.569,1.182,1.182,0,0,0,1.285,1.164,2.246,2.246,0,0,0,1.592-.868.591.591,0,0,1,.44-.208.336.336,0,0,1,.33.322,2.834,2.834,0,0,1-2.85,2.168,2.324,2.324,0,0,1-2.471-2.5,4.7,4.7,0,0,1,1.126-3.142,3.289,3.289,0,0,1,2.509-1.088,2.015,2.015,0,0,1,2.088,2.608Zm-2.373-1.649c-.857,0-1.383.9-1.581,1.732l2.339-.383a2.824,2.824,0,0,0,.049-.421C341.588,167.578,341.368,167.169,340.781,167.169Z" transform="translate(-303.091 -163.307)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_16" data-name="Caminho 16" d="M372.34,168.233l-.917,4.309a.3.3,0,0,1-.28.224h-1.274a.213.213,0,0,1-.231-.235.466.466,0,0,1,.011-.125l.906-4.234a1.482,1.482,0,0,0,.023-.311c0-.409-.246-.7-.845-.7-.8,0-.978.36-1.054.731l-.993,4.654a.28.28,0,0,1-.28.224h-1.274a.212.212,0,0,1-.231-.235.467.467,0,0,1,.011-.125l1.065-5.052a.951.951,0,0,1,.686-.792,6.148,6.148,0,0,1,2.24-.371c1.554,0,2.471.6,2.471,1.622A2.587,2.587,0,0,1,372.34,168.233Z" transform="translate(-320.77 -163.294)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_17" data-name="Caminho 17" d="M402.563,163.439l-.917,4.359a.3.3,0,0,1-.28.224h-1.274a.212.212,0,0,1-.231-.235.467.467,0,0,1,.011-.125l.857-4.1a1.5,1.5,0,0,0,.038-.3.688.688,0,0,0-.769-.716,1.631,1.631,0,0,0-1.065.394l-1.027,4.851a.28.28,0,0,1-.28.224h-1.273a.212.212,0,0,1-.231-.235.468.468,0,0,1,.011-.125L398,158.774a.277.277,0,0,1,.28-.224h1.285a.212.212,0,0,1,.231.235.483.483,0,0,1-.023.125l-.648,3.157a2.821,2.821,0,0,1,1.751-.618A1.581,1.581,0,0,1,402.605,163,2.519,2.519,0,0,1,402.563,163.439Z" transform="translate(-339.536 -158.55)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_18" data-name="Caminho 18" d="M433.821,167.539l-.857,4.048a1.175,1.175,0,0,1-.66.929,4.727,4.727,0,0,1-2.054.409c-1.8,0-2.74-1.016-2.74-2.426a4.575,4.575,0,0,1,1.471-3.332,3.81,3.81,0,0,1,2.619-.978,3.578,3.578,0,0,1,1.944.432.611.611,0,0,1,.318.569c-.008.1-.019.212-.042.349Zm-2.448-.383a1.568,1.568,0,0,0-1.186.682,4.946,4.946,0,0,0-.857,2.649c0,.94.515,1.327,1.088,1.327.428,0,.709-.148.8-.383l.769-3.665a1.429,1.429,0,0,0,.023-.186C432.009,167.342,431.861,167.156,431.372,167.156Z" transform="translate(-359.029 -163.294)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_19" data-name="Caminho 19" d="M461.731,168.024c-.769,0-.5-.853-1.164-.853a.7.7,0,0,0-.735.531l-1.027,4.84a.283.283,0,0,1-.281.224h-1.273a.212.212,0,0,1-.231-.235.466.466,0,0,1,.011-.125l1.065-5.052a.935.935,0,0,1,.686-.792,5.861,5.861,0,0,1,2.069-.371c1.164,0,1.675.5,1.675.978A.777.777,0,0,1,461.731,168.024Z" transform="translate(-377.354 -163.294)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_20" data-name="Caminho 20" d="M487.039,163l-1.224,5.829a.283.283,0,0,1-.28.224h-1.274a.213.213,0,0,1-.231-.235.465.465,0,0,1,.011-.125l1.236-5.829a.277.277,0,0,1,.28-.224h1.273a.212.212,0,0,1,.231.235.482.482,0,0,1-.023.125Zm-.587-1.239c-.527,0-.868-.322-.868-.682,0-.481.455-.868,1.114-.868.527,0,.857.311.857.667A.985.985,0,0,1,486.452,161.76Z" transform="translate(-394.127 -159.581)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_21" data-name="Caminho 21" d="M511.691,167.539l-.857,4.048a1.175,1.175,0,0,1-.659.929,4.727,4.727,0,0,1-2.054.409c-1.8,0-2.74-1.016-2.74-2.426a4.576,4.576,0,0,1,1.471-3.332,3.811,3.811,0,0,1,2.619-.978,3.578,3.578,0,0,1,1.944.432.611.611,0,0,1,.318.569c-.008.1-.019.212-.042.349Zm-2.448-.383a1.568,1.568,0,0,0-1.186.682,4.947,4.947,0,0,0-.857,2.649c0,.94.515,1.327,1.088,1.327.428,0,.709-.148.8-.383l.769-3.665a1.436,1.436,0,0,0,.023-.186C509.879,167.342,509.731,167.156,509.242,167.156Z" transform="translate(-407.385 -163.294)" fill="#848688" fill-rule="evenodd"/>
		</g>
		<g id="Grupo_23" data-name="Grupo 23">
		<path id="Caminho_22" data-name="Caminho 22" d="M128.28,65.387l12.837.008V46.69H128.28Z" transform="translate(-94.911 -46.69)" fill="#848688" fill-rule="evenodd"/>
		<path id="Caminho_23" data-name="Caminho 23" d="M69.906,98.557,46.073,75.824c-.045-.042-.091-.083-.136-.129l-.224-.216.023,0a4.495,4.495,0,0,1,.106-5.905l-.019,0L69.781,46.7H44.3v0c-.015,0-.027,0-.042,0a4.015,4.015,0,0,0-4.014,3.987h0V94.65h0a4.011,4.011,0,0,0,4.01,3.911c.015,0,.027,0,.042,0h25.61Z" transform="translate(-40.24 -46.696)" fill="#848688" fill-rule="evenodd"/>
		<rect id="Retângulo_13" data-name="Retângulo 13" width="12.834" height="18.015" transform="translate(33.373 33.843)" fill="#848688"/>
		</g>
	</g>
	<g id="Grupo_26" data-name="Grupo 26" transform="translate(46.221 46.698)">
		<g id="Grupo_25" data-name="Grupo 25" transform="translate(74.655 12.432)">
		<path id="Caminho_24" data-name="Caminho 24" d="M278.361,82.133,273.207,106.4a.841.841,0,0,1-.845.663H268.09a.64.64,0,0,1-.7-.7,1.4,1.4,0,0,1,.072-.368l2.21-10.5h-8.577l-2.32,10.9a.8.8,0,0,1-.811.663h-4.309a.613.613,0,0,1-.663-.7,1.215,1.215,0,0,1,.038-.368l5.155-24.265a.841.841,0,0,1,.845-.663H263.3a.64.64,0,0,1,.7.7,1.214,1.214,0,0,1-.038.368L261.9,91.813h8.577l2.134-10.089a.841.841,0,0,1,.845-.663h4.272a.64.64,0,0,1,.7.7A1.071,1.071,0,0,1,278.361,82.133Z" transform="translate(-252.99 -80.473)" fill="#d83135" fill-rule="evenodd"/>
		<path id="Caminho_25" data-name="Caminho 25" d="M335.343,82.145l-.406,2.024a.841.841,0,0,1-.845.663h-8.247l-1.474,7h6.773a.637.637,0,0,1,.7.7,1.4,1.4,0,0,1-.072.368l-.406,1.952a.88.88,0,0,1-.845.663H323.6l-1.656,7.694H330a.64.64,0,0,1,.7.7,1.473,1.473,0,0,1-.038.406l-.478,2.1a.8.8,0,0,1-.811.663H316.161a.64.64,0,0,1-.7-.7,1.4,1.4,0,0,1,.072-.368l5.155-24.265a.841.841,0,0,1,.845-.663h13.182a.64.64,0,0,1,.7.7A1.288,1.288,0,0,1,335.343,82.145Z" transform="translate(-291.783 -80.485)" fill="#d83135" fill-rule="evenodd"/>
		<path id="Caminho_26" data-name="Caminho 26" d="M378.437,94.617v.072a6.03,6.03,0,0,1,2.248,3.46l1.914,7a1.7,1.7,0,0,1,.11.587.786.786,0,0,1-.883.773h-4.309a.813.813,0,0,1-.921-.7l-1.618-7.546c-.258-1.141-.921-2.615-2.282-2.615h-1.141c-.368,1.656-2.172,10.2-2.172,10.2a.8.8,0,0,1-.811.663h-4.124a.637.637,0,0,1-.7-.7,1.4,1.4,0,0,1,.072-.368l5.007-23.673c.11-.515.368-.773.993-.959a30.611,30.611,0,0,1,6.515-.625c6.371,0,9.388,2.725,9.388,6.186,0,4.533-3.351,7.038-7.288,8.251Zm-2.43-11.155a9.474,9.474,0,0,0-1.876.148L372.141,93a14.333,14.333,0,0,0,1.508.072c4.052,0,6.223-2.945,6.223-6.368C379.873,84.565,378.509,83.462,376.007,83.462Z" transform="translate(-321.77 -79.926)" fill="#d83135" fill-rule="evenodd"/>
		<path id="Caminho_27" data-name="Caminho 27" d="M435.625,87.39a2.352,2.352,0,0,1-2.32-1.732,3.053,3.053,0,0,0-3.055-2.649,3.742,3.742,0,0,0-3.794,3.828c0,4.344,9.756,3.938,9.756,10.749,0,4.491-3.165,9.093-10.715,9.093-5.083,0-8.028-3.5-8.028-6a2.631,2.631,0,0,1,2.835-2.907,2.37,2.37,0,0,1,2.467,1.8,3.674,3.674,0,0,0,3.46,3.279,3.869,3.869,0,0,0,3.9-4.014c0-4.639-9.5-4.344-9.5-10.715,0-4.491,3.423-8.615,10.124-8.615,5.374,0,7.584,3.279,7.584,5.227A2.423,2.423,0,0,1,435.625,87.39Z" transform="translate(-355.129 -79.51)" fill="#d83135" fill-rule="evenodd"/>
		<path id="Caminho_28" data-name="Caminho 28" d="M487.23,107.063h-4.272a.693.693,0,0,1-.811-.7l-.663-5.78h-8.172l-3.2,5.818a1.027,1.027,0,0,1-1.031.663h-3.718a.623.623,0,0,1-.7-.625,1.535,1.535,0,0,1,.3-.773l13.512-23.859c.3-.515.515-.735,1.107-.735h4.234a.726.726,0,0,1,.845.735l3.388,23.859a3.686,3.686,0,0,1,.038.625.8.8,0,0,1-.849.773Zm-6.553-16.495a36.492,36.492,0,0,1,0-5.193h-.072a46.083,46.083,0,0,1-2.21,5.193l-3.536,6.7h6.515Z" transform="translate(-384.433 -80.479)" fill="#d83135" fill-rule="evenodd"/>
		</g>
		<path id="Caminho_29" data-name="Caminho 29" d="M124.327,72.658a2.941,2.941,0,0,0-.864-2.1h.015l-24.8-23.647a.615.615,0,0,0-.459-.2.628.628,0,0,0-.629.629.417.417,0,0,0,.008.068V66.938H81.858s.008-18.811.008-18.833a.609.609,0,0,0-1.054-.417L56.839,70.546h.015a2.883,2.883,0,0,0-.008,4.048h-.008l24,22.885,0,0a.606.606,0,0,0,1.023-.443c0-.023-.008-18.049-.008-18.049H97.6V97.889a.422.422,0,0,0-.008.064.628.628,0,0,0,.629.629.621.621,0,0,0,.428-.171l0,0,24.826-23.673h-.008A2.908,2.908,0,0,0,124.327,72.658Z" transform="translate(-56.02 -46.71)" fill="#d83135" fill-rule="evenodd"/>
	</g>
	</g>
	</svg>';
// $email_de_envio = "contato@hersa.com.br"; 
$email_de_envio = "guilherme.figueiredo@benettoncomunicacao.com.br"; 

$caminho_img = "http://".$_SERVER['HTTP_HOST'] . @array_shift(@explode("paginas/",$_SERVER['PHP_SELF'])) . "imagens/".@$logosite;
$caminho_redes = "http://".$_SERVER['HTTP_HOST'] . @array_shift(@explode("paginas/",$_SERVER['PHP_SELF']));
$pasta_img = "http://".$_SERVER['HTTP_HOST'] . @array_shift(@explode("paginas/",$_SERVER['PHP_SELF'])) . "imagens/";

$nomecliente = "Hersa Engenharia";


$form = retorna_form(@$_POST);
// $form = @$_POST;

// print_r($form);
// echo filtro( "string", @$form['name'] );
// mostra_array($form);
// mostra_array($_FILES);
// die;




/*
@$_POST[ 'g-recaptcha-response' ] = true;
if (@$_POST[ 'g-recaptcha-response' ])
{

	$campos = array(
		"secret" => "6Lf9pnIUAAAAADSWfcooq_lPBV3uzTFr8dx-Vvs_",
		"response" => $_POST[ 'g-recaptcha-response' ]
	);

	foreach($campos as $key=>$value) { $campos_string .= $key.'='.$value.'&'; }
	rtrim($campos_string, '&');
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept-Encoding: gzip,deflate'));
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch,CURLOPT_POST, count($campos));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $campos_string);
	$result = curl_exec($ch);
	curl_close($ch);

	$output= json_decode($result);

	unset( $_POST[ 'g-recaptcha-response' ] );
	unset( $form[ 'g-recaptcha-response' ] );
	
	
	$output->success = true;
	if($output->success == true)
	{
	*/
		if($op == 'contato')
		{
			
		    $sql = "INSERT INTO msgcontato SET 
		        nome = '". filtro( "string", @$form['nome'] ) ."', 
		        telefone = '". filtro( "string", @$form['telefone'] ) ."',
		        assunto = 'contato',
				mensagem = '". filtro( "string", @$form['mensagem'] ) ."'";
			// echo $sql;
		    
		    $db->query($sql);
		    $outros_dados = "";
		    
		    //Create a new PHPMailer instance
 
    
		    // require '../phpmailer/PHPMailerAutoload.php';
			// require '../phpmailer/class.phpmailer.php';


		    // $mail2 = new PHPMailer();

		    // //Set who the message is to be sent from
		    // $mail2->setFrom($email_de_envio, utf8_decode($nomecliente) );
		    // //Set an alternative reply-to address
		    // $mail2->addReplyTo($form['email'],$form['name']);
		    // //Set who the message is to be sent to
		    // // $mail2->addAddress("guilherme.figueiredo@benettoncomunicacao.com.br");
		    // $mail2->addAddress("fpires@confidenceitservices.com");
			// $mail2->addBcc("guilherme.figueiredo@benettoncomunicacao.com.br");
			// //$mail2->addBcc("");
	

		    // //Set the subject line
		    // $mail2->Subject = utf8_decode("Formulário Fale Conosco - ".$nomecliente);
 	
		    $infos = "";
		    if(@$form['nome'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Nome</span><br>'
				.@$form['nome'].'</p>';
			}
			if(@$form['email'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">E-mail</span><br>'
				.@$form['email'].'</p>';
			}
			if(@$form['telefone'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Telefone</span><br>'
				.@$form['telefone'].'</p>';
			}
			if(@$form['mensagem'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Mensagem</span><br>'
				.@str_replace("\n","<br>",$form['mensagem']).'</p>';
			}

			$mensagem2 = '
				<body bgcolor="#FFFFFF">
						<style>
							body{
								background: #FFFFFF;
							}
							p{
								line-height: 27px;
							}
						</style>
						<div align="center" style="text-align:center;margin-left:auto;margin-right:auto; width: 550px;">
								<table id="Tabela_01" width="550" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" border="0">
									<tr>
										<td style="border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 0 30px 0;">
											'.$logo_svg.'
										</td>
									</tr>
									<tr>
										<td valign="top" style="background: #FFF; display: block; font-family: Arial; color: #6c6d69; font-size: 19px; text-align:left; border-left: 1px solid #DDD; border-right: 1px solid #DDD; border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 30px 30px 30px;">
											<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">';
											$mensagem2 .= 'Olá!<br>Uma nova mensagem acabou de chegar!</p><br>';
											$mensagem2 .= $infos;
											$mensagem2 .= '</td>
										</tr>
								</table>
							</div>
				</body>';

			// echo $mensagem2;
			
			
			// $mail2->msgHTML(utf8_decode($mensagem2));
	

			
			// $mail = new PHPMailer();


			// //Set who the message is to be sent from
			// $mail->setFrom($email_de_envio, utf8_decode($nomecliente) );
			// //Set an alternative reply-to address
			// //Set who the message is to be sent to
			// $mail->addAddress($form['email']);
			// //Set the subject line
			// $mail->Subject = 'Obrigado pela mensagem! - '.$nomecliente;
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			
			$mensagem = '
					<body bgcolor="#FFFFFF">
						<style>
							body{
								background: #FFFFFF;
							}
							p{
								line-height: 27px;
							}
						</style>
						<div align="center" style="text-align:center;margin-left:auto;margin-right:auto; width: 550px;">
								<table id="Tabela_01" width="550" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" border="0">
									<tr>
										<td style="border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 0 30px 0;">
											'.$logo_svg.'
										</td>
									</tr>
									<tr>
										<td valign="top" style="background: #FFF; display: block; font-family: Arial; color: #6c6d69; font-size: 19px; text-align:left; border-left: 1px solid #DDD; border-right: 1px solid #DDD; border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 30px 30px 30px;">
											<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
												' . $define['acoes-ola'] . ' <strong style="color: '.$corbarraemail.';">'.@$form['name'].'</strong>!<br> 
												' . $define['acoes-recebemos'] . '</p><br>';

											$mensagem .= $infos;


									$mensagem .= '</td>
										</tr>
										<tr>
											<td colspan="2" style="text-align: center; color: #969696; font-family: Arial; font-size: 14px; line-height:20px; padding: 30px 60px 60px 60px;">
												<div style="text-align: center;">
												' . $define['acoes-duvida'] . ' <br><a href="'.$linksite.'" target="_blank" style="color: #'.$corbarraemail.';">'.$linkvisivel.'</a>
												</div
												>
											</td>
										</tr>
								</table>
							</div>
					</body>';
					
			// echo $mensagem;

			// die;
			
			// $mail->msgHTML(utf8_decode($mensagem));
			// $mail->send();

				
			//send the message, check for errors
			if (!$mail2->send()) {
				echo '<meta http-equiv="refresh" content="0; url=../pt/contato/emailerro">';
			} else {
				echo '<meta http-equiv="refresh" content="0; url=../pt/contato/emailok">';
			}
			
		}
		if($op == 'carreiras')
		{
			
		    $nomearquivo = "";
		    if(@$_FILES['anexo']['name'])
			{
				$uploaddir = '../anexos/';
				$ext = strtolower( @array_pop( @explode( ".", $_FILES['anexo']['name'] ) ) );
				$gravaArquivo = "anexo-de-" . forma_url($form['nome']) . "-" . date("dmYHis") . "." . $ext;
				$uploadfile = $uploaddir . $gravaArquivo;
				if (move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
					$nomearquivo = $gravaArquivo;
			}

			$sql = "INSERT INTO msgcontato SET 
		        nome = '". filtro( "string", @$form['nome'] ) ."', 
		        email = '". filtro( "string", @$form['email'] ) ."', 
		        telefone = '". filtro( "string", @$form['telefone'] ) ."',
		        assunto = '".$op."',
				mensagem = '". filtro( "string", @$form['mensagem'] ) ."'";
				if($nomearquivo)
					$sql .= ", curriculo = '{$nomearquivo}'";
			// echo $sql;
		    
		    $db->query($sql);
		    $outros_dados = "";
		    
		    //Create a new PHPMailer instance
 
    
		    // require '../phpmailer/PHPMailerAutoload.php';
			// require '../phpmailer/class.phpmailer.php';


		    // $mail2 = new PHPMailer();

		    // //Set who the message is to be sent from
		    // $mail2->setFrom($email_de_envio, utf8_decode($nomecliente) );
		    // //Set an alternative reply-to address
		    // $mail2->addReplyTo($form['email'],$form['name']);
		    // //Set who the message is to be sent to
		    // // $mail2->addAddress("guilherme.figueiredo@benettoncomunicacao.com.br");
		    // $mail2->addAddress("fpires@confidenceitservices.com");
			// $mail2->addBcc("guilherme.figueiredo@benettoncomunicacao.com.br");
			// //$mail2->addBcc("");
	

		    // //Set the subject line
		    // $mail2->Subject = utf8_decode("Formulário Trabalhe Conosco - ".$nomecliente);
 	
		    $infos = "";
		    if(@$form['nome'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Nome</span><br>'
				.@$form['nome'].'</p>';
			}
			if(@$form['email'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">E-mail</span><br>'
				.@$form['email'].'</p>';
			}
			if(@$form['telefone'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Telefone</span><br>'
				.@$form['telefone'].'</p>';
			}
			if(@$_FILES['anexo']['name'])
			{
				$infos .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Anexo</span><br><a href="'.$linkAnexo.$nomearquivo.'">Anexo</a></p>';
				$infos2 .= '<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
				<span style="text-transform: uppercase; font-size:13px; color: #b3b5a7;">Anexo</span><br><a href="'.$linkAnexo.$nomearquivo.'">'
				.$nomearquivo.'</a></p>';
			}

			$mensagem2 = '
				<body bgcolor="#FFFFFF">
						<style>
							body{
								background: #FFFFFF;
							}
							p{
								line-height: 27px;
							}
						</style>
						<div align="center" style="text-align:center;margin-left:auto;margin-right:auto; width: 550px;">
								<table id="Tabela_01" width="550" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" border="0">
									<tr>
										<td style="border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 0 30px 0;">
											'.$logo_svg.'
										</td>
									</tr>
									<tr>
										<td valign="top" style="background: #FFF; display: block; font-family: Arial; color: #6c6d69; font-size: 19px; text-align:left; border-left: 1px solid #DDD; border-right: 1px solid #DDD; border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 30px 30px 30px;">
											<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">';
											$mensagem2 .= 'Olá!<br>Uma nova mensagem acabou de chegar!</p><br>';
											$mensagem2 .= $infos;
											$mensagem2 .= $infos2;
											$mensagem2 .= '</td>
										</tr>
								</table>
							</div>
				</body>';

			// echo $mensagem2;
			
			
			// $mail2->msgHTML(utf8_decode($mensagem2));
	

			
			// $mail = new PHPMailer();


			// //Set who the message is to be sent from
			// $mail->setFrom($email_de_envio, utf8_decode($nomecliente) );
			// //Set an alternative reply-to address
			// //Set who the message is to be sent to
			// $mail->addAddress($form['email']);
			// //Set the subject line
			// $mail->Subject = 'Obrigado pela mensagem! - '.$nomecliente;
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			
			$mensagem = '
					<body bgcolor="#FFFFFF">
						<style>
							body{
								background: #FFFFFF;
							}
							p{
								line-height: 27px;
							}
						</style>
						<div align="center" style="text-align:center;margin-left:auto;margin-right:auto; width: 550px;">
								<table id="Tabela_01" width="550" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;" border="0">
									<tr>
										<td style="border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 0 30px 0;">
											'.$logo_svg.'
										</td>
									</tr>
									<tr>
										<td valign="top" style="background: #FFF; display: block; font-family: Arial; color: #6c6d69; font-size: 19px; text-align:left; border-left: 1px solid #DDD; border-right: 1px solid #DDD; border-bottom: 5px solid '.$corbarraemail.'; padding: 30px 30px 30px 30px;">
											<p style="font-family: Arial;font-size: 19px;color: #6c6d69; text-align:left;">
												' . $define['acoes-ola'] . ' <strong style="color: '.$corbarraemail.';">'.@$form['name'].'</strong>!<br> 
												' . $define['acoes-recebemos'] . '</p><br>';

											$mensagem .= $infos;


									$mensagem .= '</td>
										</tr>
										<tr>
											<td colspan="2" style="text-align: center; color: #969696; font-family: Arial; font-size: 14px; line-height:20px; padding: 30px 60px 60px 60px;">
												<div style="text-align: center;">
												' . $define['acoes-duvida'] . ' <br><a href="'.$linksite.'" target="_blank" style="color: #'.$corbarraemail.';">'.$linkvisivel.'</a>.
												</div
												>
											</td>
										</tr>
								</table>
							</div>
					</body>';
					
			// echo $mensagem;

			// die;
			
			$mail->msgHTML(utf8_decode($mensagem));
			$mail->send();

				
			//send the message, check for errors
			if (!$mail2->send()) {
				echo '<meta http-equiv="refresh" content="0; url=../pt/trabalhe-conosco/emailerro">';
			} else {
				echo '<meta http-equiv="refresh" content="0; url=../pt/trabalhe-conosco/emailok">';
			}
			
		}
		if($op == 'cookies')
		{
			$_SESSION['cookies'] = @$_POST['cookies'];
			echo @$_POST['cookies'];
			die;
		}
	/*			
	}
	else
		echo '<meta http-equiv="refresh" content="0; url=../'.$form['origem'].'/errotimeout">';
}
else
	echo '<meta http-equiv="refresh" content="0; url=../'.$form['origem'].'/errocaptcha">';	
*/	


