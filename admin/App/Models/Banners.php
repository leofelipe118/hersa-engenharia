<?php
/**
 * Created by PhpStorm.
 * User: cleversonr
 * Date: 27/02/2018
 * Time: 12:48
 */

namespace App\Models;


use CRO\Model\Table;

class Banners extends Table
{
    protected $table = "banners";

    public function excluiFoto($id)
    {
        $query = "UPDATE {$this->table} SET foto = NULL WHERE id = {$id}";
        return $this->db->query($query);
    }

    public function status($form)
    {
        $contador = "SELECT id FROM {$this->table}";
        $contador = $this->db->query($contador)->num_rows;
        $query = "UPDATE {$this->table} SET status = '{$form['status']}', ordem = {$contador}  WHERE id = '{$form['idfoto']}' ";
        $this->db->query($query);

    }

    public function ordem($form)    {
        $ids = @explode("|", $form['ordem']);
        foreach ($ids as $pos => $id) {
            $query = "UPDATE {$this->table} SET ordem = '{$pos}' WHERE id = '{$id}' ";
            $this->db->query($query);
        }
    }

    public function exclui($form)
    {
        $query = "DELETE FROM {$this->table} WHERE id = {$form['id']}";
        if ($this->db->query($query)) {
            @unlink("../imagens/banners/large/" . $form['foto']);
            @unlink("../imagens/banners/thumb/" . $form['foto']);
        }
    }

    public function excluiTodas($form)
    {
        $ids = explode("|", $form['ids']);
        $fotos = explode("|", $form['fotos']);

        foreach ($ids as $i => $v) {
            $query = "DELETE FROM {$this->table} WHERE id = {$v}";
            if ($this->db->query($query)) {
                @unlink("../imagens/banners/large/" . $fotos[$i]);
                @unlink("../imagens/banners/thumb/" . $fotos[$i]);
            }
        }
    }
}