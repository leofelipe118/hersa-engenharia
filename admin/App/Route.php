<?php

namespace App;


use CRO\Init\Bootstrap;

class Route extends Bootstrap
{

    protected function initRoutes()
    {
        session_start();

        define( 'DB_DATABASE', Conn::nomeDb() );
        define( 'COR_SITE', Conn::corSite() );
        define( 'BASE_SITE', Conn::base_site() );
        define( 'RAIZ_SITE', Conn::raiz_site() );
        define( 'NOME_CLIENTE', Conn::nomeCliente() );

        $routes['home'] = array('route' => "/", "controller" => "indexController", "action" => "index");
        $routes['index'] = array('route' => "/index", "controller" => "indexController", "action" => "index");
        $routes['logout'] = array('route' => "/logout", "controller" => "loginController", "action" => "logout");
        $routes['notifsh'] = array('route' => "/notificacoes/sh", "controller" => "notificaController", "action" => "sistemaHoras");
        $routes['notifmsg'] = array('route' => "/notificacoes/msg", "controller" => "notificaController", "action" => "buscaMsg");
        $routes['excluir'] = array('route' => "/acoes/excluir", "controller" => "acoesController", "action" => "excluir");
        $routes['inserir'] = array('route' => "/acoes/inserir", "controller" => "acoesController", "action" => "inserir");
        $routes['status'] = array('route' => "/acoes/status", "controller" => "acoesController", "action" => "status");
        $routes['ordem'] = array('route' => "/acoes/ordem", "controller" => "acoesController", "action" => "ordem");
        $routes['inserirMulti'] = array('route' => "/acoes/inserirMulti", "controller" => "acoesController", "action" => "inserirMulti");
        $routes['limpaTexto'] = array('route' => "/acoes/limpaTexto", "controller" => "acoesController", "action" => "limpaTexto");
        $routes['salvaImagem'] = array('route' => "/acoes/salvaImagem", "controller" => "acoesController", "action" => "salvaImagem");

        //Formulário de Contato
        $routes['mensagens'] = array('route' => "/mensagens", "controller" => "mensagensController", "action" => "listar");
        $routes['msgStatus'] = array('route' => "/mensagens/status", "controller" => "mensagensController", "action" => "status");
        $routes['msgExportar'] = array('route' => "/mensagens/exportar", "controller" => "mensagensController", "action" => "exportar");
        $routes['msgImportar'] = array('route' => "/mensagens/importar", "controller" => "mensagensController", "action" => "importar");
        $routes['msgExcel'] = array('route' => "/mensagens/excel", "controller" => "mensagensController", "action"
        => "excel");
        $routes['msgExcel'] = array('route' => "/mensagens/tudoLido", "controller" => "mensagensController", "action"
        => "tudoLido");

        //Formulário Trabalhe Conosco
        $routes['mensagensTrabalheConosco'] = array('route' => "/mensagens-trabalhe-conosco", "controller" => "mensagensTrabalheConoscoController", "action" => "listar");
        $routes['msgStatusTrabalheConosco'] = array('route' => "/mensagens-trabalhe-conosco/status", "controller" => "mensagensTrabalheConoscoController", "action" => "status");
        $routes['msgExportarTrabalheConosco'] = array('route' => "/mensagens-trabalhe-conosco/exportar", "controller" => "mensagensTrabalheConoscoController", "action" => "exportar");
        $routes['msgImportarTrabalheConosco'] = array('route' => "/mensagens-trabalhe-conosco/importar", "controller" => "mensagensTrabalheConoscoController", "action" => "importar");
        $routes['msgExcelTrabalheConosco'] = array('route' => "/mensagens-trabalhe-conosco/excel", "controller" => "mensagensTrabalheConoscoController", "action"
        => "excel");
        $routes['msgExcelTrabalheConosco'] = array('route' => "/mensagens-trabalhe-conosco/tudoLido", "controller" => "mensagensTrabalheConoscoController", "action"
        => "tudoLido");


        //Usuários
        $routes['cadastrar'] = array('route' => "/usuarios/cadastrar", "controller" => "usuariosController", "action" => "cadastrar");
        $routes['listar'] = array('route' => "/usuarios/listar", "controller" => "usuariosController", "action" => "listar");
        $routes['meus-dados'] = array('route' => "/usuarios/meus-dados", "controller" => "usuariosController", "action" => "meusDados");
        $routes['cadastro'] = array('route' => "/usuarios/cadastro", "controller" => "usuariosController", "action" => "cadastro");
        $routes['cadastroDados'] = array('route' => "/usuarios/dados", "controller" => "usuariosController", "action" => "alteraDados");

        //Parceiros
        $routes['parceirosCadastrar'] = array('route' => "/clientes/cadastrar", "controller" => "parceirosController", "action" => "cadastrar");
        $routes['parceirosCadastro'] = array('route' => "/clientes/cadastro", "controller" => "parceirosController", "action" => "cadastro");
        $routes['parceirosListar'] = array('route' => "/clientes/listar", "controller" => "parceirosController", "action" => "listar");
        $routes['parceirosCategoria'] = array('route' => "/clientes/categoria", "controller" => "parceirosController", "action" => "categoria");
        $routes['parceirosStatus'] = array('route' => "/clientes/status", "controller" => "parceirosController", "action" => "status");
        $routes['parceirosDelImg'] = array('route' => "/clientes/excImg", "controller" => "parceirosController", "action" => "excluirImg");
        $routes['parceirosListarOK'] = array('route' => "/clientes/listar/ok", "controller" => "parceirosController", "action" => "listar");
        $routes['parceirosListarERRO'] = array('route' => "/clientes/listar/erro", "controller" => "parceirosController", "action" => "listar");

        //Newsletter
        $routes['newsletter'] = array('route' => "/newsletter", "controller" => "newsletterController", "action" => "listar");
        $routes['newsStatus'] = array('route' => "/newsletter/status", "controller" => "newsletterController", "action" => "status");
        $routes['newsExportar'] = array('route' => "/newsletter/exportar", "controller" => "newsletterController", "action" => "exportar");
        $routes['newsImportar'] = array('route' => "/newsletter/importar", "controller" => "newsletterController", "action" => "importar");
        $routes['newsExcel'] = array('route' => "/newsletter/excel", "controller" => "newsletterController", "action"
        => "excel");
        $routes['newsExcel'] = array('route' => "/newsletter/tudoLido", "controller" => "newsletterController", "action"
        => "tudoLido");

        //Banners
        $routes['bannersHomeCadastrar'] = array('route' => "/bannershome/cadastrar", "controller" => "bannersController", "action" => "cadastrar");
        $routes['bannersHomeListar'] = array('route' => "/bannershome/listar", "controller" => "bannersController", "action" => "listar");
        $routes['bannersHomeListarOK'] = array('route' => "/bannershome/listar/ok", "controller" => "bannersController", "action" => "listar");
        $routes['bannersHomeListarERRO'] = array('route' => "/bannershome/listar/erro", "controller" => "bannersController", "action" => "listar");
        $routes['bannersCadastrar'] = array('route' => "/bannerscarossel/cadastrar", "controller" => "bannersController", "action" => "cadastrar");
        $routes['bannersCadastro'] = array('route' => "/banners/cadastro", "controller" => "bannersController", "action" => "cadastro");
        $routes['bannersListar'] = array('route' => "/bannerscarossel/listar", "controller" => "bannersController", "action" => "listar");
        $routes['bannersListarOK'] = array('route' => "/bannerscarossel/listar/ok", "controller" => "bannersController", "action" => "listar");
        $routes['bannersListarERRO'] = array('route' => "/bannerscarossel/listar/erro", "controller" => "bannersController", "action" => "listar");
        $routes['bannersStatus'] = array('route' => "/banners/status", "controller" => "bannersController", "action" => "status");
        $routes['bannersOrdem'] = array('route' => "/banners/ordem", "controller" => "bannersController", "action" => "ordem");
        $routes['bannersExcImg'] = array('route' => "/banners/excImg", "controller" => "bannersController", "action" => "excluirImg");
        $routes['bannersExclui'] = array('route' => "/banners/exclui", "controller" => "bannersController", "action" => "exclui");
        $routes['bannersExcluiTodas'] = array('route' => "/banners/excluiTodas", "controller" => "bannersController",
            "action" => "excluiTodas");
        $routes['bannersHome'] = array('route' => "/bannersfixo/bannerFixo", "controller" => "bannersController", "action" => "cadastrar");
        $routes['bannersCadastrarRodape'] = array('route' => "/banners/cadastrarRodape", "controller" => "bannersController", "action" => "cadastrar");
        $routes['bannersListarRodape'] = array('route' => "/banners/listarRodape", "controller" => "bannersController", "action" => "listar");
        $routes['bannersListarRodapeOK'] = array('route' => "/banners/listarRodape/ok", "controller" => "bannersController", "action" => "listar");
        $routes['bannersListarRodapeERRO'] = array('route' => "/banners/listarRodape/erro", "controller" => "bannersController", "action" => "listar");

        //Banners
        $routes['premiosCadastrar'] = array('route' => "/premios/cadastrar", "controller" => "premiosController", "action" => "cadastrar");
        $routes['premiosCadastro'] = array('route' => "/premios/cadastro", "controller" => "premiosController", "action" => "cadastro");
        $routes['premiosListar'] = array('route' => "/premios/listar", "controller" => "premiosController", "action" => "listar");
        $routes['premiosListarOK'] = array('route' => "/premios/listar/ok", "controller" => "premiosController", "action" => "listar");
        $routes['premiosListarERRO'] = array('route' => "/premios/listar/erro", "controller" => "premiosController", "action" => "listar");
        $routes['premiosStatus'] = array('route' => "/premios/status", "controller" => "premiosController", "action" => "status");
        $routes['premiosOrdem'] = array('route' => "/premios/ordem", "controller" => "premiosController", "action" => "ordem");
        $routes['premiosExcImg'] = array('route' => "/premios/excImg", "controller" => "premiosController", "action" => "excluirImg");
        $routes['premiosExclui'] = array('route' => "/premios/exclui", "controller" => "premiosController", "action" => "exclui");
        $routes['premiosExcluiTodas'] = array('route' => "/premios/excluiTodas", "controller" => "premiosController",
            "action" => "excluiTodas");

        //Noticias/Blog
        $routes['noticiasCadastrar'] = array('route' => "/noticias/cadastrar", "controller" => "noticiasController", "action" => "cadastrar");
        $routes['noticiasCadastro'] = array('route' => "/noticias/cadastro", "controller" => "noticiasController", "action" => "cadastro");
        $routes['noticiasListar'] = array('route' => "/noticias/listar", "controller" => "noticiasController", "action" => "listar");
        $routes['noticiasCategoria'] = array('route' => "/noticias/categoria", "controller" => "noticiasController", "action" => "categoria");
        $routes['noticiasStatus'] = array('route' => "/noticias/status", "controller" => "noticiasController", "action" => "status");
        $routes['noticiasDelImg'] = array('route' => "/noticias/excImg", "controller" => "noticiasController", "action" => "excluirImg");
        $routes['noticiasListarOK'] = array('route' => "/noticias/listar/ok", "controller" => "noticiasController", "action" => "listar");
        $routes['noticiasListarERRO'] = array('route' => "/noticias/listar/erro", "controller" => "noticiasController", "action" => "listar");
        $routes['noticiascheckUrl'] = array('route' => "/noticias/checkUrl", "controller" => "noticiasController", "action" => "checkUrl");
        $routes['noticiasgeraUrl'] = array('route' => "/noticias/geraUrl", "controller" => "noticiasController", "action" => "geraUrl");
        $routes['noticiasCategoria'] = array('route' => "/noticias/categorias", "controller" => "noticiasController", "action" => "categorias");
        $routes['noticiasAddCategoria'] = array('route' => "/noticias/addCategoria", "controller" => "noticiasController", "action" => "addCategoria");
        
        //Projetos
        $routes['projetosCadastrar'] = array('route' => "/projetos/cadastrar", "controller" => "projetosController", "action" => "cadastrar");
        $routes['projetosCadastro'] = array('route' => "/projetos/cadastro", "controller" => "projetosController", "action" => "cadastro");
        $routes['projetosListar'] = array('route' => "/projetos/listar", "controller" => "projetosController", "action" => "listar");
        $routes['projetosCategoria'] = array('route' => "/projetos/categoria", "controller" => "projetosController", "action" => "categoria");
        $routes['projetosStatus'] = array('route' => "/projetos/status", "controller" => "projetosController", "action" => "status");
        $routes['projetosDelImg'] = array('route' => "/projetos/excImg", "controller" => "projetosController", "action" => "excluirImg");
        $routes['projetosDelVideo'] = array('route' => "/projetos/excVideo", "controller" => "projetosController", "action" => "excluirVideo");
        $routes['projetosListarOK'] = array('route' => "/projetos/listar/ok", "controller" => "projetosController", "action" => "listar");
        $routes['projetosListarERRO'] = array('route' => "/projetos/listar/erro", "controller" => "projetosController", "action" => "listar");
        $routes['projetoscheckUrl'] = array('route' => "/projetos/checkUrl", "controller" => "projetosController", "action" => "checkUrl");
        $routes['projetosgeraUrl'] = array('route' => "/projetos/geraUrl", "controller" => "projetosController", "action" => "geraUrl");
        $routes['projetosgaleria'] = array('route' => "/projetos/galeria", "controller" => "projetosController", "action" => "galeria");
        $routes['projetosStatusGaleria'] = array('route' => "/projetos/statusGaleria", "controller" => "projetosController", "action" => "statusGaleria");
        $routes['projetosSalvaDescricao'] = array('route' => "/projetos/salvaDescricao", "controller" => "projetosController", "action" => "salvaDescricao");
        $routes['projetosCategoria'] = array('route' => "/projetos/categorias", "controller" => "projetosController", "action" => "categorias");
		$routes['projetosAddCategoria'] = array('route' => "/projetos/addCategoria", "controller" => "projetosController", "action" => "addCategoria");

		$this->setRoutes($routes);

    }

}
