<?php

namespace App;

class Conn
{

    public static function getDb()
    {
        $db = Conn::nomeDb();
		if( Conn::buscaIP() == "local" )
            return new \mysqli("50.116.87.54","root","",$db);//SERVIDOR
        else
            return new \mysqli("50.116.87.54","beora708_benetto","Benet159ton",$db);//HOSPEDAGEM - AMOSTRA
    }

    public static function nomeDb()
    {
        if( Conn::buscaIP() == "local" )
            return "beora708_hersa";//SERVIDOR
        else
            return "beora708_semanapp";//HOSPEDAGEM AMOSTRA
    }

    /* Coloquei essa chamada aqui pra facilitar quando se inicia um novo trabalho
    *
    * Esta será a cor padrão do painel administrativo
    *
    */
    public static function corSite()
    {
        return "#A60000";
    }

    public static function nomeCliente()
    {
        return "Hersa";
    }

    public static function base_site()
    {
        $protocolo = $_SERVER['SERVER_PORT'] == "443" ? "https" : "http";
        return $protocolo."://".$_SERVER['HTTP_HOST'] . @array_shift(@explode("index.php", $_SERVER['SCRIPT_NAME']));
    }

    public static function raiz_site()
    {
        $protocolo = $_SERVER['SERVER_PORT'] == "443" ? "https" : "http";
        return $protocolo."://".$_SERVER['HTTP_HOST'] . @array_shift(@explode("admin/index.php",
                $_SERVER['SCRIPT_NAME']));
    }

    public static function buscaIP()
    {
		$ip = @explode( ".", $_SERVER['SERVER_ADDR'] );
		array_pop($ip);
        $ip = @implode( ".", $ip );
        $servidor = strtolower( $_SERVER['SERVER_NAME'] );
        return $ip == "127.0.0" || $ip == "192.168.0" || $servidor == "localhost" ? "local" : "producao";
    }
}