<?php @date_default_timezone_set('America/Sao_Paulo');

if( isset( $op ) )
{
	////// CAMPOS DOS FORMUL�RIOS
	$formulario = array();
	$fomulario[$op]["nome"] = "Nome completo *";
	$fomulario[$op]["email"] = "E-mail *";
	$fomulario[$op]["telefone"] = "Telefone principal *";
	$fomulario[$op]["telefone2"] = utf8_encode("Telefone secund�rio");
	$fomulario[$op]["cpf"] = "CPF *";
	$fomulario[$op]["assunto"] = "Assunto *";
	$fomulario[$op]["mensagem"] = "Sua mensagem *";
	$fomulario[$op]["rg"] = "RG *";

	$fomulario[$op]["cateira_prof"] = "Carteira profissional";
	$fomulario[$op]["serie_cateira_prof"] = utf8_encode("S�rie carteira profissional");
	$fomulario[$op]["data_nasc"] = "Data de nascimento *";

	//// MOTORISTA OU PARCEIRO DE LOGISTICA
	$fomulario[$op]["oque_deseja"] = utf8_encode("O que voc� deseja? *");
	$fomulario[$op]["cadastro_antt"] = "Possui cadastro na ANTT?";
	$fomulario[$op]["numero_antt"] = utf8_encode("N�mero ANTT?");
	$fomulario[$op]["cep"] = "Informe seu CEP *";
	$fomulario[$op]["endereco"] = utf8_encode("Qual o seu endere�o? *");
	$fomulario[$op]["numero"] = utf8_encode("N�mero *");
	$fomulario[$op]["bairro"] = "Bairro *";
	$fomulario[$op]["complemento"] = "Complemento";
	$fomulario[$op]["cidade"] = "Cidade *";
	$fomulario[$op]["estado"] = "Estado *";
	$fomulario[$op]["regiao_atendida"] = utf8_encode("Qual regi�o atendida? *");

	$fomulario[$op]["cnpj"] = "CNPJ *";
	$fomulario[$op]["num_funcionario"] = utf8_encode("N�mero de funcion�rios *");
	$fomulario[$op]["capacidade_base"] = utf8_encode("Qual � �rea (m�) dispon�vel para armazenagem?");
	$fomulario[$op]["opera_produtos"] = utf8_encode("Voc� opera com produtos");
	$fomulario[$op]["docas"] = utf8_encode("Quantas docas para expedi��o? *");

	if(@$escolha == "entregas-fracionadas") 
	{
		$fomulario[$op]["cnh"] = "CNH *";
		$fomulario[$op]["validade_cnh"] = "Validade da CNH";
		$fomulario[$op]["categoria_cnh"] = "Categoria da CNH *";
		$fomulario[$op]["crlv"] = "CRLV";
		$fomulario[$op]["renavan"] = "Renavam";
		$fomulario[$op]["placa"] = utf8_encode("Placa do ve�culo");
		$fomulario[$op]["modelo"] = "Modelo";
		$fomulario[$op]["cor"] = "Cor";
		$fomulario[$op]["marca"] = "Marca";
		$fomulario[$op]["modelo"] = "Modelo";
		$fomulario[$op]["ano"] = "Ano";
		$fomulario[$op]["mensagem"] = utf8_encode("Coment�rios");
	}
	else if(@$escolha == "toda-logistica")
	{

		$fomulario[$op]["veiculos"]["3_4"] = "3/4";
		$fomulario[$op]["veiculos"]["carreta"] = "Carreta";
		$fomulario[$op]["veiculos"]["carro_passeio"] = "Carro de passeio";
		$fomulario[$op]["veiculos"]["fiorino"] = "Fiorino";
		$fomulario[$op]["veiculos"]["kombi"] = "Kombi";
		$fomulario[$op]["veiculos"]["moto"] = "Moto";
		$fomulario[$op]["veiculos"]["toco"] = "Toco";
		$fomulario[$op]["veiculos"]["truck"] = "Truck";
		$fomulario[$op]["veiculos"]["vuc_hr"] = "VUC HR";
		$fomulario[$op]["veiculos"]["vuc_iveco"] = "VUC Iveco";
		$fomulario[$op]["veiculos"]["vuc"] = "VUC";
		$fomulario[$op]["veiculos"]["van"] = "Van";
	}

	///////// CONTRATAR SERVI�O
	$fomulario[$op]["nome_empresa"] = "Nome da empresa *";
	$fomulario[$op]["cnpj"] = "CNPJ *";
	$fomulario[$op]["tem_ecommerce"] = utf8_encode("Voc� j� possui E-Commerce? *");
	$fomulario[$op]["site"] = "Qual seu site?";
	$fomulario[$op]["cidade"] = utf8_encode("Qual a cidade do seu neg�cio? *");
	$fomulario[$op]["estado"] = "Estado *";
	$fomulario[$op]["segmento"] = utf8_encode("Qual o segmento do seu neg�cio? *");
	$fomulario[$op]["volume_movimentado"] = "Selecione um volume esperado *";
	$fomulario[$op]["coleta_cd"] = "Deseja a coleta em seu CD?";
	$fomulario[$op]["primeiro_contato"] = utf8_encode("Este � o seu primeiro contato com a Direct?");
	$fomulario[$op]["como_conheceu"] = "Como nos conheceu? *";
	$fomulario[$op]["qual_transportadora"] = utf8_encode("Atualmente, tem opera��o com qual transportadora?");
	$fomulario[$op]["marketplace_b2w"] = utf8_encode("Voc� est� no Marketplace da B2W?");
	$fomulario[$op]["cidade"] = utf8_encode("Qual � a sua cidade? *");
	$fomulario[$op]["estado"] = "Estado *";

	///// INFORMA��ES SOBRE PEDIDO
	$fomulario[$op]["comentario"] = utf8_encode("Coment�rio");
	$fomulario[$op]["motivo_contato"] = "Motivo do contato";
	$fomulario[$op]["qual_loja"] = "Qual Loja / Empresa onde adquiriu o produto?";
	$fomulario[$op]["num_pedido"] = utf8_encode("Informe o n�mero do pedido do site da Direct ");

	///// INFORMA��ES TRABALHE CONOSCO
	$fomulario[$op]["trabalhe_empresa"] = "Empresa";
	$fomulario[$op]["funcao"] = utf8_encode("Fun��o");
	$fomulario[$op]["data_admissao"] = utf8_encode("Data de admiss�o");
	$fomulario[$op]["data_saida"] = utf8_encode("Data de sa�da");

	$fomulario[$op]["pergunta_curriculo"] = utf8_encode("Deseja enviar um arquivo do seu curr�culo?");

	$fomulario[$op]["formacao_curso"] = "Curso";
	$fomulario[$op]["formacao_instituicao"] = utf8_encode("Institui��o de ensino");
	$fomulario[$op]["formacao_data_conclusao"] = utf8_encode("Data de conclus�o");
	$fomulario[$op]["formacao_nivel"] = utf8_encode("N�vel");

	$fomulario[$op]["idioma_curso"] = "Curso";
	$fomulario[$op]["idioma_instituicao"] = utf8_encode("Institui��o de ensino");
	$fomulario[$op]["idioma_data_conclusao"] = utf8_encode("Data de conclus�o");
	$fomulario[$op]["idioma_nivel"] = utf8_encode("N�vel");

	$fomulario[$op]["certificacao_curso"] = "Curso";
	$fomulario[$op]["certificacao_instituicao"] = utf8_encode("Institui��o de ensino");
	$fomulario[$op]["certificacao_data_conclusao"] = utf8_encode("Data de conclus�o");

	$fomulario[$op]["curso"] = "Curso";
	$fomulario[$op]["curso_instituicao"] = utf8_encode("Institui��o de ensino");
	$fomulario[$op]["curso_data_conclusao"] = utf8_encode("Data de conclus�o");

	$fomulario[$op]["curriculo"] = utf8_encode("Curr�culo");

	$fomulario[$op]["servico"] = utf8_encode("Qual servi�o deseja nos prestar? *");
	$fomulario[$op]["outro_servico"] = utf8_encode("Qual servi�o?");

}

function retornaCarro($marca = "")
{
	$modelo_carros = array(
		"Volkswagen" => array(
			"AMAROK",
			"BORA",
			"FOX",
			"FOX",
			"SPACEFOX",
			"GOL",
			"VOYAGE",
			"SAVEIRO",
			"GOLF",
			"JETTA",
			"NEW BEETLE",
			"PARATI",
			"NOVO FUSCA",
			"POLO",
			"PASSAT VARIANT",
			"SANTANA",
			"TIGUAN / TOUAREG",
			"VAN",
			"KOMBI",
			"CARAVELE",
			"EUROVAN",
			"5.140",
			"8.150",
			"9.150",
			"DELIVERY",
			"WORKER",
			"VAN"
		),
		"AGRALE" => array(
			"VUC"
		),
		"PEUGEOT" => array(
			"106",
			"206",
			"207",
			"208",
			"306",
			"405",
			"307",
			"308",
			"406",
			"407",
			"408",
			"3008",
			"RCZ",
			"HOGGAR",
			"PARTNER",
			"BOXER VAN",
			"BOXER VAN"
		), 
		"RENAULT" => array(
			"CLIO",
			"SANDERO",
			"LOGAN",
			"LAGUNA",
			"MEGANE",
			"FLUENCE",
			"SCENIC",
			"SYMBOL",
			"KANGOO",
			"OROCH",
			"DUSTER",
			"MASTER"
		),
		"MERCEDES-BENS" => array(
			"CLASSE A",
			"CLASSE B",
			"CLASSE C",
			"CLASSE S",
			"CLASSE CL",
			"CLASSE CLA",
			"CLASSE CLS",
			"CLASSE GL",
			"CLASSE GLA",
			"CLASSE GLK",
			"CLASSE M",
			"CLASSE SL",
			"NOVA SPRINTER",
			"710",
			"912",
			"914",
			"915",
			"ACCELO",
			"NOVA SPRINTER"
		),
		"HONDA" => array(
			"ACCORD",
			"CIVIC",
			"NEW CIVIC",
			"CIVIC",
			"FIT",
			"NEW FIT",
			"CITY",
			"CR-V",
			"HR-V"
		),
		"FORD" => array(
			"EDGE",
			"FIESTA",
			"NEW FIESTA",
			"Sigma",
			"KA",
			"Rocam",
			"FOCUS",
			"ZETEC",
			"FUSION",
			"MONDEO",
			"COURIER",
			"ECOSPORT",
			"F250",
			"RANGER",
			"F 350",
			"CARGO"
		),
		"FIAT" => array(
			"FREEMONT",
			"FIAT 500",
			"IDEA",
			"MAREA",
			"MAREA WEEKEND",
			"PALIO",
			"PALIO WEEKEND",
			"SIENA FIRE",
			"STRADA",
			"UNO",
			"TORQ",
			"SIENA",
			"BRAVO",
			"LINEA",
			"MOBI EASY",
			"MOBI DRIVE",
			"PUNTO",
			"ATTRACTIVE",
			"UNO MILLE",
			"FIORINO",
			"STILO",
			"DOBL�",
			"DUCATO",
			"TORO",
			"FREEDOM",
			"VOLCANO",
			"DUCATO"
		),
		"TOYOTA" => array(
			"CAMRY",
			"CORONA",
			"COROLLA",
			"ETIOS",
			"PRIUS",
			"HILUX / SW4 PICK-UP",
			"LAND CRUISER PRADO",
			"RAV 4",
			"DYNA"
		),
		"HYUNDAI" => array(
			"HB 20",
			"AZERA",
			"ACCENT",
			"VERA CRUZ",
			"VELOSTER",
			"SANTA FE",
			"TUCSON",
			"SONATA",
			"ELANTRA",
			"EQUUS",
			"GENESIS",
			"HR"
		),
		"JAC" => array(
			"J2",
			"J3",
			"J5",
			"J6",
			"T6",
			"T8",
			"V260",
			"T140"
		),
		"CHEVROLET" => array(
			"AGILE",
			"ASTRA",
			"CAPTIVA",
			"CAMARO",
			"CELTA",
			"CLASSIC",
			"CORSA",
			"CRUZE",
			"COBALT",
			"MALIBU",
			"MERIVA",
			"MONTANA",
			"OMEGA",
			"PRISMA",
			"ONIX",
			"SONIC",
			"SPIN",
			"VECTRA",
			"BLAZER / S10",
			"S10",
			"TRACKER",
			"TRAILBLAZER",
			"DURAMAX"
		),
		"IVECO" => array(
			"IVECO - DAILY"
		),
		"KIA" => array(
			"SORENTO",
			"OPIRUS",
			"SPORTAGE",
			"CARNIVAL",
			"MAGENTIS",
			"PICANTO",
			"CARENS",
			"CERATO",
			"KOUP",
			"SOUL",
			"MOHAVE",
			"CADENZA",
			"OPTIMA",
			"QUORIS",
			"BONGO",
			"CERES",
			"K 3500",
			"BONGO"
		)
	);
	
	return $marca ? $modelo_carros[ $marca ] : $modelo_carros;

}

function busca_foto($id_fb, $tamanho = 250)
{
	$ch = curl_init();
	curl_setopt ($ch, CURLOPT_URL, 'https://graph.facebook.com/'.$id_fb.'?fields=picture.width('.$tamanho.').height('.$tamanho.')');
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
	$file_contents = curl_exec($ch);
	curl_close($ch);
	$json = json_decode($file_contents, true);
	$foto = $json["picture"]["data"]["url"];
	return $foto;
}

function formaNomeArquivo($string) {
	$string = iconv( "UTF-8" , "ASCII//TRANSLIT//IGNORE" , strtolower( trim( strip_tags( htmlspecialchars_decode( tira_acento( filtro( "string",$string ) ) ) ) ) ) ); 
	$string = strtr($string," ","-");
	$string = strtr($string,"_","-");
	$string = preg_replace( array( '/[ ]/' , '/[^A-Za-z0-9\-.]/' ) , array( '' , '' ) , $string );
	return $string;
}

function mostra_array($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

function removeEspaco($arquivo)
{
	$array1 = array(" ", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "-" );
	$array2 = array("_", "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "_" );
		
	return $arquivo = str_replace( $array1, $array2, $arquivo);

}

function tira_acento($palavra)
{
	$array1 = array("&rsquo;", "&aacute;", "&agrave;", "&acirc;", "&atilde;", "&auml;", "&eacute;", "&egrave;", "&ecirc;", "&euml;", "&iacute;", "&igrave;", "&icirc;", "&iuml;", "&oacute;", "&ograve;", "&ocirc;", "&otilde;", "&ouml;", "&uacute;", "&ugrave;", "&ucirc;", "&uuml;", "&ccedil;", "&Aacute;", "&Agrave;", "&Acirc;", "&Atilde;", "&Auml;", "&Eacute;", "&Egrave;", "&Ecirc;", "&Euml;", "&Iacute;", "&Igrave;", "&Icirc;", "&Iuml;", "&Oacute;", "&Ograve;", "&Ocirc;", "&Otilde;", "&Ouml;", "&Uacute;", "&Ugrave;", "&Ucirc;", "&Uuml;", "&Ccedil;", "&ordm;", "&ordf;" );
	$array2 = array("", "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "o", "a" );
		
	return $palavra = str_replace( $array1, $array2, $palavra);

}

function forma_url($string) {
	$string = iconv( "UTF-8" , "ASCII//TRANSLIT//IGNORE" , trim(strip_tags(htmlspecialchars_decode(tira_acento($string)))) );
	$string = strtr($string," ","-");
	$string = preg_replace( array( '/[ ]/' , '/[^A-Za-z0-9\-]/' ) , array( '' , '' ) , $string );
	return strtolower( $string );
}

function limpa_nome($string) {
	$string = iconv( "UTF-8" , "ASCII//TRANSLIT//IGNORE" , htmlspecialchars_decode(tira_acento($string)) );
	$string = strtr($string,"-","");
	$string = strtr($string," ","-");
	$string = preg_replace( array( '/[ ]/' , '/[^A-Za-z0-9\-]/' ) , array( '' , '' ) , $string );
	$string = strtr($string,"-"," ");
	return $string;
}

function limitaTexto($string, $word_limit) 
{   
	$tags = array("[imagem]", "[galeria]", "[video]");
	$string = str_replace($tags,"",strip_tags($string));
	$words = explode(' ', $string); 
	$ret = "";
	if(count($words) > $word_limit)
		$ret = "...";
	return implode(' ', array_slice($words, 0, $word_limit)).$ret;
}

function limpaTexto( $t )
{
	$t = str_replace("</p><p>", "<br>", $t );
	$t = str_replace("<p>", "", $t );
	$t = str_replace("</p>", "", $t );
	$t = str_replace("</div><div>", "<br>", $t );
	$t = str_replace("<div>", "", $t );
	$t = str_replace("</div>", "", $t );
	return ($t);
}

function seg_login($v)
{
    $valor = addslashes($v);
    $array2 = array("/\bDELETE\b/i","/\bUPDATE\b/i","/\bINSERT\b/i","/\bDROP\b/i","/\bSELECT\b/i","/\bSET\b/i","/\bINTO\b/i","/\bWHERE\b/i","/'/","/\bOFFSET\b/i","/\bJOIN\b/i","/\bUNION\b/i","/\bCONCAT\b/i","/\bLIMIT\b/i","/\bALERT\b/i","/\bLEFT\b/i","/\bINNER\b/i","/\bNOT\b/i","/\bLIKE\b/i","/\bTRUNCATE\b/i","/\bALTER\b/i","/\bDELIMITER\b/i","/\bAND\b/i","/\bSLEEP\b/i","/\bCONFIRM\b/i","/\bDBMS\b/i","/\bDBMS_LOCK\b/i","/\bDBMS_LOCK.SLEEP\b/i","/\bWAITFOR\b/i","/\bDELAY\b/i","/\bCENZICMARKER\b/i","/\bCENZICBENIGN\b/i","/\bNULL\b/i");
    $valor = str_replace('"',"&quot;",$valor);
    $valor = str_replace("'","&rsquo;",$valor);
    $valor = preg_replace($array2,"",$valor);
    return($valor);
}

function seguranca($valor)
{
	$valor = addslashes($valor);
	$valor = str_replace('"',"&quot;",$valor);
	$valor = str_replace("'","&rsquo;",$valor);
	return $valor;
}

function retorna_form($post){
	$names = array_keys($post);
	$form = array();
	foreach($names as $n){
		if(gettype($post[$n]) == "string")
			$form[$n] = seguranca($post[$n]);
		elseif(gettype($post[$n]) == "array")
			$form[$n] = retorna_form($post[$n]);
		else
			$form[$n] = $post[$n];
	}
	return($form);
}

function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
{
	$lmin = 'abcdefghijklmnopqrstuvwxyz';
	$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$num = '1234567890';
	$simb = '!@#$%*-';
	$retorno = '';
	$caracteres = '';
	
	$caracteres .= $lmin;
	if ($maiusculas) $caracteres .= $lmai;
	if ($numeros) $caracteres .= $num;
	if ($simbolos) $caracteres .= $simb;
	
	$len = strlen($caracteres);
	for($n = 1; $n <= $tamanho; $n++)
	{
		$rand = mt_rand(1, $len);
		$retorno .= $caracteres[$rand-1];
	}
	return $retorno;
}

function redireciona($valor)
{
	echo "<script>window.location='".$valor."';</script>";
}
 
function salvaimagem($imagem, $largura, $altura, $caminho, $crop, $pb, $seo_nome = "", $watermark = "", $tipo_watermark = "", $pos_watermark = "", $opacity = array() )
{
	error_reporting(E_ALL);
	ini_set('memory_limit', '2048M');
	ini_set('max_execution_time', 0);
	include_once('classe_imagem/src/class.upload.php');
	$handle = new Upload($imagem);
	if ($handle->uploaded)
	{
		if($seo_nome)//redefine o nome da imagem
			$handle->file_src_name_body = forma_url( $seo_nome );
		$nome_arquivo = "";
		//Se foi feito o upload, percorrer o array para salvar as imagens
		foreach($largura as $i => $v)
		{
			$handle->image_resize			= true;
			if(@$crop[$i])
				$handle->image_ratio_crop	= true;
			else
				$handle->image_ratio		= true;			
			$handle->image_y				= $altura[$i];
			$handle->image_x				= $v;
			if($pb[$i])
				$handle->image_greyscale	= true;
			if(@$opacity[$i])
				$handle->image_opacity		= $opacity[$i];

			if($watermark)
			{
				if($tipo_watermark == "i") //a marca d'�gua � uma imagem
				{
					$handle->image_watermark = $watermark;
					$handle->image_watermark_position = $pos_watermark ? $pos_watermark : "B";//se n�o for setado, usar bottom para prevenir erros de processamento de imagem
				}
				else //� um texto
				{
					$handle->image_text = $watermark;
					$handle->image_text_background = '#000000';
					$handle->image_text_background_opacity = 50;
					$handle->image_text_padding    = 5;
					$handle->image_text_position   = $pos_watermark ? $pos_watermark : "B";//se n�o for setado, usar bottom para prevenir erros de processamento de imagem
				}
			}

			//processar a imagem
			$handle->Process($caminho[$i]);
			if($handle->processed)
				$nome_arquivo = $handle->file_dst_name;
		}
		//$handle-> Clean();
		return $nome_arquivo;
	}
	else
		return false;
}

function troca_acentos($texto, $inverso = false)
{
	//$inverso = true - retorna a letra acentuada
	//$inverso = false - retorna o c�digo html
	
	$arr_caracteres1 = array('�', '�','�','�',"�","�","�","�","�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�","�","�");
	
	$arr_caracteres2 = array('&frac12;', '&reg;','&sup3;','&sup2;',"&sect;","&ordm;","&ordm;","&ordf;","&aacute;", "&agrave;", "&acirc;", "&atilde;", "&auml;", "&eacute;", "&egrave;", "&ecirc;", "&euml;", "&iacute;", "&igrave;", "&icirc;", "&iuml;", "&oacute;", "&ograve;", "&ocirc;", "&otilde;", "&ouml;", "&uacute;", "&ugrave;", "&ucirc;", "&uuml;", "&ccedil;", "&Aacute;", "&Agrave;", "&Acirc;", "&Atilde;", "&Auml;", "&Eacute;", "&Egrave;", "&Ecirc;", "&Euml;", "&Iacute;", "&Igrave;", "&Icirc;", "&Iuml;", "&Oacute;", "&Ograve;", "&Ocirc;", "&Otilde;", "&Ouml;", "&Uacute;", "&Ugrave;", "&Ucirc;", "&Uuml;", "&Ccedil;","&Ntilde;","&ntilde;");
	
	if($inverso)
		$valor = str_replace($arr_caracteres2,$arr_caracteres1,$texto);
	else
		$valor = str_replace($arr_caracteres1,$arr_caracteres2,utf8_decode($texto));

	return $valor;
}

function troca($texto)
{
	$arraylixo = array("–", "�", "•","”",'�',"�","’",'‘',"“","�","�","�","�","�","\n","�","�","&amp;","\r","&nbsp;");
	$arraytroca = array("&ndash;", "&rsquo;", "&bull;",'&rdquo;','&rdquo;',"&rsquo;","&#039;","&#039;",'&ldquo;','&quot;','&ldquo;','&rdquo;',"&lsquo;","&rsquo;","<br />","-","&bull;","&",""," ");
	$valor = str_replace($arraylixo,$arraytroca,$texto);
	return $valor;
}

function filtro($tipocampo, $valor)
{
	$arraylixo = array( "&quot;&ldquo;", "?&rsquo;", "”",'…', 'Õ','Ô','”','�',"�","’",'‘',"“","�","�","�","�","�","\n","�","�","&amp;","\r","&nbsp;");
	$arraytroca = array( "&ndash;", "&rsquo;", "&quot;",'...','&Otilde;','&Ocirc;','&quot;','&quot;',"&#039;","&#039;","&#039;",'&quot;','&quot;','&quot;','&quot;',"&#039;","&#039;","<br />","-","&bull;","&",""," ");

	$array_O1 = array("�", "�", "�", "�", "�","�","�","�","�","�");
	$array_O2 = array("Ó","Ò","Ô","Õ","Ö","ó","ò","ô","õ","ö");
	$array_O = array("&Oacute;", "&Ograve;", "&Ocirc;", "&Otilde;", "&Ouml;","&oacute;", "&ograve;", "&ocirc;", "&otilde;", "&ouml;");

	switch($tipocampo)
	{
		case "string":
		$valor = str_replace($array_O1,$array_O,$valor);
		$valor = str_replace($array_O2,$array_O,$valor);
		$valor = troca($valor);
		$valor = troca_acentos($valor);
		$valor = str_replace($arraylixo,$arraytroca,$valor);
		$valor = htmlentities(utf8_decode($valor), ENT_QUOTES);
		$valor = str_replace($arraylixo,$arraytroca,$valor);
		break;
			
		case "date":
		if($valor != null || $valor != ""){
			$valor = @date("Y-m-d H:i", @strtotime(str_replace("/","-",$valor)));
		}else{
			$valor = "";
		}
		break;
						
		case "num":
		$valor = preg_replace("/\D+/", "", $valor); // remove qualquer caracter n�o num�rico
		break;
			
		case "float":
		$valor = str_replace(",",".",str_replace(".","",$valor));
		break;
		
		case "url":
		$valor = urldecode($valor);
		break;
		
		case "cond"://condi��o
		$valor = "";
		break;
		
		default:
		$valor = str_replace("\n","<br />",htmlentities(utf8_decode($valor), ENT_QUOTES));
		
	}
	return $valor;
}

function executa( $conexao, $form, $tabela, $acao, $condicao)
{
	$campos = "";
	$valores = "";
	$count = 0;
	if($acao == "inserir")
	{
		foreach($form as $campo => $valor)
		{
			$tipo = explode("-",$campo);
			
			if ($valor != null || $valor != "" || $valor != 0){
			
				if($tipo[1] != 'cond'){

					$valor = filtro($tipo[1],$valor);
				
					if($count != 0)
						$campos .= ", ".$tipo[0]." = '".($valor)."'";
					else
						$campos .= $tipo[0]." = '".($valor)."'";
			  
					$count++;
				}
			}
		}
		$sql = "INSERT INTO $tabela SET $campos";
		$sql = mysqli_query( $conexao, $sql);
		$id = mysqli_insert_id( $conexao );
		return $id;
	}
	else if($acao == "edita")
	{
		foreach($form as $campo => $valor)
		{
			$tipo = explode("-",$campo);
			
			if($tipo[1] != 'cond'){

				$valor = filtro($tipo[1],$valor);

				if($count != 0)
					$campos .= ", ".$tipo[0]." = '".($valor)."'";
				else
					$campos .= $tipo[0]." = '".($valor)."'";
		  
				$count++;
			}
		}
		$sql = "UPDATE $tabela SET $campos WHERE $condicao";
		$sql = mysqli_query( $conexao, $sql);
		return $sql;
	}
}

function lat_long($endereco){
	$endereco = tira_acento($endereco);
    $address = str_replace(' ','+', html_entity_decode( $endereco,ENT_QUOTES) ).',+Brasil';

    $ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, "http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	$result = curl_exec($ch);
	curl_close($ch);

    $output= json_decode($result);

    $lat = @$output->results[0]->geometry->location->lat;
    $long = @$output->results[0]->geometry->location->lng;

    $array = array();
    $array[0] = $lat;
    $array[1] = $long;
    return $array;
}

function assuntos($a)
{
	$assuntos = array(
		"s" => "Solicita&ccedil;&atilde;o de Parceria",
		"c" => "Contratar Servi&ccedil;o",
		"i" => "Informa&ccedil;&otilde;es do Pedido",
		"o" => "Outros"
	);
	return $assuntos[ $a ];
}
?>
