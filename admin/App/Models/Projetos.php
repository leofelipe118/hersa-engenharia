<?php

namespace App\Models;

use CRO\Model\Table;

class Projetos extends Table
{
    protected $table = "projetos";

    public function total($filtro = "")
    {
        $query = "SELECT id FROM {$this->table} WHERE 1 ".$filtro;
        return mysqli_num_rows( $this->db->query($query) );
    }

    public function listar($filtro,$inicio,$limite)
    {
        $query = "SELECT *, data, DATE_FORMAT(data, '%d/%m/%Y') as data_correta FROM {$this->table} WHERE 1 ".$filtro." ORDER BY data DESC LIMIT $inicio,$limite ";
        return $this->db->query($query);
    }

    public function excluiFoto($id)
    {
        $query = "UPDATE {$this->table} SET foto = NULL WHERE id = {$id}";
        return $this->db->query($query);
    }

	public function excluiVideo($id)
    {
        $query = "UPDATE {$this->table} SET video = NULL WHERE id = {$id}";
        return $this->db->query($query);
    }
	public function buscaGaleria($id)
	{
		$query = "SELECT * FROM projetos_galeria WHERE id_projeto = {$id} ORDER BY status DESC, ordem DESC, id DESC";
		return $this->db->query($query);
	}

	public function salvaDescricao($form)
	{
		$sql = executa($this->db, $form, "projetos_galeria", "edita", "id = '".$form['id-cond']."'");
		return $sql ? "ok" : "erro";
	}

	public function gravaGaleria($form)
    {
        if($form['id-cond'])
            $sql = executa($this->db, $form, "projetos_galeria", "edita", "id = '".$form['id-cond']."'");
        else
            $sql = executa($this->db, $form, "projetos_galeria", "inserir", "");

        if($sql)
            return $form['id-cond'] ? $form['id-cond'] : $sql;
        else
            return false;

    }

	public function alteraStatusGaleria($status,$id, $coluna = "")
    {
        $coluna = $coluna ? $coluna : "id";
        $query = "UPDATE projetos_galeria SET status = '{$status}' WHERE {$coluna} = '{$id}'";
        return $this->db->query($query);
    }
	
	public function checkUrl( $url, $id )
	{
		$geraUrl = forma_url($url);
		$cond = $id ? "AND id != {$id}" : "";
		$query = "SELECT id FROM {$this->table} WHERE url = '{$geraUrl}' {$cond}";
		return( $this->db->query($query)->num_rows );
	}

	public function geraUrl( $titulo, $id )
	{
		$geraUrl = forma_url($titulo);
		$this->novaUrl($geraUrl,$id);
		return false;
	}
	
	public function novaUrl( $titulo, $id, $passo = 0 )
	{
		$cond = $id ? "AND id != {$id}" : "";
		if($passo)
		{
			$teste = $titulo . "-" . $passo;
			$query = "SELECT id FROM {$this->table} WHERE url = '{$teste}' {$cond}";
			if( $this->db->query($query)->num_rows )
				$this->novaUrl($titulo,$id,$passo++);
			else
				echo $teste;
		}
		else
		{
			$query = "SELECT id FROM {$this->table} WHERE url = '{$titulo}' {$cond}";
			if( $this->db->query($query)->num_rows )
			{
				if($id)
				{
					$teste = $titulo . "-" . $id;
					$query = "SELECT id FROM {$this->table} WHERE url = '{$teste}' {$cond}";
					if( $this->db->query($query)->num_rows )
						$this->novaUrl($titulo,$id,1);
					else
						echo  $teste;
				}
				else
					$this->novaUrl($titulo,$id,1);				
			}
			else
				echo  $titulo;
		}
	}

	//Funções relacionadas as categorias
	public function busca_categorias()
    {
        $query = "SELECT * FROM categorias ORDER BY categoria";
        return $this->db->query($query);
    }

    public function addCategoria($form)
    {
		if( $form['id-cond'] )
			$sql = executa($this->db, $form, "categorias", "edita", "id = '".$form['id-cond']."'");
		else
			$sql = executa($this->db, $form, "categorias", "inserir", "");

        return $sql ? "ok" : "erro";
    }

}