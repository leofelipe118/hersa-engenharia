<div class="projetos_nav noticias-interno">

    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm"
        <?= $n['categoria'] == 'dm' ? 'class="active"' : ''?>><?= $define['projetos_internos-projetos_nav-DataCenter']?>
    </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h"
        <?= $n['categoria'] == 'h' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos_internos-projetos_nav-Hospitais']?>
    </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds"
        <?= $n['categoria'] == 'ds' ? 'class="active"' : ''?>><?= $define['projetos_internos-projetos_nav--Defesa-Seguranca']?>
    </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t"
        <?= $n['categoria'] == 't' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos_internos-projetos_nav-Transportes']?>
    </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m"
        <?= $n['categoria'] == 'm' ? 'class="active"' : ''?>><?= $define['projetos_internos-projetos_nav-Manutencao']?>
    </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=d"
        <?= $n['categoria'] == 'd' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos_internos-projetos_nav-Diversos']?>
    </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c"
        <?= $n['categoria'] == 'c' ? 'class="active"' : ''?>><?= $define['projetos_internos-projetos_nav-Concessoes']?>
    </a>


</div>


<a href="#" class="icon_backToPage"><img src="imagens/right-arrow.png" alt="voltar" srcset=""></a>

<div class="select-wrapper">


    <div class="select">

        <div class="container-select-filter">

            <div class="select-box">
                <div class="options-container">
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm" class="option">
                        <input type="radio" class="radio" id="sdataCenter" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm'"
                            for="sdataCenter"><?= $define['projetos_internos-select-options-DataCenter']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" class="option">
                        <input type="radio" class="radio" id="sHospitais" name="category" />
                        <label onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h'"
                            for="sHospitais"><?= $define['projetos_internos-select-options-Hospitais']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds" class="option">
                        <input type="radio" class="radio" id="sDefesa" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds'"
                            for="sDefesa"><?= $define['projetos_internos-select-options--Defesa-Seguranca']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t" class="option">
                        <input type="radio" class="radio" id="sTransportes" name="category" />
                        <label onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t'"
                            for="sTransportes"><?= $define['projetos_internos-select-options-Transportes']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m" class="option">
                        <input type="radio" class="radio" id="sManutencao" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m'"
                            for="sManutencao"><?= $define['projetos_internos-select-options-Manutencao']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/" class="option">
                        <input type="radio" class="radio" id="sDiversos" name="category" />
                        <label onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/'"
                            for="sDiversos"><?= $define['projetos_internos-select-options-Diversos']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c" class="option">
                        <input type="radio" class="radio" id="sConcessoes" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c'"
                            for="sConcessoes"><?= $define['projetos_internos-select-options-Concessoes']?>
                        </label>
                    </a>
                </div>

                <div class="selected">
                    <?= $define['projetos_internos-selected-filtro']?>

                </div>
            </div>
        </div>

    </div>
</div>

<section id="obra_interno">
    <div class="banner">
        <!-- <img src="imagens/projetos/large/<?=$n['foto']?$n['foto']:'bg_obra_interno.jpg'?>" alt=""> -->
        <div class="img_banner">
            <div class="swiper-container GaleriaProjetos">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image: url('imagens/projetos/large/<?=$n['foto']?$n['foto']:'bg_obra_interno.jpg'?>');"></div>
                    <?php 
                    // mostra_array($regGalProj);
                    $check_is_empty = $regGalProj->fetch_array(MYSQLI_ASSOC);
                    // mostra_array($check_is_empty);
                    if($check_is_empty ):?>
                    <?php foreach ($regGalProj as $galeriaPj => $galPj):?>
                        <div class="swiper-slide" style="background-image: url('imagens/projetos/large/<?=$galPj['foto']?>')"></div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                
                <div class="paginacao_swiper swiper-button-next" style="<?= $check_is_empty? '' : 'display: none;'?>"></div>
                <div class="paginacao_swiper swiper-button-prev" style="<?= $check_is_empty? '' : 'display: none;'?>"></div>
            </div>
        </div>
    </div>
    <div class="infos">
        <h2 class="titulo_obra">
            <?=html_entity_decode($n['titulo_'.$lang]?$n['titulo_'.$lang]:$n['titulo_pt'], ENT_QUOTES)?></h2>
        <p class="descricao_obra">
            <?=html_entity_decode($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt'], ENT_QUOTES)?></p>

        <p class="obras-intero-descricao-up"><?= $define['projetos_internos-#projetos_recomendados-status']?> <span class="espacamento"> </span>

        <?php
            if($n['status_projeto']=="oa"){
                if($lang == 'pt'){
                    echo "Obra em Andamento";
                }
                else if($lang == 'en'){
                    echo "In progress";
                }
                else if($lang == 'es'){
                    echo "Trabajo en progreso";
                }
            }
            else{
                if($lang == 'pt'){
                    echo "Obra Concluída";
                }
                else if($lang == 'en'){
                    echo "Completed";
                }
                else if($lang == 'es'){
                    echo "Trabajo completo";
                }
            }
        ?>
        </p>
        <p class="obras-intero-descricao-down"><?= $define['projetos_internos-#projetos_recomendados-categoria']?><span class="espacamento"></span>
            <?php
       switch($n['categoria'])
       {
            case "c":
                $categoria_echo_pt = "Concessões";
                $categoria_echo_en = "Concessions";
                
            break;

            case "dm":
                $categoria_echo_pt = "Datacenter e Missão Crítica";
                $categoria_echo_en = "Datacenter and Mission Critical";
                
            break;

            case "ds":
                $categoria_echo_pt = "Defesa e Segurança";
                $categoria_echo_en = "Defence and Security";
            break;

            case "d":
                $categoria_echo_pt = "Diversos";
                $categoria_echo_en = "Other";
            break;

            case "h":
                $categoria_echo_pt = "Hospitais e Instituições de Saúde";
                $categoria_echo_en = "Hospitals and Healthcare Institutions";
            break;

            case "m":
                $categoria_echo_pt = "Manutenção e Facilities";
                $categoria_echo_en = "Maintenance and Facilities";
            break;

            case "t":
                $categoria_echo_pt = "Transportes";
                $categoria_echo_en = "Transportations";
            break;
       }
        if($lang == 'en'){
           echo $categoria_echo_en;
        }
        else{
            echo $categoria_echo_pt;
        }
       ?></p>

        <div class="redes_sociais">
            <?php if($n['url_linkedin']): ?>
            <a href="<?=$n['url_linkedin']?>" target="_blank">
                <img src="imagens/icon_linkedin.svg" alt="">
            </a>
            <?php endif; ?>
            <?php if($n['url_facebook']): ?>
            <a href="<?=$n['url_facebook']?>" target="_blank">
                <img src="imagens/icon_face.svg" alt="">
            </a>
            <?php endif; ?>
            <?php if($n['url_instagram']): ?>
            <a href="<?=$n['url_instagram']?>" target="_blank">
                <img src="imagens/icon_insta.svg" alt="">
            </a>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php if($n['video']):?>
<video controls class="video_projeto">
    <source src="imagens/videos/<?=$n['video']?>" type="video/mp4">
</video>
<?php endif; ?>

<section id="projetos_recomendados">
    <h3 class="titulo"><?= $define['projetos_internos-#projetos_recomendados-titulo']?>
    </h3>
    <div class="recomendados">
        <?php foreach ($arrNoticiasRel as $noticias => $n): 
        $texto = htmlspecialchars_decode($n['texto_'.$lang]);
        // $texto = str_replace("<h3>", "</p><h3>", $texto);
        // $texto = str_replace("</h3>", "</h3><p>", $texto);
        // $texto = str_replace("<h2>", "</p><h2>", $texto);
        // $texto = str_replace("</h2>", "</h2><p>", $texto);
            ?>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$n['url']?>/<?=$n['id']?>" rel="noopener noreferrer">
            <div class="projeto">
                <div class="contem_imagem"
                    style="background-image: url('imagens/projetos/large/<?=$n['foto']?$n['foto']:''?>');"></div>
                <!-- <img src="imagens/projetos/large/<?=$n['foto']?$n['foto']:''?>" alt=""> -->
                <h3 class="subtitulo">
                    <?=html_entity_decode($n['titulo_'.$lang]?$n['titulo_'.$lang]:$n['titulo_pt'], ENT_QUOTES)?></h3>
                <!-- <span class="descricao"><?=html_entity_decode($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt'], ENT_QUOTES)?></span> -->
                <span class="descricao"><?=$texto?></span>
            </div>
        </a>
        <?php endforeach; ?>
        <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <img src="imagens/obra_int_ciscea.jpg" alt="">
                <h3 class="subtitulo">CISCEA – Modernização de Sistemas de Energia e Climatização</h3>
                <span class="descricao">Reforma e Modernização dos Sistemas Elétricos e de Climatização em 09 Destacamentos...</span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <img src="imagens/obra_int_marinha.jpg" alt="">
                <h3 class="subtitulo">Marinha – COGESN</h3>
                <span class="descricao">Construção dos edifícios de escritórios de projetos com 5 pavimentos e prédio anexo para paióis e...</span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <img src="imagens/obra_int_cnh.jpg" alt="">
                <h3 class="subtitulo">CNH – Centro de Distribuição Sorocaba</h3>
                <span class="descricao">Construção do Prédio da Subestação (SUB2) do Laboratório de Geração de Energia...</span>
            </div>
        </a>     -->
    </div>
</section>

<script type="text/javascript">
    $(function () {
        $(".filtros_int").click(function (e) {
            var classe = $(this).attr("class");

            if (classe.indexOf('ativo') >= 0) {
                $(this).find(".seta").removeClass("ativo");
                $(this).find(".filtros_disponiveis").removeClass("ativo");
                $(this).removeClass("ativo");
            } else {
                $(this).find(".seta").addClass("ativo");
                $(this).find(".filtros_disponiveis").addClass("ativo");
                $(this).addClass("ativo");
            }
        });
    })


    // ===*Selected*===
    // const selected = document.querySelector(".selected");
    //         const optionsContainer = document.querySelector(".options-container");

    //         const optionsList = document.querySelectorAll(".option");

    //         selected.addEventListener("click", () => {
    //             optionsContainer.classList.toggle("active");
    //             });

    //         optionsList.forEach(o => {
    //             o.addEventListener("click", () => {
    //             selected.innerHTML = o.querySelector("label").innerHTML;
    //             optionsContainer.classList.remove("active");
    //             });
    //         });
</script>