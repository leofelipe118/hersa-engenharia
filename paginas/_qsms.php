<section class="a_hersa_top qsms">
    <div class="filtro"></div>
</section>
<section class="areas_atuacao dark" id="qsms_qualidade">
    <div class="intro">
        <h2 class="titulo">Qualidade</h2>
        <h3 class="descricao">Na busca da melhoria contínua desde a certificação inicial da série ISO 9001 em 2004, possuímos atualmente um sistema de gestão de qualidade, certificado na norma ISO 9001:2015. Enxuto e eficaz, garante a entrega do produto final de acordo com os requisitos aplicáveis.</h3>
    </div>
    <img src="imagens/qsms_qualidade.jpg" alt="">
</section>
<section class="areas_atuacao dark" id="qsms_seg_trabalho">
    <div class="intro">
        <h2 class="titulo">Segurança do Trabalho</h2>
        <h3 class="descricao">A vida é um bem maior e buscamos incessantemente garantir que todos os riscos envolvidos em um empreendimento estejam devidamente mapeados, monitorados, reduzidos ou eliminados.Somos uma empresa membro do VISION ZERO e adotamos abordagens do método desde 2019. Nossa gestão é alicerçada em ações preventivas continuamente verificadas e medidas, utilizando-se dos métodos mais atuais existentes. <br>Somos empresa membro do VISION ZERO e adotamos abordagem do método desde 2019. <br><br>Através da integração das disciplinas de QSMS, conseguimos garantir que nossos projetos sigam padronizados e controlados, sem desvios relevantes. Por conta do dinamismo que projetos de engenharia requerem, utilizamos métodos ágeis de gestão, como o SCRUM, de modo a garantir a celeridade e foco no que realmente importa ao processo, buscando sempre a satisfação de nossos clientes.</h3>
    </div>
    <img src="imagens/qsms_seg_trabalho.jpg" alt="">
</section>
<section class="areas_atuacao dark" id="qsmq_meio_ambiente">
    <div class="intro">
        <h2 class="titulo">Meio Ambiente</h2>
        <h3 class="descricao">Estamos cientes dos impactos ambientais de nossas operações, portanto, traçamos ações de modo a reduzi-los ao máximo, com o levantamento e monitoramento dos aspectos e impactos ambientais envolvidos em cada projeto, destinação adequada dos resíduos gerados, plano de contingência para emergências ambientais e, assim, garantimos a execução de projetos sustentáveis. Contamos com experiência na participação de projetos com certificação LEED - Leadership in Energy and Environmental Design.</h3>
    </div>
    <img src="imagens/qsmq_meio_ambiente.jpg" alt="">
</section>
<section class="areas_atuacao dark" id="qsms_saude">
    <div class="intro">
        <h2 class="titulo">Saúde Ocupacional</h2>
        <h3 class="descricao">Buscamos oferecer um ambiente sadio a todos os colaboradores através de uma política de saúde ocupacional voltada ao controle e mitigação de riscos ocupacionais, garantindo um ambiente de trabalho saudável, sem a prevalência de doenças ocupacionais.</h3>
    </div>
    <img src="imagens/qsms_saude_ocupacional.jpg" alt="">
</section>
<section class="areas_atuacao dark" id="qsmq_meio_ambiente">
    <div class="intro">
        <h2 class="titulo">Métodos Ágeis e Inovação</h2>
        <h3 class="descricao">Com gestão orientada a processos e pessoas, na vanguarda do setor, utilizamos métodos ágeis na gestão de QSMS. Estabelecemos nossas estratégias com uso do OKR – Objective anda Key Results (Objetivos e Resultados Chave) desdobrando as metas estratégicas a todos os envolvidos, acompanhamos o desdobramento das tarefas via Kanban Eletrônico. O SCRUM garante o fluxo contínuo ao processo. Esses métodos garantem um sistema de gestão robusto, enxuto e eficaz.</h3>
    </div>
    <img src="imagens/qsmq_meio_ambiente.jpg" alt="">
</section>
