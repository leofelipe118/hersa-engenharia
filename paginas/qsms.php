 <section class="qsms ">

     <!-- <section class="header_qsms"> 
            <div class="text_header_qsms">
                <h3>
                Qualidade, Segurança do Trabalho, Meio Ambiente e Saúde Ocupacional
                </h3> 
            </div>
    </section> -->

     <section class="a_hersa_top qsms">
         <section class="a_hersa_engenharia qsms ">
             <h3 class="titulo">
                 <?= $define['qsms-a_hersa_engenharia-title']?>
                 <br> <?= $define['qsms-a_hersa_engenharia-title-line2']?>
             </h3>

         </section>
     </section>

     <div class="qsms_wrapper">
         <section class="obras_projetos">
             <div class="projeto" id="">
                 <img src="imagens/qsms-image-1.png" alt="">
                 <div class="infos">
                     <h3 class="titulo"><?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-1']?>
                     </h3>
                     <p class="descricao"><?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-1']?>
                     </p>
                     <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais">Ver mais <i><img src="imagens/arrow_vermais.svg" alt=""></i></a> -->
                 </div>
             </div>
             <div class="projeto qsmsRight" id="">
                 <img src="imagens/qsms-image-2.png" alt="">
                 <div class="infos">
                     <h3 class="titulo"><?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-2']?>
                     </h3>
                     <p class="descricao"><?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-2']?>
                     </p>
                     <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais">Ver mais <i><img src="imagens/arrow_vermais.svg" alt=""></i></a> -->
                 </div>
             </div>
             <div class="projeto" id="">
                 <img src="imagens/qsms-image-3.png" alt="">
                 <div class="infos">
                     <h3 class="titulo"> <?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-3']?>
                     </h3>
                     <p class="descricao"><?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-3']?>
                     </p>
                     <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais">Ver mais <i><img src="imagens/arrow_vermais.svg" alt=""></i></a> -->
                 </div>
             </div>
             <div class="projeto qsmsRight" id="">
                 <img src="imagens/qsms-image-4.png" alt="">
                 <div class="infos">
                     <h3 class="titulo"> <?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-4']?>
                     </h3>
                     <p class="descricao">
                         <?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-4']?>

                     </p>
                     <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais">Ver mais <i><img src="imagens/arrow_vermais.svg" alt=""></i></a> -->
                 </div>
             </div>
             <div class="projeto" id="">
                 <img src="imagens/qsms-image-5.png" alt="">
                 <div class="infos">
                     <h3 class="titulo"><?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-5']?>
                     </h3>
                     <p class="descricao">
                         <?= $define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-5']?>
                     </p>
                     <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais">Ver mais <i><img src="imagens/arrow_vermais.svg" alt=""></i></a> -->
                 </div>
             </div>
         </section>
     </div>


 </section>



 <script type="text/javascript">
     $(function () {
         $(".filtros_int").click(function (e) {
             var classe = $(this).attr("class");

             if (classe.indexOf('ativo') >= 0) {
                 $(this).find(".seta").removeClass("ativo");
                 $(this).find(".filtros_disponiveis").removeClass("ativo");
                 $(this).removeClass("ativo");
             } else {
                 $(this).find(".seta").addClass("ativo");
                 $(this).find(".filtros_disponiveis").addClass("ativo");
                 $(this).addClass("ativo");
             }
         });
     })
 </script>