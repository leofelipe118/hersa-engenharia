<?php

namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class LoginController extends Action
{
    public function index()
    {
        if(@$_POST)
        {
            $usu = Container::getModel("Login");
            $form = retorna_form($_POST);
            if($form['acao'] == "login")//usuario fazendo login
            {
                $this->view->retorno = $usu->login($form);
                if($this->view->retorno === true)
                {
                    $retorno = $_SESSION[DB_DATABASE]['retorno'] ? $_SESSION[DB_DATABASE]['retorno'] : "";
                    redireciona( BASE_SITE . $retorno );
                }
                else
                {
                    $this->view->retorno = "E-mail ou senha inválidos! :(";
                    $this->render("login", false);
                }
            }
            else//usuario tentanto lembrar senha
            {
                $this->view->retorno = $usu->lembrar($form['email']) ?  "Já enviamos a senha para o e-mail " . $_POST['email'] . " ;)" : "Não encontramos o e-mail " . $_POST['email'] . " em nossa base de dados.<br />Verifique o e-mail e tente novamente ;)";
                $this->render("login",false);
            }
        }
        else
            $this->render("login",false);
    }

    public function logout()
    {
        session_destroy();
        session_unset();
        redireciona(BASE_SITE);
    }
}