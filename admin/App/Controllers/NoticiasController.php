<?php
namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class NoticiasController extends Action
{
    public function listar()
    {
        $dados = Container::getModel("Noticias");

        $this->view->pesquisar = "";
        $filtro = "";
        if(@$_POST['pesquisar']) {
            $this->view->pesquisar = seguranca($_POST['pesquisar']);
            $palavra = tira_acento( filtro( "string", $this->view->pesquisar ) );
            $filtro = " AND titulo_pt LIKE '%".$palavra."%' ";
        }

        $this->view->url = Bootstrap::getUrl();
        $buscaUrl = @explode("/", $this->view->url );
        $id_retorno = @array_pop( $buscaUrl );
        $pagina = is_numeric($id_retorno) ? $id_retorno : 1;
        $this->view->temPag = is_numeric($id_retorno) ? true : false;

        //verifica se tem retorno de insert
        $verif = @array_pop( $buscaUrl );
        if($verif == "ok" || $verif == "erro")
            $this->view->retorno = $verif;

        $this->view->pagina = $pagina;
        $this->view->limite = 25;
        $this->view->inicio = ( $this->view->pagina * $this->view->limite ) - $this->view->limite;
        $this->view->resul = $dados->total( $filtro );
        $this->view->totalPaginas = ceil( $this->view->resul / $this->view->limite );

        $this->view->header = "Blog";
		$this->view->headerSmall = "Listagem de Publicações";
        $this->view->dados = $dados->listar( $filtro, $this->view->inicio, $this->view->limite );
        $this->render("listar");
    }

    public function status()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Noticias");
        $this->view->dados = $dados->alteraStatus($form['status'],$form['id'],"id");
        return false;
    }

    public function checkUrl()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Noticias");
        echo $chk = $dados->checkUrl($form['url'],$form['id']);
        return false;
    }

    public function geraUrl()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Noticias");
        $chk = $dados->geraUrl($form['titulo'],$form['id']);
        return false;
    }

    public function cadastrar()
    {
        $temID = @array_pop( @explode("/", Bootstrap::getUrl() ) );
        $dados = Container::getModel("Noticias");
        $this->view->header = "Blog";
		$this->view->headerSmall = "Cadastro e Edição de Publicação";
		$this->view->categorias = $dados->busca_categorias();
        if(is_numeric( $temID ) )
            $this->view->dados = $dados->find($temID,"id");

        $this->render("cadastrar");
    }

    public function cadastro()
    {
        $dados = Container::getModel("Noticias");
        $form = retorna_form($_POST);
        $form['texto_pt-string'] = str_replace("</p><p>", "<br>", $form['texto_pt-string'] );
        $form['texto_en-string'] = str_replace("</p><p>", "<br>", $form['texto_en-string'] );
        $form['texto_es-string'] = str_replace("</p><p>", "<br>", $form['texto_es-string'] );

        if( isset( $_FILES['files'] ) )
            unset( $_FILES['files'] );

		if(isset($_FILES['logo']))
        {
			$tamanho = getimagesize($_FILES['logo']['tmp_name']);
			$tamL = $tamanho[0] > 1920 ? 1920 : $tamanho[0];
			$tamA = $tamanho[1] > 1080 ? 1080 : $tamanho[1];
			$tamThumbL = $tamL > $tamA ? 480 : 270;
			$tamThumbA = $tamL > $tamA ? 270 : 480;
			
			
            $largura 	= array($tamL, $tamThumbL, 650 );
            $altura 	= array($tamA, $tamThumbA, 650 );
            $crop 		= array("","","s");
            $pb 		= array("","","");
            $caminho 	= array("../imagens/posts/large/","../imagens/posts/thumb/","../imagens/posts/crop/");
            $form['foto-string'] = salvaimagem($_FILES["logo"], $largura, $altura, $caminho, $crop,$pb, forma_url( NOME_CLIENTE . "-" . $form['titulo-string'] . date("dmY") ) );
            if( $form['foto-string'] && @$form['fotoOld-cond'] )
            {
                @unlink( "../imagens/posts/large/".$form['fotoOld-cond'] );
                @unlink( "../imagens/posts/thumb/".$form['fotoOld-cond'] );
                @unlink( "../imagens/posts/crop/".$form['fotoOld-cond'] );
            }
        }

        $retorno = $dados->grava($form);
        $urlRet = $retorno ? "ok" : "erro";

        redireciona(BASE_SITE."noticias/listar/".$urlRet."/1");
    }

    public function excluirImg()
    {
        $form = retorna_form($_POST);
        @unlink( "../imagens/posts/large/".$form['imagem'] );
        @unlink( "../imagens/posts/thumb/".$form['imagem'] );
        @unlink( "../imagens/posts/crop/".$form['imagem'] );
        $dados = Container::getModel("Noticias");
        $dados->excluiFoto($form['id']);
        return false;
    }

	//Funções relacionadas as categorias
	
    public function categorias()
    {
        $cat = Container::getModel("Noticias");
        $this->view->header = "Blog";
		$this->view->headerSmall = "Cadastro e Edição de Categorias";
        $this->view->cat = $cat->busca_categorias();
        $this->render("categorias");
    }

	public function addCategoria()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Noticias");
        $this->view->dados = $dados->addCategoria( $form );
        return false;
    }
	
	public function buscaCategorias()
	{
        $cat = Container::getModel("Noticias");
        $this->view->cat = $cat->busca_categorias();
		return $this->view->cat;
	}


}