<section>
    <div class="bg-circles">
        <div class="circle1"><img src="imagens/circle1.svg" title="Confidence it services" alt="Confidence it services"/></div>
        <div class="circle2"><img src="imagens/circle2.svg" title="Confidence it services" alt="Confidence it services"/></div>
        <div class="circle3"><img src="imagens/circle3.svg" title="Confidence it services" alt="Confidence it services"/></div>
    </div>
    <div class="container section-lg position-relative section-first">
        <div class="pagination-arrow blog_int">
            <a href="<?=$linkcanonical?><?=$lang?>/blog">
                <svg xmlns="http://www.w3.org/2000/svg" width="81.024" height="81.513" viewBox="0 0 51.024 51.513"><g transform="translate(0 51.513) rotate(-90)"><g transform="translate(18.608 29.651) rotate(-90)"><path d="M8.442,7.962,1.869,14.535a.81.81,0,0,1-1.143,0l-.484-.484a.809.809,0,0,1,0-1.143l5.52-5.52L.236,1.863A.81.81,0,0,1,.236.72L.72.236a.81.81,0,0,1,1.143,0L8.442,6.815a.816.816,0,0,1,0,1.147Z" fill="#fff"/></g><g fill="none" stroke="#fff" stroke-width="2"><ellipse cx="25.756" cy="25.512" rx="25.756" ry="25.512" stroke="none"/></g></g></svg>
            </a>
        </div>
        <h1 class="h1 font-secondary"><?=$n['titulo_'.$lang]?></h1>
        <div class="blog-post-img"><img class="w-100" src="imagens/posts/large/<?=$n['foto']?>" srcset="imagens/posts/large/<?=$n['foto']?> 2x, imagens/posts/large/<?=$n['foto']?> 1x" loading="lazy" title="<?=$n['titulo_'.$lang]?>" alt="<?=$n['titulo_'.$lang]?>"/></div>
        <div class="row mt-3 align-items-center">
            <div class="col-auto"><img class="rounded mw-100" src="imagens/confidence-o.png" srcset="imagens/confidence-o@2x.png 2x, imagens/confidence-o.png 1x" loading="lazy" alt=""/></div>
            <div class="col"><?= $define['bloginterno-ConfidenceIT']?>
<br/><?= formataDataAbrev($n['data'], $arr_meses, $lang);?></div>
        </div>
        <div class="my-5"><?= $texto?></div>
    </div>
</section>

<section class="bg-primary round-top-left">
    <div class="text-center section-lg">
        <div class="h3 color-w50 mb-4"><?= $define['bloginterno-bg-primary-container-h3']?>
</div>
        <form class="row" action="" method="POST">
            <div class="col-lg-4 offset-lg-4">
                <input type="text" name="" class="input-xl w-100 mb-3" placeholder="Seu nome"/>
                <input type="text" name="" class="input-xl w-100 mb-3" placeholder="Seu e-mail"/>
                <button type="submit" class="button button-secondary mt-4 h5"><?= $define['bloginterno-bg-primary-container-button']?></button>
            </div>
        </form>
    </div>
</section>