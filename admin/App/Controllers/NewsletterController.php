<?php
/**
 * Created by PhpStorm.
 * User: cleversonr
 * Date: 01/08/2017
 * Time: 14:21
 */

namespace App\Controllers;


use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class NewsletterController extends Action
{
    public function listar()
    {
        $lista = Container::getModel("Newsletter");
        $this->view->header = "Cadastro de E-mails";
        $this->view->headerSmall = "Lista com todos os e-mails para recebimento de newsletter";

        $this->view->palavra = "";
        $this->view->dataIni = date("d/m/Y", strtotime("-1 week"));
        $this->view->dataFim = date("d/m/Y");
        $dataIniSQL = date("Y-m-d 00:00", strtotime("-1 week"));
        $dataFimSQL = date("Y-m-d 23:59");
		$addClausula = "";
        if($_POST)
        {
            $form = retorna_form($_POST);
            if(@$form['start'])
            {
                $this->view->dataIni = $form['start'];
                $dataIniSQL = filtro("date",$form['start']);
            }
            if(@$form['end'])
            {
                $this->view->dataFim = $form['end'];
                $dataFimSQL = filtro( "date", $form['end']." 23:59" );
            }
            $this->view->palavra = $form['palavra'];
			
        }

        //Busca a informação de contatos/dia nos ultimos 7 dias
        $dataHoje = date("Y-m-d");
        $dataini7 = date("Y-m-d", strtotime("-7 days"));
        $this->view->start7 = strtotime("-7 days");
        $this->view->end7 = strtotime( "today" );
        $clausula7 = " AND data BETWEEN '".$dataini7."' AND '".$dataHoje." 23:59' ";
        $this->view->lista7 = $lista->contMes( $clausula7 );

        $clausulaCont7 = " AND a.data BETWEEN '".$dataini7."' AND '".$dataHoje." 23:59' ";
        $this->view->listaCont7 = $lista->contPontos( $clausulaCont7 );

        $clausula = " AND a.data BETWEEN '".$dataIniSQL."' AND '".$dataFimSQL."' " . $addClausula;
        if($this->view->palavra)
        {
            $palavra = tira_acento( filtro("string", $this->view->palavra ) );
            $clausula .= " AND ( HTML_UnEncode(a.email) LIKE '%{$palavra}%' ) ";
        }

        $this->view->lista = $lista->busca_msg( $clausula );

        //Verifica se tem retorno na URL
        $url = Bootstrap::getUrl();
        $id_retorno = @array_pop( @explode("/", $url ) );
        if( is_numeric($id_retorno) )
            $this->view->retorno  = $id_retorno == 1 ? "ok" : "erro";



        $this->render("listar");
    }

    public function status()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Newsletter");
        $this->view->dados = $dados->alteraStatus($form['status'],$form['id']);
        return false;
    }

    public function tudoLido()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Newsletter");
        $dados->tudoLido();
        return false;
    }

    public function exportar()
    {
        $lista = Container::getModel("Newsletter");
        $form = retorna_form($_GET);
        $dataIniSQL = filtro("date",$form['start']);
        $dataFimSQL = filtro( "date", $form['end']." 23:59" );
        $clausula = " AND a.data BETWEEN '".$dataIniSQL."' AND '".$dataFimSQL."' ";
        $this->view->lista = $lista->busca_msg( $clausula );
        $this->view->novoNome = "Cadastros-Newsletter-". str_replace("/","-",$form['start'])."-".str_replace("/","-",$form['end']);
        $this->render("exportar", false);

    }

    public function importar()
    {
        $dados = Container::getModel("Newsletter");
        $this->view->header = "Upload de Contatos";
        $this->view->headerSmall = "Importação de Contatos do ERP";

        $confere = @array_pop( @explode( "/", trim( Bootstrap::getUrl(), "/" ) ) );
        $this->view->retorno = $confere != "importar" ? $confere : false;

        $this->view->palavra = "";
        if($_POST)
        {
            $form = retorna_form($_POST);
            $this->view->dataIni = $form['start'];
            $this->view->dataFim = $form['end'];
            $dataIniSQL = filtro("date",$form['start']);
            $dataFimSQL = filtro( "date", $form['end']." 23:59" );
            $this->view->palavra = $form['palavra'];
        }
        else
        {
            $this->view->dataIni = date("d/m/Y", strtotime("-1 month"));
            $this->view->dataFim = date("d/m/Y");
            $dataIniSQL = date("Y-m-d 00:00", strtotime("-1 month"));
            $dataFimSQL = date("Y-m-d 23:59");
        }

        $clausula = " AND dt_cadastro BETWEEN '".$dataIniSQL."' AND '".$dataFimSQL."' ";
        if($this->view->palavra)
        {
            $palavra = tira_acento( filtro("string", $this->view->palavra ) );
            $clausula .= " AND ( HTML_UnEncode(email) LIKE '%{$palavra}%' ) ";
        }

        $this->view->lista = $dados->busca_parceiros( $clausula );

        $this->render("importar");
    }

    public function excel()
    {
        require "vendor/CRO/BaseAdmin/excel/reader.php";
        $dados = Container::getModel("Newsletter");

        $data = new \Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('UTF-8');
        $data->read($_FILES['arquivo']['tmp_name']);

        //envia para o model para fazer a manipulação dos dados - o retorno é o nome do arquivo de log
        $log = $dados->importa( $data );
        redireciona(BASE_SITE."newsletter/importar/".$log);
    }

}