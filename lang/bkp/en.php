<?php

$define = array();



// ===*Areas de Atuacao*===
 

$define['areas_atuacao_wrapper-titulo'] = 'SERVICES ';
$define['areas_atuacao_wrapper-titulo-#eng-civil'] = ' Civil Engineering ';
$define['areas_atuacao_wrapper-descrica-#eng-civil'] = ' Building, commercial and industrial constructions, retrofit, bases and structures for equipment and underground infrastructure.';
$define['areas_atuacao_wrapper-titulo-#eng-ele'] = 'Electrical Engineering';
$define['areas_atuacao_wrapper-descricao-#eng-ele'] = 'Power Transformer Substations, Power Generation Systems, Medium and Low Voltage Electrical Installations, Energy Efficiency Systems, Lighting, Grounding and Protection against Atmospheric Discharge - SPDA.';
$define['areas_atuacao_wrapper-titulo-#eng-hid'] = 'Hydraulic Engineering';
$define['areas_atuacao_wrapper-descricao-#eng-hid'] = 'Potable, rain and hot water networks, cold water plants (CWP), sanitary sewage, fire fighting systems and fuel gas.';
$define['areas_atuacao_wrapper-titulo-#eng_mec'] = 'Mechanical Engineering';
$define['areas_atuacao_wrapper-descricao-#eng_mec'] = 'Plants and pipelines for fluids, special gases and compressed air, industrial wastewater treatment systems and ventilation and air conditioning systems.';
$define['areas_atuacao_wrapper-titulo-#sis_esp'] = 'Special Systems';
$define['areas_atuacao_wrapper-descricao-#sis_esp'] = 'Critical mission special systems, building automation and supervision, detection, alarm and fire fighting, access control, CCTV, sound, telecommunications, data and image and structured cabling.';
$define['areas_atuacao_wrapper-titulo-#sis_con'] = 'Concessions';
$define['areas_atuacao_wrapper-descricao-#sis_con'] = 'Public Service Concession Contracts and Public Private Partnerships aiming at a rational and efficient management of assets.';

// ===*Blog *===
 
$define['blog-title-section-1'] = 'Conteúdo de';
$define['blog-title-section-bold'] = 'confiança e credibilidade';
$define['blog-title-section-2'] = 'para empresários de pequenas, médias e grandes empresas.';
$define['blog-title-Confidence-IT'] = 'CONFIDENCE IT';
$define['blog-bg-primary-container-h3'] = 'Assine a nossa newsletter e não perca nenhuma novidade! ';
$define['blog-bg-primary-container-button'] = 'Assinar';

// ===*Blog Interno*===
$define['bloginterno-ConfidenceIT'] = 'CONFIDENCE IT';
$define['bloginterno-bg-primary-container-h3'] = 'Assine a nossa newsletter e não perca nenhuma novidade! ';
$define['bloginterno-bg-primary-container-button'] = 'Assinar';

// ===*Clientes*===

$define['clientes-title-Data-Center'] = 'Data Center';
$define['clientes-title-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['clientes-title-Defesa-Seg'] = 'Defence and Security';
$define['clientes-title-Transportes'] = 'Transportations';
$define['clientes-title-Manutencao-Facilities'] = 'Maintenance and Facilities';
$define['clientes-title-Ensino'] = 'Educational Institutions';
$define['clientes-title-Orgaos-Judiciais'] = 'Judicial';
$define['clientes-title-Prefeituras'] = 'City Halls';
$define['clientes-title-Conc-Servicos'] = 'Service Concessionaires';
$define['clientes-title-Seg-Diversos'] = 'Other Segmentes';
$define['clientes-title-Concessoes'] = 'Concessions';

// ===*Compliance*===
$define['compliance-linear-background-titulo-compliance-strong'] = 'Compliance';
$define['compliance-linear-background-titulo-compliance'] = 'and ethics';

$define['compliance-infos_compliance-descricao'] = 'Hersa Engenharia establishes the highest standard of conduct for its employees, both in dealings within a professional context with customers and suppliers, and in activities related to its segment of operation. ';
$define['compliance-infos_compliance-descricao-parte2'] = 'Learn about our compliance policies.';

$define['compliance-texto_arquivos-introducao-principios'] = 'INTRODUÇÃO E EXPLICAÇÃO DOS PRINCÍPIOS';
$define['compliance-texto_arquivos-introducao-principios-Kbps'] = 'PDF (154kb)';
$define['compliance-texto_arquivos-codigo-de-etica'] = 'CÓDIGO DE ÉTICA E CONDUTA';
$define['compliance-texto_arquivos-codigo-de-etica-Kbps'] = 'PDF (382kb)';
$define['compliance-texto_arquivos-anticorrupcao'] = 'POLÍTICA ANTICORRUPÇÃO';
$define['compliance-texto_arquivos-anticorrupcao-Kbps'] = 'PDF (427kb)';
$define['compliance-texto_arquivos-conflito-interesses'] = 'POLÍTICA DE CONFLITO DE INTERESSES';
$define['compliance-texto_arquivos-conflito-interesses-Kbps'] = 'PDF (580kb)';
$define['compliance-texto_arquivos-politicas-brindes'] = 'POLÍTICA SOBRE BRINDES, PRESENTES, VIAGENS E HOSPITALIDADES';
$define['compliance-texto_arquivos-politicas-brindes-Kbps'] = 'PDF (451kb)';
$define['compliance-texto_arquivos-contratacao-terceiros'] = 'POLÍTICA DE CONTRATAÇÃO DE TERCEIROS';
$define['compliance-texto_arquivos-contratacao-terceiros-Kbps'] = 'PDF (948kb)';
$define['compliance-texto_arquivos-doacoes'] = 'POLÍTICA DE DOAÇÕES E PATROCÍNIOS';
$define['compliance-texto_arquivos-doacoes-Kbps'] = 'PDF (355kb)';
$define['compliance-texto_arquivos-privacidade'] = 'POLÍTICA DE PRIVACIDADE';
$define['compliance-texto_arquivos-privacidade-Kbps'] = 'PDF (210kb)';

$define['compliance-infos_compliance-footer-descriacao'] = 'Contact the compliance sector for more information:';
$define['compliance-infos_compliance-footer-descriacao-email'] = 'compliance@hersa.com.br';

// ===*Contato*===
$define['contato-#contato-infos-text_contato'] = 'Talk to us';
$define['contato-#contato-infos-telefone'] = '55 11 2603 4178';
$define['contato-#contato-infos-email'] = 'contato@hersa.com.br';
$define['contato-#contato-infos-endereco'] = 'Rua Padre Raposo, 497 |  Mooca ';
$define['contato-#contato-infos-endereco-cep'] = 'CEP: 03118-000 | São Paulo/SP ';

$define['contato-#contato-form_flutuante-titulo'] = 'Use the fields below to leave your 
message that we will soon return.';

$define['contato-#contato-form-input-#nome-placeholder'] = 'Name';
$define['contato-#contato-form-input-#telefone-placeholder'] = 'Telephone';
$define['contato-#contato-form-input-#mensagem-placeholder'] = 'Message';

$define['contato-#contato-form_flutuante-label'] = 'I agree with the Privacy Policy and Terms';
$define['contato-#contato-form_flutuante-button'] = 'Submit';




$define['contato-trabalhe_conosco_top-bg-titulo-strong'] = 'Careers';
$define['contato-form_trabalhe_conosco-descricao'] = 'Fill out the form below and register a new chance to evolve your  ';
$define['contato-form_trabalhe_conosco-descricao-strong'] = 'professional career';
$define['contato-form-input-#nome-placeholder'] = 'Name';
$define['contato-form-input-#email-placeholder'] = 'E-mail';
$define['contato-form-input-#telefone-placeholder'] = 'Telephone';
$define['contato-form-botoes-label'] = 'Attach Curriculum';
$define['contato-form-botoes-label-small'] = '*Word or PDF format';
$define['contato-form-botoes-enviar_form'] = 'Submit';

// ===*Home*===
	$define['home-banner-titulo'] = ' Building';
	$define['home-banner-titulo-space-2'] = 'solutions for over ';
	$define['home-banner-titulo-space-3'] = '25 years';
	$define['home-banner-descricao'] = 'We have qualified professionals and a technical team focused on quality and excellence in the execution of works and engineering projects.';

	$define['home-home_2-titulo'] = 'SERVICES';
	$define['home-home_2-areas_atuacao-descricao-civil '] = 'Civil';
	$define['home-home_2-areas_atuacao-descricao-eletrica '] = 'Electrical';
	$define['home-home_2-areas_atuacao-descricao-hidraulica '] = 'HYDRAULIC';
	$define['home-home_2-areas_atuacao-descricao-mecanica '] = 'MECHANICAL';
	$define['home-home_2-areas_atuacao-descricao-sistemas '] = 'SYSTEMS';
	$define['home-home_2-ahref '] = 'See more';

	$define['home-home_projetos-banner-titulo'] = 'Follow our main ';
	$define['home-home_projetos-banner-titulo-strong '] = 'Projects';
	$define['home-home_projetos-banner-descricao_top '] = 'Follow the main projects developed by Hersa Engenharia
';
	$define['home-home_projetos-intro-titulo '] = 'Follow our main';
	$define['home-home_projetos-intro-titulo-strong '] = 'Projects';
	$define['home-home_projetos-intro-descricao_top'] = 'Follow the main projects developed by Hersa Engenharia
';
	$define['home-home_projetos-info-VerTodos'] = 'See more';

	$define['home-home_clientes-titulo'] = 'MAIN CUSTOMERS
	';
	$define['home-home_clientes-descricao '] = 'We have among our main customers some of the largest and most important companies in Brazil';

	$define['home-home_noticias-titulo '] = 'News';
	$define['home-home_noticias-descricao '] = 'Stay on top of everything that happens at Hersa Engenharia';
	
	$define['home-home_noticias-ler_mais'] = 'See more';
	$define['home-home_noticias-ir_noticias'] = 'More news';



// ===*Index *===

$define['index-header-menu-home'] = 'Home';
$define['index-header-menu-institucional'] = 'Institutional';
$define['index-header-menu-areas-atuacao'] = 'Services';
$define['index-header-menu-projetos'] = 'Projects'; 
$define['index-header-menu-clientes'] = 'Customers';
$define['index-header-menu-qsms'] = 'QSMS';
$define['index-header-menu-noticias'] = 'News';
$define['index-header-menu-compliance'] = 'Compliance';
$define['index-header-menu-contato'] = 'Contact';


$define['index-header-menu-hamburguer-home'] = 'Home';
$define['index-header-menu-hamburguer-institucional'] = 'Institucional';
$define['index-header-menu-hamburguer-areas-atuacao'] = 'Services';
$define['index-header-menu-hamburguer-projetos'] = 'Projects';
$define['index-header-menu-hamburguer-clientes'] = 'Customers';
$define['index-header-menu-hamburguer-qsms'] = 'QSMS';
$define['index-header-menu-hamburguer-noticias'] = 'News';
$define['index-header-menu-hamburguer-compliance'] = 'Compliance';
$define['index-header-menu-hamburguer-contato'] = 'Contact';

$define['index-section-lvl1-home'] = 'Home';
$define['index-section-lvl1-institucional'] = 'Institucional';
$define['index-section-lvl1-areas-atuacao'] = 'Services';
$define['index-section-lvl1-obras'] = 'Projects';
$define['index-section-lvl1-concessoes'] = 'Concessions';
$define['index-section-lvl1-clientes'] = 'Customers';
$define['index-section-lvl1-qsms'] = 'QSMS';
$define['index-section-lvl1-noticias'] = 'News';
$define['index-section-lvl1-trabalhe-conosco'] = 'Contact';
$define['index-section-lvl1-compliance'] = 'Compliance';
$define['index-section-lvl1-contato'] = 'Contact';

$define['index-section-lvl2-title-#'] = 'Services';
$define['index-section-lvl2-subtitle-#eng-civil'] = 'Civil Engineering';
$define['index-section-lvl2-description-#eng-civil'] = 'Construções e reformas prediais, comerciais e industriais, retrofit, bases e estruturas para equipamentos e infraestrutura underground.';


$define['index-section-lvl2-subtitle-#eng-ele'] = 'Electrical Engineering';
$define['index-section-lvl2-description-#eng-ele'] = 'Subestações Transformadoras de Energia, Sistemas de Geração de Energia, Instalações Elétricas em Média e Baixa Tensão, Sistemas de Eficiência Energética, Iluminação, Aterramento e Proteção contra Descargas Atmosférica – SPDA.';
$define['index-section-lvl2-subtitle-#eng-hid'] = 'Engenharia Hidráulica';
$define['index-section-lvl2-description-#eng-hid'] = 'Redes de água potável, pluvial e quente, centrais de água gelada (CAG), esgoto sanitário, sistemas de combate à incêndio e gás combustível.';
$define['index-section-lvl2-subtitle-#eng-mec'] = 'Engenharia Mecânica';
$define['index-section-lvl2-description-#eng-mec'] = 'Centrais e tubulações de fluidos, gases especiais e ar comprimido, sistemas de tratamento de efluentes industriais e sistemas de ventilação e climatização.';
$define['index-section-lvl2-subtitle-#sis_esp'] = 'Sistemas Especiais';
$define['index-section-lvl2-description-#sis_esp'] = 'Sistemas especiais de missão crítica, automação e supervisão predial, detecção, alarme e combate a incêndios, controle de acesso, CFTV, sonorização, telecomunicações, dados e imagem e cabeamento estruturado.';


$define['index-modal-cookies-p'] = 'To improve your browsing experience, we use cookies, among other technologies. In accordance with our ';
$define['index-modal-cookies-ahref'] = 'Privacy Policy';
$define['index-modal-cookies-p2'] = 'by continuing to browse, you accept these conditions. Access our Privacy Policy and see how we treat personal data at Hersa.';
$define['index-modal-cookies-button'] = ' To accept';



$define['index-footer-span'] = 'We area a multidisplinary company operanting in the civil, electrical, hydraulic, mechanical and special systems engineering segments, formed by professional with great capactity and experience in the management and execution of public and private works.';


$define['index-footer-menu-menu'] = 'Menu';
$define['index-footer-menu-institucional'] = 'Institucional';
$define['index-footer-menu-areas-atuacao'] = 'Services';
$define['index-footer-menu-projetos'] = 'Projects';
$define['index-footer-menu-clientes'] = 'Customers';
$define['index-footer-menu-qsms'] = 'QSMS';
$define['index-footer-menu-noticias'] = 'News';
$define['index-footer-menu-compliance'] = 'Compliance';
$define['index-footer-menu-contato'] = 'Contact';


$define['index-footer-contatos_footer-h3'] = 'Contact us';
$define['index-footer-contatos_footer-span-email'] = 'contato@hersa.com.br';
$define['index-footer-contatos_footer-span-telefone'] = '55 11 2603 4178';
$define['index-footer-contatos_footer-span-rua'] = 'Rua Padre Raposo, 497 ';
$define['index-footer-contatos_footer-span-bairro'] = 'Mooca | CEP 03118-000 ';
$define['index-footer-contatos_footer-span-cidade'] = 'São Paulo/SP';


$define['index-footer-redes_sociais-h3'] = 'Follow us ';
$define['index-footer-rodape_bottom-Pol-Priv'] = 'Privacy Policy';

// ===*Noticias *===

$define['noticias-home_noticias-titulo-linha1'] = 'Stay on top of everything ';
$define['noticias-home_noticias-titulo-linha2'] = 'that happens at';
$define['noticias-home_noticias-titulo-linha2-strong'] = 'Hersa Engenharia';

// ===*Noticias Interno*===
$define['noticias_interno_text-h3'] = 'Inauguração da Nova Subestação de Energia Elétrica da Base Aérea 
de Santa Maria/RS';

$define['noticias_interno_text-p-1'] = 'Inauguração da Nova Subestação de Energia Elétrica da Base Aérea de Santa Maria/RS';
$define['noticias_interno_text-p-2'] = 'A Hersa concluiu este mês a obra de modernização da rede aérea, cabine de medição, usina de geração de energia e KF da Base Aérea de Santa Maria/RS.';
$define['noticias_interno_text-p-3'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.';
$define['noticias_interno_text-p-4'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.';

$define['noticias_interno-#projetos_recomendados-titulo'] = 'Projects you may like';

// ===*Obras *===

$define['obras-introducao-titulo'] = 'Conheça';
$define['obras-introducao-titulo-strong'] = 'nossas obras e concessões';
$define['obras-introducao-titulo-parte2'] = 'em andamento';
$define['obras-introducao-descricao'] = 'utilize o filtro ao lado';
$define['obras-filtros-intro'] = 'Filters:';
$define['obras-campos'] = 'Categories';

$define['obras-campos-filtro-concessoes'] = 'Concessions';
$define['obras-campos-filtro-Data_Center'] = 'Datacenter e Missão Crítica';
$define['obras-campos-filtro-Defesa-Seg'] = 'Defence and Security';
$define['obras-campos-filtro-Diversos'] = 'Other';
$define['obras-campos-filtro-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['obras-campos-filtro-Manutencao'] = 'Maintenance and Facilities';
$define['obras-campos-filtro-Transportes'] = 'Transportations';
$define['obras-campos-filtro_status'] = 'Status';
$define['obras-campos-filtro_status-top'] = 'Obra Concluída';
$define['obras-campos-filtro_status-bottom'] = 'Obra em Andamento';
$define['obras-campos-filtro_cliente'] = 'Cliente';

$define['obras-obras_projeto-infos-titulo'] = 'AC CAMARGO CÂNCER CENTER';
$define['obras-obras_projeto-infos-descricao'] = 'Retrofit do Edifício Castro Alves em São Paulo/SP visando a implantação de uma nova
unidade de atendimento do AC Camargo Câncer Center. Área Total de 5.105,00 m²';

$define['obras-obras_projeto-infos-titulo-first'] = 'BANRISUL – DATACENTER';
$define['obras-obras_projeto-infos-descricao-first'] = 'Construção do Edifício Data Center do Banrisul na cidade de Porto Alegre/RS incluindo a
execução de obras civis, elétricas, mecânicas, lógicas, segurança e automação, equipamentos e PPCI.';
$define['obras-obras_projeto-infos-titulo-second'] = 'SENAI – Edif. Theobaldo de Nigris';
$define['obras-obras_projeto-infos-descricao-second'] = 'Reforma e ampliação do Edifício Theobaldo de Nigris do SENAI/SP, localizado na Av.
Paulista, 750 . Área Total: 29.186,00 m²';
$define['obras-obras_projeto-infos-titulo-third'] = 'Banco do Brasil – Datacenter Complexo Verbo Divino';
$define['obras-obras_projeto-infos-descricao-third'] = 'Modernização do Datacenter do Complexo Verbo Divino do Banco do Brasil. Sistema de
Geração: 6.000 kVA Sistema de UPS/No break: 2.0000 kVA Área: 2.500,00 m²';


$define['obras-obras_projeto-verMais'] = 'See more';

// ===*Obras Interno*===

$define['obras_interno-introducao-titulo'] = 'Conheça';
$define['obras_interno-introducao-titulo-strong'] = 'nossas _interno e concessões';
$define['obras_interno-introducao-titulo-parte2'] = 'em andamento';
$define['obras_interno-introducao-descricao'] = 'utilize o filtro ao lado';
$define['obras_interno-filtros-intro'] = 'Filters:';
$define['obras_interno-campos'] = 'Categories';

$define['obras_interno-campos-filtro-concessoes'] = 'Concessions';
$define['obras_interno-campos-filtro-Data_Center'] = 'Datacenter e Missão Crítica';
$define['obras_interno-campos-filtro-Defesa-Seg'] = 'Defence and Security';
$define['obras_interno-campos-filtro-Diversos'] = 'Other';
$define['obras_interno-campos-filtro-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['obras_interno-campos-filtro-Manutencao'] = 'Maintenance and Facilities';
$define['obras_interno-campos-filtro-Transportes'] = 'Transportations';

$define['obras_interno-campos-filtro_status'] = 'Status';
$define['obras_interno-campos-filtro_status-top'] = 'Obra Concluída';
$define['obras_interno-campos-filtro_status-bottom'] = 'Obra em Andamento';


$define['obras_interno-campos-filtro_cliente'] = 'Cliente';


$define['obras_interno-#obras_interno-banner-info-titulo'] = 'Banco do Brasil – Datacenter Complexo Verbo Divino';
$define['obras_interno-#obras_interno-banner-info-descricao'] = 'Modernização do Datacenter do Complexo Verbo Divino do Banco do Brasil. <br>Sistema de
Geração: 6.000 kVA ';
$define['obras_interno-#obras_interno-banner-info-descricao-parte-2'] = 'Sistema de UPS/No break: 2.0000 kVA';
$define['obras_interno-#obras_interno-banner-info-descricao-parte-3'] = 'Área: 2.500,00 m²';

$define['obras_interno-#obras_interno-informacoes_obra-status-top'] = 'Status:';
$define['obras_interno-#obras_interno-informacoes_obra-status-bottom'] = 'Obra em Andamento';
$define['obras_interno-#obras_interno-informacoes_obra-categoria-top'] = 'Categories:';
$define['obras_interno-#obras_interno-informacoes_obra-categoria-bottom'] = 'Datacenter e Missão Crítica';

$define['obras_interno-#projetos_recomendados-titulo'] = 'Projects you may like';
$define['obras_interno-#projetos_recomendados-recomendados-subtitulo'] = 'Ministério do Esporte';
$define['obras_interno-#projetos_recomendados-recomendados-descricao'] = 'Reforma, Construção, Operação e Manutenção das Instalações do Centro de
Treinamento...';

$define['obras_interno-#projetos_recomendados-projeto-subtitulo'] = 'CISCEA – Modernização de Sistemas de Energia e Climatização';
$define['obras_interno-#projetos_recomendados-projeto-descricao'] = 'Reforma e Modernização dos Sistemas Elétricos e de Climatização em 09
Destacamentos...';

$define['obras_interno-#projetos_recomendados-projeto2-subtitulo'] = 'Marinha – COGESN';
$define['obras_interno-#projetos_recomendados-projeto2-descricao'] = 'Construção dos edifícios de escritórios de projetos com 5 pavimentos e prédio
anexo para paióis e...';

$define['obras_interno-#projetos_recomendados-projeto3-subtitulo'] = 'CNH – Centro de Distribuição Sorocaba';
$define['obras_interno-#projetos_recomendados-projeto3-descricao'] = 'Construção do Prédio da Subestação (SUB2) do Laboratório de Geração de
Energia...';

// ===*Projetos Internos*===

$define['projetos_internos-projetos_nav-DataCenter'] = 'Data Center';
$define['projetos_internos-projetos_nav-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['projetos_internos-projetos_nav--Defesa-Seguranca'] = 'Defence and Security';
$define['projetos_internos-projetos_nav-Transportes'] = 'Transportations';
$define['projetos_internos-projetos_nav-Manutencao'] = 'Maintenance and Facilities';
$define['projetos_internos-projetos_nav-Diversos'] = 'Other';
$define['projetos_internos-projetos_nav-Concessoes'] = 'Concessions';

$define['projetos_internos-select-options-DataCenter'] = 'Data Center';
$define['projetos_internos-select-options-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['projetos_internos-select-options--Defesa-Seguranca'] = 'Defence and Security';
$define['projetos_internos-select-options-Transportes'] = 'Transportations';
$define['projetos_internos-select-options-Manutencao'] = 'Maintenance and Facilities';
$define['projetos_internos-select-options-Diversos'] = 'Other';
$define['projetos_internos-select-options-Concessoes'] = 'Concessions';
$define['projetos_internos-selected-filtro'] = 'Filters';

$define['projetos_internos-#projetos_recomendados-titulo'] = 'Projects you may like';
$define['projetos_internos-#projetos_recomendados-status'] = 'Status:';
$define['projetos_internos-#projetos_recomendados-categoria'] = 'Categories:';
// ===*Projetos*===

$define['projetos-projetos_nav-DataCenter'] = 'Data Center';
$define['projetos-projetos_nav-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['projetos-projetos_nav--Defesa-Seguranca'] = 'Defence and Security';
$define['projetos-projetos_nav-Transportes'] = 'Transportations';
$define['projetos-projetos_nav-Manutencao'] = 'Maintenance and Facilities';
$define['projetos-projetos_nav-Diversos'] = 'Other';
$define['projetos-projetos_nav-Concessoes'] = 'Concessions';

$define['projetos-projetos_wrapper-text_projetos'] = 'Meet';
$define['projetos-projetos_wrapper-text_projetos-strong'] = 'our projects';

$define['projetos-select-options-DataCenter'] = 'Data Center';
$define['projetos-select-options-Hospitais'] = 'Hospitals and Healthcare Institutions';
$define['projetos-select-options--Defesa-Seguranca'] = 'Defence and Security';
$define['projetos-select-options-Transportes'] = 'Transportations';
$define['projetos-select-options-Manutencao'] = 'Maintenance and Facilities';
$define['projetos-select-options-Diversos'] = 'Other';
$define['projetos-select-options-Concessoes'] = 'Concessions';

$define['projetos-selected-filtro'] = 'Filters';

$define['projetos-#projetos_recomendados-titulo'] = 'Projects you may like

';
$define['projetos-verMais'] = 'See more';


// ===*QSMS*===
$define['qsms-a_hersa_engenharia-title'] = 'Quality, Work Safety,';
$define['qsms-a_hersa_engenharia-title-line2'] = 'Environment and Occupational Health';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-1'] = 'Quality';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-1'] = 'We are a company committed to quality, that is why we continue to seek continuous improvement since the initial certification of the ISO 9001 series, held in 2004. Currently we have a quality management system, also certified in the standard ISO 9001:2015. Lean, practical and effective, ensures that all deliveries of final products are in accordance with applicable requirements, always with quality, safety and offering the best to our employees and customers.';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-2'] = 'Safety at Work';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-2'] = "Our management is based on preventive actions continuously verified, using the most current methods available. We are a member company of VISION ZERO and we have adopted approaches to the method since 2019. By integrating QSMS disciplines, we guarantee standardized and controlled projects without relevant deviations. Due to the dynamism that engineering projects require, we use agile management methods, such as SCRUM, ensuring speed and focus on processes, always seeking our clients' satisfaction.";

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-3'] = 'Environment';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-3'] = "We are aware of the environmental impacts of our operations, therefore, we trace actions in order to reduce them to the maximum, with the survey and monitoring of the environmental aspects and impacts involved in each project, proper disposal of generated waste, contingency plan for environmental emergencies and, thus, we guarantee the execution of sustainable projects. We have experience in participating in LEED - Leadership in Energy and Environmental Design - certified projects.";

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-4'] = 'Occupational Health';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-4'] = "We are highly committed to the occupational health of our employees, therefore we seek to offer a healthy environment through an occupational health policy aimed at controlling and mitigating occupational risks, ensuring a healthy work environment without the prevalence of occupational diseases. We believe that the guarantee of a healthy environment to everyone provides an even greater escalation of our productivity.";

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-5'] = 'Agile Methods and Innovation';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-5'] = 'With management oriented to processes and people, at the forefront of the sector, we use agile methods in the management of QSMS. We establish our strategies using OKR - Objective and Key Results, unfolding the strategic goals to all those involved, we follow the unfolding of tasks via Electronic Kanban. SCRUM guarantees the continuous flow to the process. These methods guarantee a robust, lean and effective management system.';

// ===*Sobre*===
$define['sobre-wrapper_a_hersa_engenharia-up_title'] = '25 YEARS OF QUALITY  AND PROFESSIONALISM';
$define['sobre-wrapper_a_hersa_engenharia-titulo'] = 'Hersa Engenharia';
$define['sobre-wrapper_a_hersa_engenharia-descricao-1p'] = 'Hersa Engenharia was founded in 1996 in the traditional Mooca neighborhood in São Paulo and today has over 800 projects and works completed throughout the national territory.';
$define['sobre-wrapper_a_hersa_engenharia-descricao-2p'] = 'We are a multidisciplinary company operating in the Electrical, Civil, Mechanical, Hydraulic and Special Systems Engineering segments, formed by professionals with great capacity and experience in the management and execution of public and private works.';
$define['sobre-wrapper_a_hersa_engenharia-descricao-3p'] = 'We have as main characteristic the excellence in the delivery of our works through a rigorous standard of quality, being able to carry out projects of high technical complexity. Over 25 years of existence, Hersa Engenharia has become one of the most admired companies in Brazil.';
$define['sobre-a_hersa_saiba_mais-intro-descricao'] = 'Learn more about ';
$define['sobre-a_hersa_saiba_mais-intro-descricao-strong'] = 'Hersa Engenharia';
$define['sobre-a_hersa_saiba_mais-intro-descricao-part2'] = '';

$define['sobre-a_hersa_colaboradores-titulo'] = 'OUR TEAM';
$define['sobre-a_hersa_colaboradores-colaboradores-number'] = '600';
$define['sobre-a_hersa_colaboradores-colaboradores-span'] = 'employees';

$define['sobre-missao_visao_valores-missao-titulo'] = 'Mission';
$define['sobre-missao_visao_valores-missao-descricao'] = 'Offer customers engineering solutions, transforming their needs in effective works, executed and directed by skilled and responsible people, thus ensuring quality and excellence in the provision of services.
';

$define['sobre-missao_visao_valores-visao-titulo'] = 'Vision';
$define['sobre-missao_visao_valores-visao-descricao'] = 'Be a benchmark for quality and responsibility in constructions and services of engineering';
$define['sobre-missao_visao_valores-visao-descricao-linha-span-1'] = '• Motivated, committed and results-focused team;';
$define['sobre-missao_visao_valores-visao-descricao-linha-span-2'] = '• Strong internal policy and organizational culture;';
$define['sobre-missao_visao_valores-visao-descricao-linha-span-3'] = '•  Investing in the search for the best professionals.';


$define['sobre-missao_visao_valores-valores-titulo'] = 'Values';
$define['sobre-missao_visao_valores-valores-descricao-1'] = '• Commitment';
$define['sobre-missao_visao_valores-valores-descricao-2'] = '• Professionalism';
$define['sobre-missao_visao_valores-valores-descricao-3'] = '• Initiative';
$define['sobre-missao_visao_valores-valores-descricao-4'] = '• Responsibility';
$define['sobre-missao_visao_valores-valores-descricao-5'] = '• Ethics and Transparency';


// ===*Trabalhe Conosco*===

$define['trabalhe-conosco-trabalhe_conosco_top-bg-titulo-strong'] = 'Careers';
$define['trabalhe-conosco-form_trabalhe_conosco-descricao'] = 'Fill out the form below and register a new chance to evolve your  ';
$define['trabalhe-conosco-form_trabalhe_conosco-descricao-strong'] = 'professional career';
$define['trabalhe-conosco-form-input-#nome-placeholder'] = 'Name';
$define['trabalhe-conosco-form-input-#email-placeholder'] = 'E-mail';
$define['trabalhe-conosco-form-input-#telefone-placeholder'] = 'Telephone';
$define['trabalhe-conosco-form-botoes-label'] = 'Attach Curriculum';
$define['trabalhe-conosco-form-botoes-label-small'] = '*Word or PDF format';
$define['trabalhe-conosco-form-botoes-enviar_form'] = 'Submit';








