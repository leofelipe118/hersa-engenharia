<?php

namespace App\Models;

use CRO\Model\Table;

class Parceiros extends Table
{
    protected $table = "clientes";

    public function total($filtro = "")
    {
        $query = "SELECT id FROM {$this->table} WHERE 1 ".$filtro;
        return mysqli_num_rows( $this->db->query($query) );
    }

    public function listar($filtro,$inicio,$limite)
    {
        $query = "SELECT * FROM {$this->table} WHERE 1 ".$filtro." ORDER BY nome LIMIT $inicio,$limite ";
        return $this->db->query($query);
    }

    public function excluiFoto($id)
    {
        $query = "UPDATE {$this->table} SET logo = NULL WHERE id = {$id}";
        return $this->db->query($query);
    }

    public function busca_parceiros()
    {
        $query = "SELECT id, nome FROM {$this->table} ORDER BY nome";
        return $this->db->query($query);
    }

}