<?php
/***** Exemplo de chamada para retornar um único registro
/* $query = "select * FROM clientes WHERE id = 1";
/* $reg = $db->query($query)->fetch_assoc();
 *****/
/***** Exemplo de chamada para vários registros
/***** Retorna um array associativo
/* $query = "select * FROM clientes";
/* $reg  = $db->query($query);
/* $reg->num_rows;//retorna o numero de linhas
/* foreach($reg as $r)
/* {
/*		mostra_array($r);
/* }
 *****/

if ($pag == "noticias") {
	if (!@$_GET['op'] || is_numeric(@$_GET['op'])) //listagem do blog
	{
		if (@$_GET['busca']) {
			$palavra = urldecode( @array_pop( @explode( "&busca=", $_SERVER['REQUEST_URI'] ) ) );
			$palavraBusca = limpa_nome( filtro( "string", seguranca( $palavra ) ) );
			if($palavra)
			{
				$condicao = " AND ( HTML_UnEncode(titulo_{$lang}) LIKE '%" . $palavraBusca . "%' OR HTML_UnEncode(descricao_{$lang}) LIKE '%" . $palavraBusca . "%' OR HTML_UnEncode(texto_{$lang}) LIKE '%" . $palavraBusca . "%' OR HTML_UnEncode(tags_{$lang}) LIKE '%" . $palavraBusca . "%' ) ";

			}
		} else
			$condicao = "";

		//paginação

		$pagina = @$_GET['op'] && is_numeric(@$_GET['op']) ? @$_GET['op'] : 1;
		$limite = 6;
		$inicio = ($pagina * $limite) - $limite;

		$sqlpaginas = $db->query("SELECT id FROM noticias WHERE status = 's' $condicao AND data <= NOW() ORDER BY data");
		$resul = $sqlpaginas->num_rows;
		$totalPaginas = ceil($resul / $limite);

		$sqln = "SELECT * FROM noticias WHERE status = 's' $condicao AND data <= NOW() ORDER BY data DESC LIMIT {$inicio},{$limite}";


		$regn = $db->query($sqln);
		$arrNoticias = array();
		foreach ($regn as $n)
			$arrNoticias[] = $n;

		$sqln2 = "SELECT * FROM noticias WHERE status = 's' AND principal = 's' AND data <= NOW() ORDER BY data DESC LIMIT 3";


		$regn2 = $db->query($sqln2);
		$arrNoticias2 = array();
		foreach ($regn2 as $n2)
			$arrNoticias2[] = $n2;

	} else //pagina interna do blog
	{
		$idEspec =  $_GET['id'];
		// $cond = "url = '{$url}'";
		// if ($lang != "pt")
		// 	$cond .= " OR url_{$lang} = '{$url}'";
		$sqln = "SELECT *, DATE_FORMAT(data, '%d') AS dia, DATE_FORMAT(data, '%m') AS mes, DATE_FORMAT(data, '%Y') AS ano, DATE_FORMAT(data, '%D') AS diaEn FROM noticias WHERE id = {$idEspec}";
		$n = $db->query($sqln)->fetch_assoc();

		//busca proximas
		$sqlNR = "SELECT * FROM projetos WHERE status = 's' AND id <> " . $n['id'] . " ORDER BY data LIMIT 4";
		$regnRel = $db->query($sqlNR); 
		$arrNoticiasRel = array();
		foreach ($regnRel as $nrel)
			$arrNoticiasRel[] = $nrel;
		// $regNR = $db->query($sqlNR)->fetch_assoc();
	}
}

//busca de pacotes
// $sqlCli = "SELECT * FROM pacotes where status = 's'";
// $regCli = $db->query($sqlCli);
// $arrCli = array();
// $row = mysqli_fetch_assoc($regCli);
// foreach($regCli as $c)
//     $arrCli[] = $c;

// $sqlpaginas = $db->query( "SELECT id FROM noticias WHERE status = 's' AND DATA <= NOW()" );
// $resulBlog = $sqlpaginas->num_rows;
 
// if($pag=="noticias"){
//     if($id){
//         // busca notícia
//         $sqlBlogInd = "SELECT * FROM noticias where status = 's' AND id='{$id}'";
//         $regBlogInd = $db->query($sqlBlogInd);
//         foreach($regBlogInd as $c)
//             $arrBlogInd[] = $c;
            
//         $n = $db->query($sqlBlogInd)->fetch_assoc();

//         $texto = converteTexto($n['texto_'.$lang]);
//         $texto = str_replace("<h3>", "</p><h3>", $texto);
//         $texto = str_replace("</h3>", "</h3><p>", $texto);
//         $texto = str_replace("<h2>", "</p><h2>", $texto);
//         $texto = str_replace("</h2>", "</h2><p>", $texto);

//     } else {
// 		// paginação
// 		$pagina = $_GET['op'] && is_numeric($_GET['op']) ? $_GET['op'] : 1;
// 		if($pagina > 1){$limite = 3;}else{$limite = 3;}
// 		$limite = 6;
// 		$inicio = ($pagina * $limite) - $limite;
// 		$totalPaginas = ceil( $resulBlog / $limite );

// 		$sqln = "SELECT * FROM noticias WHERE status = 's' AND data <= NOW() ORDER BY DATA DESC LIMIT {$inicio},{$limite}";

// 		if($resulBlog > 0)
// 		{
// 			$arrProjetos = array();
// 			$condLimite = " LIMIT {$inicio},{$limite} ";
// 			$sqlPosts = "SELECT * 
// 				FROM noticias
// 				WHERE status = 's'
// 				ORDER BY id DESC 
// 				{$condLimite}";
// 			$regBlog = $db->query($sqlPosts);
// 			foreach($regBlog as $c)
// 				$arrBlog[] = $c;
// 		}	
//     }



//     if($_GET['op'] || is_numeric($_GET['op']) || $_GET['op']==null)
// 	{
// 		if($_GET['id'])//pagina interna do blog
// 		{
// 			$sqln = "SELECT *, DATE_FORMAT(data, '%d') AS dia, DATE_FORMAT(data, '%m') AS mes, DATE_FORMAT(data, '%Y') AS ano FROM noticias WHERE id = '" . seguranca($_GET['op']) . "'";
// 			$n = $db->query($sqln)->fetch_assoc();
// 			//mostra_array($n);
// 			$descricao = $n["texto_" . $lang] ? $n["texto_" . $lang] : $n["texto_pt"];
// 			$texto = html_entity_decode( $descricao, ENT_QUOTES );
// 			for($x=1;$x<=6;$x++)
// 			{
// 				$texto = str_replace("<h".$x.">","</p><h".$x.">", $texto);
// 				$texto = str_replace("</h".$x.">","</h".$x."><p>", $texto);
// 				$texto = str_replace("<table","</p><table", $texto);
// 				$texto = str_replace("</table>","</table><p>", $texto);
// 			}
// 			$tags = explode(",", $n['tags']);
			
// 			//busca as noticias relacionadas por tags
// 			$sqlrel = "SELECT id, foto, titulo_pt, titulo_en, titulo_es FROM noticias WHERE status = 's' AND data <= NOW() AND id != '" . seguranca($_GET['op']) . "'";
// 			$condtags = "";
// 			if($tags)
// 			{
// 				foreach($tags as $t)
// 				{
// 					$tag = trim ( $t );
// 					$condtags .= $condtags ? " OR " : "AND (";
// 					$condtags .= " tags LIKE '%" . $tag . "%' ";
// 				}
// 			}
// 			$condtags .= $condtags ? ")" : "";
// 			$sqlrel .= $condtags . " ORDER BY DATA DESC LIMIT 3";
// 			$regrel = $db->query($sqlrel);
// 			$arrNoticias = array();
// 			foreach($regrel as $not)
// 				$arrNoticias[] = $not;

// 			$linkCompartilhamento = $protocolo . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
// 		}
// 		else//listagem do blog
// 		{
// 			$condicao = "";
// 			$busca = "";
// 			if( strpos($_SERVER['REQUEST_URI'],"busca=") !== false )
// 			{
// 				$busca = seguranca( urldecode( @array_pop( @explode( "busca=", $_SERVER['REQUEST_URI'] ) ) ) );
// 				$palavra = tira_acento( filtro( "string", $busca ) );
// 				$condicao = " AND ( HTML_UnEncode(titulo_".$lang.") LIKE '%".$palavra."%' OR HTML_UnEncode(descricao_".$lang.") LIKE '%".$palavra."%' OR HTML_UnEncode(texto_".$lang.") LIKE '%".$palavra."%' ) ";
// 			}
// 			$tag = "";
// 			if( strpos($_SERVER['REQUEST_URI'],"tag=") !== false )
// 			{
// 				$tag = seguranca( urldecode( @array_pop( @explode( "tag=", $_SERVER['REQUEST_URI'] ) ) ) );
// 				$palavra = filtro( "string", $tag );
// 				$condicao = " AND tags LIKE '%" . $palavra . "%' ";
// 			}



// 			// $sqln = "SELECT id, foto, titulo_pt, titulo_en, titulo_es FROM noticias WHERE status = 's' AND data <= NOW() {$condicao} ORDER BY DATA DESC LIMIT 11";
// 			$regn = $db->query($sqln);
// 			$arrNoticias = array();
// 			foreach($regn as $n)
// 				$arrNoticias[] = $n;
			
// 		}
// 	}
// }

if($pag=="home" || $pag==""){

		$sqlnBan = "SELECT * FROM banners WHERE status = 's' AND tipo = 'h' ORDER BY ordem";

		$regBanner = $db->query($sqlnBan);
		foreach($regBanner as $b)
			$arrBan[] = $b;

		$sqlnProj = "SELECT * FROM projetos WHERE status = 's' ORDER BY data DESC";

		$regProjeto = $db->query($sqlnProj);
		foreach($regProjeto as $p)
			$arrProj[] = $p;
   
		$sqln = "SELECT * FROM noticias WHERE status = 's' AND data <= NOW() ORDER BY DATA DESC LIMIT 3";

		$regBlog = $db->query($sqln);
		foreach($regBlog as $c)
			$arrBlog[] = $c;

		$sqlnCli = "SELECT * FROM clientes WHERE status = 's' AND principal = 's'";
		$regCli = $db->query($sqlnCli);
		foreach($regCli as $b)
			$arrCliPrincipal[] = $b;

		// mostra_array($arrBan);
		// echo 'teste';
		// die;
}

if ($pag == "projetos") {
	if (!@$_GET['id']) //listagem dos projetos
	{
		if(@$_GET['op'] == "busca")
		{
			// echo $palavra;
			// die;
			$palavra = urldecode( @array_pop( @explode( "?busca=", $_SERVER['REQUEST_URI'] ) ) );
			$palavraBusca = "'".$palavra."'";
			if($palavra)
				$condicao = " AND categoria = {$palavraBusca}";
			$sqln = "SELECT * FROM projetos WHERE status = 's' $condicao AND data <= NOW() ORDER BY data DESC";
			$totalPaginas = 1;
		} else if(@$_GET['op'] == "pesquisa")
		{
			$busca = seguranca( urldecode( @array_pop( @explode( "?pesquisa=", $_SERVER['REQUEST_URI'] ) ) ) );
			$palavra = tira_acento( filtro( "string", $busca ) );
			$condicao = " AND ( HTML_UnEncode(titulo_pt) LIKE '%".$palavra."%' OR HTML_UnEncode(texto_pt) LIKE '%".$palavra."%' ) ";
			
			$sqln = "SELECT * FROM projetos WHERE status = 's' AND DATA <= NOW() " . $condicao;









			// $linguagem = @$_GET['lang'] ? @$_GET['lang'] : "pt";
			// // die;
			// $palavra_bus = urldecode( @array_pop( @explode( "?pesquisa=", $_SERVER['REQUEST_URI'] ) ) );
			// $palavra = $palavra_bus;
			// // echo $palavra;
			// // die;
			// if($palavra)
			// 	$condicao = " AND ( HTML_UnEncode(titulo_".$linguagem.") LIKE '%".$palavra."%' OR HTML_UnEncode(texto_".$linguagem.") LIKE '%".$palavra."%') ";
			// $sqln = "SELECT * FROM projetos WHERE status = 's' $condicao AND data <= NOW() ORDER BY data DESC";
			// echo $sqln;
			// die;
			$totalPaginas = 1;

		} else {
			//paginação
	
			$pagina = @$_GET['op'] && is_numeric(@$_GET['op']) ? @$_GET['op'] : 1;
			$limite = 6;
			$inicio = ($pagina * $limite) - $limite;
	
			$sqlpaginas = $db->query("SELECT id FROM projetos WHERE status = 's' $condicao AND data <= NOW() ORDER BY data");
			$resul = $sqlpaginas->num_rows;
			$totalPaginas = ceil($resul / $limite);
	
			$sqln = "SELECT * FROM projetos WHERE status = 's' $condicao AND data <= NOW() ORDER BY data DESC LIMIT {$inicio},{$limite}";



		}



		$regn = $db->query($sqln);
		$arrNoticias = array();
		foreach ($regn as $n)
			$arrNoticias[] = $n;

		// mostra_array($arrNoticias);
		// die;

		$sqln2 = "SELECT * FROM projetos WHERE status = 's' AND data <= NOW() ORDER BY data DESC LIMIT 6";


		$regn2 = $db->query($sqln2);
		$arrNoticias2 = array();
		foreach ($regn2 as $n2)
			$arrNoticias2[] = $n2;
	} else //pagina interna do blog
	{
		$idEspec =  $_GET['id'];
		// $cond = "url = '{$url}'";
		// if ($lang != "pt")
		// 	$cond .= " OR url_{$lang} = '{$url}'";
		$sqln = "SELECT *, DATE_FORMAT(data, '%d') AS dia, DATE_FORMAT(data, '%m') AS mes, DATE_FORMAT(data, '%Y') AS ano, DATE_FORMAT(data, '%D') AS diaEn FROM projetos WHERE id = {$idEspec}";
		$n = $db->query($sqln)->fetch_assoc();
		//mostra_array($n);

		// $texto = $n['texto_' . $lang] ? $n['texto_' . $lang] : $n['texto_pt'];
		// $texto = converteTexto($texto);

		// // $tags = explode(",", $n['tags_' . $lang] ? $n['tags_' . $lang] : $n['tags_pt']);

		// $linkCompartilhamento = $protocolo . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


		// Galeria
		$sqlGalProj = "SELECT * FROM projetos_galeria WHERE id_projeto = ".@$_GET['id']." AND status = 's' ORDER BY ordem DESC, id";
		// echo "Testando 123";
		// echo $sqlGalProj;
		$regGalProj = $db->query($sqlGalProj);
		// mostra_array($regGalProj);

		//busca proximas
		$sqlNR = "SELECT * FROM projetos WHERE status = 's' AND id <> " . $n['id'] . " ORDER BY data LIMIT 4";
		$regnRel = $db->query($sqlNR);
		$arrNoticiasRel = array();
		foreach ($regnRel as $nrel)
			$arrNoticiasRel[] = $nrel;
		// $regNR = $db->query($sqlNR)->fetch_assoc();
	}

	// $sqlnBan = "SELECT * FROM projetos WHERE status = 's'";

	// $regBanner = $db->query($sqlnBan);
	// foreach($regBanner as $b)
	// 	$arrBan[] = $b;
}

if($pag=="clientes"){

	$sqlnCli['h'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'h'";
	$regCli['h'] = $db->query($sqlnCli['h']);
	foreach($regCli['h'] as $b)
		$arrCli['h'][] = $b;

	$sqlnCli['d'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'd'";
	$regCli['d'] = $db->query($sqlnCli['d']);
	foreach($regCli['d'] as $b)
		$arrCli['d'][] = $b;

	$sqlnCli['t'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 't'";
	$regCli['t'] = $db->query($sqlnCli['t']);
	foreach($regCli['t'] as $b)
		$arrCli['t'][] = $b;

	$sqlnCli['dc'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'dc'";
	$regCli['dc'] = $db->query($sqlnCli['dc']);
	foreach($regCli['dc'] as $b)
		$arrCli['dc'][] = $b;

	$sqlnCli['i'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'i'";
	$regCli['i'] = $db->query($sqlnCli['i']);
	foreach($regCli['i'] as $b)
		$arrCli['i'][] = $b;

	$sqlnCli['o'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'o'";
	$regCli['o'] = $db->query($sqlnCli['o']);
	foreach($regCli['o'] as $b)
		$arrCli['o'][] = $b;

	$sqlnCli['p'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'p'";
	$regCli['p'] = $db->query($sqlnCli['p']);
	foreach($regCli['p'] as $b)
		$arrCli['p'][] = $b;

	$sqlnCli['c'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'c'";
	$regCli['c'] = $db->query($sqlnCli['c']);
	foreach($regCli['c'] as $b)
		$arrCli['c'][] = $b;

	$sqlnCli['s'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 's'";
	$regCli['s'] = $db->query($sqlnCli['s']);
	foreach($regCli['s'] as $b)
		$arrCli['s'][] = $b;

	$sqlnCli['mf'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'mf'";
	$regCli['mf'] = $db->query($sqlnCli['mf']);
	foreach($regCli['mf'] as $b)
		$arrCli['mf'][] = $b;

	$sqlnCli['cc'] = "SELECT * FROM clientes WHERE status = 's' AND categoria = 'cc'";
	$regCli['cc'] = $db->query($sqlnCli['cc']);
	foreach($regCli['cc'] as $b)
		$arrCli['cc'][] = $b;


		

	// mostra_array($arrBan);
	// echo 'teste';
	// die;
}

    
//busca de especialidades
// $sqlEspec = "SELECT * FROM especialidades where status = 's'";
// $regEspec = $db->query($sqlEspec);
// $arrEspec = array();
// $row = mysqli_fetch_assoc($regEspec);
// foreach($regEspec as $c)
// 	$arrEspec[] = $c;

// //busca os servicos da home
// $sqls = "SELECT * FROM servicos WHERE status = 's' ORDER BY data";
// $regs = $db->query($sqls);
// $arrServicos = array();
// foreach($regs as $s)
// 	$arrServicos[] = $s;


// $sqlt = "SELECT * FROM trabalhos WHERE status ='s' ORDER BY id";
// $regTrabalhos = $db->query($sqlt);
// $arrTrabalhos = array();
// foreach($regTrabalhos as $t)
// 	$arrTrabalhos[] = $t;