<?php

namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;

class AnalyticsController extends Action
{
    public function analytics()
    {

        $VIEW_ID = "223866442";
        $sitecliente = "https://www.directlog.com.br/";

        require_once "vendor/CRO/BaseAdmin/google-api/vendor/autoload.php";
        $KEY_FILE_LOCATION = 'vendor/CRO/BaseAdmin/google-api/service-account-credentials.json';
        $client = new \Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new \Google_Service_AnalyticsReporting($client);

        $response7 = $this->getReport($analytics, date("Y-m-d", strtotime("-7 days")), date("Y-m-d"), $VIEW_ID, $sitecliente);
        $response30 = $this->getReport($analytics, date("Y-m-d", strtotime("-30 days")), date("Y-m-d"), $VIEW_ID,
            $sitecliente);

        $this->view->analytics7 = $this->printResults($response7, $sitecliente);
        $this->view->analytics30 = $this->printResults($response30, $sitecliente);

        $this->render("analytics",false);

    }

    public function nomeOrigem($origem)
    {
        $nomemidia = "";
        switch ($origem) {
            case 'Direct':
                $nomemidia = "Direto";
                break;

            case 'Organic Search':
                $nomemidia = "Orgânico";
                break;

            case 'Paid Search':
                $nomemidia = "Adwords";
                break;

            case 'Display':
                $nomemidia = "Display";
                break;

            case 'Referral':
                $nomemidia = "Sites Diversos";
                break;

            case '(Other)':
                $nomemidia = "Outros";
                break;

            case 'Social':
                $nomemidia = "Mídias Sociais";
                break;

            default:
                $nomemidia = $origem;
                break;

        }

        return $nomemidia;

    }

    public function printResults($reports, $sitecliente) {

        $registros = array();
        $paginas = array();
        $midia = array();
        $dispositivo = array();
        $cidade = array();

        $report = $reports[0];
        $rows = $report->getData()->getRows();
        $totais = $report->getData()->getTotals();
        foreach ($totais as $total) {

            $registros["sessions"] = $total->getValues()[0];
            $registros["pageviews"] = $total->getValues()[1];
        }

        for($a=0; $a < count($rows); $a++)
        {
            $row = $rows[$a];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();

            $sessoesval = 0;
            $pagesval = 0;

            if(@$auxdimensao != $dimensions[1])
            {
                $paginas[$dimensions[1]]["titulo"] = $dimensions[1];
                $paginas[$dimensions[1]]["url"] = $sitecliente.$dimensions[0];

                $paginas[$dimensions[1]]["sessions"] = $metrics[0]->getValues()[0];
                $paginas[$dimensions[1]]["pageviews"] = $metrics[0]->getValues()[1];
            }
            else
            {
                $paginas[$dimensions[1]]["sessions"] = $paginas[$dimensions[1]]["sessions"] + $metrics[0]->getValues()[0];
                $paginas[$dimensions[1]]["pageviews"] =  $paginas[$dimensions[1]]["pageviews"] + $metrics[0]->getValues()[1];
            }

            $auxdimensao = $dimensions[1];
        }

        ////// MIDIA
        $report2 = $reports[1];
        $rows = $report2->getData()->getRows();
        for($a=0; $a < count($rows); $a++)
        {
            $row = $rows[$a];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();

            $midia[$a]["sessions"] = $metrics[0]->getValues()[0];
            $midia[$a]["midia"] = $dimensions[0];
        }

        ////// DISPOSITIVOS
        $report3 = $reports[2];
        $rows = $report3->getData()->getRows();
        for($a=0; $a < count($rows); $a++)
        {
            $row = $rows[$a];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();

            $dispositivo[$a]["sessions"] = $metrics[0]->getValues()[0];
            $dispositivo[$a]["dispositivo"] = $dimensions[0];
        }

        /////// CIDADES
        $report4 = $reports[3];
        $rows = $report4->getData()->getRows();
        for($a=0; $a < count($rows); $a++)
        {
            $row = $rows[$a];
            $dimensions = $row->getDimensions();
            $metrics = $row->getMetrics();

            if($dimensions[1] == "Brazil")
            {
                $cidade[$a]["sessions"] = $metrics[0]->getValues()[0];
                $cidade[$a]["cidade"] = $dimensions[0];
            }
        }

        $pag = array();
        $count = 0;
        foreach($paginas as $pagina)
        {
            $pag[$count]["pageviews"]   = $pagina["pageviews"];
            $pag[$count]["titulo"]      = $pagina["titulo"];
            $pag[$count]["url"]         = $pagina["url"];
            $pag[$count]["sessions"]    = $pagina["sessions"];

            $count++;
        }

        $registros["midia"] = $midia;
        $registros["dispositivo"] = $dispositivo;
        $registros["cidade"] = $cidade;
        $registros["paginas"] = $pag;

        arsort($registros["paginas"]);
        arsort($registros["dispositivo"]);
        arsort($registros["midia"]);
        arsort($registros["cidade"]);

        return $registros;
    }

    public function getReport($analytics, $mesinicio, $mesfinal, $VIEW_ID, $sitecliente) {

        // Replace with your view ID, for example XXXX.
        $VIEW_ID = $VIEW_ID;
        $sitecliente = $sitecliente;


        // Create the DateRange object.
        $mes = new \Google_Service_AnalyticsReporting_DateRange();
        $mes->setStartDate($mesinicio);
        $mes->setEndDate($mesfinal);

        // Create the Metrics object.
        $pageviews = new \Google_Service_AnalyticsReporting_Metric();
        $pageviews->setExpression("ga:pageviews");
        $pageviews->setAlias("pageviews");

        $sessions = new \Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");
        $sessions->setAlias("sessions");



        //Create the Dimensions object.
        $urls = new \Google_Service_AnalyticsReporting_Dimension();
        $urls->setName("ga:pagePath");

        $titulos = new \Google_Service_AnalyticsReporting_Dimension();
        $titulos->setName("ga:pageTitle");

        $sources = new \Google_Service_AnalyticsReporting_Dimension();
        $sources->setName("ga:channelGrouping");

        $devices = new \Google_Service_AnalyticsReporting_Dimension();
        $devices->setName("ga:deviceCategory");

        $cidades = new \Google_Service_AnalyticsReporting_Dimension();
        $cidades->setName("ga:city");

        $paises = new \Google_Service_AnalyticsReporting_Dimension();
        $paises->setName("ga:country");


        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($VIEW_ID);
        $request->setDateRanges($mes);
        $request->setDimensions(array($urls,$titulos));
        $request->setMetrics(array($sessions,$pageviews));

        // Create the ReportRequest object.
        $request2 = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request2->setViewId($VIEW_ID);
        $request2->setDateRanges($mes);
        $request2->setDimensions(array($sources));
        $request2->setMetrics(array($sessions));

        // Create the ReportRequest object.
        $request3 = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request3->setViewId($VIEW_ID);
        $request3->setDateRanges($mes);
        $request3->setDimensions(array($devices));
        $request3->setMetrics(array($sessions));

        // Create the ReportRequest object.
        $request4 = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request4->setViewId($VIEW_ID);
        $request4->setDateRanges($mes);
        $request4->setDimensions(array($cidades,$paises));
        $request4->setMetrics(array($sessions));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request,$request2,$request3,$request4) );
        return $analytics->reports->batchGet( $body );

    }

}