<?php
$nomeDaEmpresa = 'Hersa Engenharia';
$bread_solucoes = "<li><a onclick='toggleMenu();openSubMenu(\"m2-solucoes\",2, document.getElementById(\"menu-btn-soluc\"))' style='cursor: pointer;'>Soluções</a></li>";
$bread_home = "<li><a href='".$linkcanonical."pt'>Início</a></li>";
$bread_fiscal = "<li><a onclick='toggleMenu();openSubMenu(\"m2-solucoes\",2, document.getElementById(\"menu-btn-soluc\"));openSubMenu(\"m3-comp-fiscal\",3, document.querySelector(\".item.comp_fiscal\"))' style='cursor: pointer;'>Compliance Fiscal</a></li>";
$bread_hcm = "<li><a onclick='toggleMenu();openSubMenu(\"m2-solucoes\",2, document.getElementById(\"menu-btn-soluc\"));openSubMenu(\"m3-comp-hcm\",3, document.querySelector(\".item.comp_hcm\"))' style='cursor: pointer;'>Compliance HCM</a></li>";
$bread_serv_esp = "<li><a onclick='toggleMenu();openSubMenu(\"m2-solucoes\",2, document.getElementById(\"menu-btn-soluc\"));openSubMenu(\"m3-serv-especializados\",3, document.querySelector(\".item.serv-especializados\"))' style='cursor: pointer;'>Compliance HCM</a></li>";
if ( $pag != "" && $pag != 'home')
{
    switch($pag)
    {

        case "sobre":
        $arq = "sobre.php";
        $title = $nomeDaEmpresa." | Institucional";
        $description = "A Confidence IT Services é uma empresa conectada com o futuro. Temos por missão oferecer produtos e serviços com tecnologia de ponta nas áreas fiscal e contábil.";
        $breadcrumb = $bread_home.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">A Confidence</a></h3>';
        break;

        case "obras":
            if(@$op){
                $arq = "obra_interno.php";
                $title = "Veja nossas obras! - ".$nomeDaEmpresa;
                $description = "Conteúdo de confiança e credibilidade para empresários de pequenas, médias e grandes empresas. O Blog da Confidence IT Services traz para você as últimas notícias e dicas referente a área de tecnologia da informação.";
                $breadcrumb = '';
			} else {
                $arq = "obras.php";
                $title = "Veja nossas obras! - ".$nomeDaEmpresa;
                $description = "A Confidence IT Services é uma empresa conectada com o futuro. Temos por missão oferecer produtos e serviços com tecnologia de ponta nas áreas fiscal e contábil.";
                $breadcrumb = $bread_home.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">A Confidence</a></h3>';
            }
        break;

        case "areas-de-atuacao":
        $arq = "area_atuacao.php";
        $title = $nomeDaEmpresa." | Areas de atuação";
        $description = "Conheça os cases de sucesso da Confidence IT Services. Entre eles estão Complience Soluções Fiscais, Sal Cisne, Aceco TI, Venco e muito mais. A Solução Fiscal da COMPLIANCE é a única Solução 100% web comercializada em “cloud” e “on premises” certificada pela ORACLE.";
        $breadcrumb = $bread_home.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Cases</a></h3>';
        break;

        case "contato":
        $arq = "contato.php";
        $title = $nomeDaEmpresa." | Contato";
        $description = "Como você prefere falar com a gente?";
        $breadcrumb = $bread_home.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Contato</a></h3>';
        break;

        case "trabalhe-conosco":
        $arq = "trabalhe_conosco.php";
        $title = $nomeDaEmpresa." | Trabalhe Conosco";
        $description = "Preencha o formulário abaixo e cadastre uma nova chance em evoluir sua carreira profissional.";
        $breadcrumb = $bread_home.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Trabalhe Conosco</a></h3>';
        break;
 
        case "projetos":
            if(@$op=="busca"){
                $arq = "projetos.php";
                $title = $nomeDaEmpresa." | Projetos";
                $description = "Fique por dentro de tudo que acontece na Hersa Engenharia";
            } else {
                if(@$id){
                    $arq = "projetos_interno.php";
                    $title = "Veja nossas notícias! - ".$nomeDaEmpresa;
                    $description = "Conteúdo de confiança e credibilidade para empresários de pequenas, médias e grandes empresas. O Blog da Confidence IT Services traz para você as últimas notícias e dicas referente a área de tecnologia da informação.";
                } else {
                    $arq = "projetos.php";
                    $title = $nomeDaEmpresa." | Projetos";
                    $description = "Fique por dentro de tudo que acontece na Hersa Engenharia";
                }
            }
        break;

        case "clientes":
        $arq = "clientes.php";
        $title = $nomeDaEmpresa." | Nossos clientes";
        $description = "Por mais de 25 anos, realizamos muitas parcerias com grandes empresas do país.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Erp Soluções Inteligentes</a></h3>';
        break;

        case "qsms":
        $arq = "qsms.php";
        $title = $nomeDaEmpresa." | QSMS";
        $description = "QSMS Qualidade, Segurança do Trabalho, Meio Ambiente e Saúde Ocupacional";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Erp Soluções Inteligentes</a></h3>';
        break;

        case "noticias":
            if(@$id){
                $arq = "noticias_interno.php";
                $title = "Veja nossas notícias! - ".$nomeDaEmpresa;
                $description = "Conteúdo de confiança e credibilidade para empresários de pequenas, médias e grandes empresas. O Blog da Confidence IT Services traz para você as últimas notícias e dicas referente a área de tecnologia da informação.";
            } else {
                $arq = "noticias.php";
                $title = $nomeDaEmpresa." | Notícias";
                $description = "Fique por dentro de tudo que acontece na Hersa Engenharia";
                $breadcrumb = $bread_home.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">A Confidence</a></h3>';
            }
        break;

        case "compliance":
        $arq = "compliance.php";
        $title = $nomeDaEmpresa." | Compliance";
        $description = "Conheça nossas políticas de compliance.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Treinamentos</a></h3>';
        break;

        case "calculadora-fiscal":
        $arq = "calculadoraFiscal.php";
        $title = $nomeDaEmpresa." | Calculadora Fiscal";
        $description = "Calculadora Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Calculadora Fiscal</a></h3>';
        break;

        case "calendario-fiscal":
        $arq = "calendarioFiscal.php";
        $title = $nomeDaEmpresa." | Calendário Fiscal";
        $description = "Calendário Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Calendário Fiscal</a></h3>';
        break;

        case "ciap-e-dirf":
        $arq = "ciapDirf.php";
        $title = $nomeDaEmpresa." | CIAP e DIRF";
        $description = "CIAP - Controle de Crédito do Ativo Permanente - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence CIAP e DIRF</a></h3>';
        break;

        case "ecf":
        $arq = "ecf.php";
        $title = $nomeDaEmpresa." | Escrituração contábil e fiscal";
        $description = "ECF - Escrituração Contábil e Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Escrituração contábil e fiscal</a></h3>';
        break;

        case "e-credac":
        $arq = "eCredAc.php";
        $title = $nomeDaEmpresa." | E-credAc";
        $description = "E-credAc - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence E-credAc</a></h3>';
        break;

        case "efd-contribuicoes":
        $arq = "efdContribuicoes.php";
        $title = $nomeDaEmpresa." | EFD-Contribuições";
        $description = "EFD-Contribuições - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence EFD-Contribuições</a></h3>';
        break;

        case "efd-reinf":
        $arq = "efdReinf.php";
        $title = $nomeDaEmpresa." | EFD – REINF";
        $description = "Confidence EFD – REINF - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence EFD – REINF</a></h3>';
        break;

        case "efd-reinf-faq":
        $arq = "efdReinfFaq.php";
        $title = $nomeDaEmpresa." | EFD – REINF | FAQ";
        $description = "FAQ EFD – REINF - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence EFD – REINF | FAQ</a></h3>';
        break;

        case "fcont":
        $arq = "fcont.php";
        $title = $nomeDaEmpresa." | FCONT";
        $description = "FCONT - Confidence IT Services | O FCONT é uma escrituração, das contas patrimoniais e de resultado, em partidas dobradas, que considera os métodos e critérios contábeis vigentes em 31.12.2007.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence FCONT</a></h3>';
        break;

        case "impostos-retidos":
        $arq = "impostosRetidos.php";
        $title = $nomeDaEmpresa." | Impostos retidos";
        $description = "Impostos retidos - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Impostos retidos</a></h3>';
        break;

        case "integracao-oracle-cloud":
        $arq = "integracaoOracleCloud.php";
        $title = $nomeDaEmpresa." | Integrações Oracle ERP Cloud";
        $description = "Integração Oracle ERP Cloud - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Integrações Oracle ERP Cloud</a></h3>';
        break;

        case "inteligencia-fiscal":
        $arq = "inteligenciaFiscal.php";
        $title = $nomeDaEmpresa." | Inteligência Fiscal";
        $description = "Inteligência Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Inteligência Fiscal</a></h3>';
        break;

        case "obrigacoes":
        $arq = "obrigacoes.php";
        $title = $nomeDaEmpresa." | Obrigações Federais, Estaduais e Municipais";
        $description = "Obrigações Federais, Estaduais e Municipais - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Obrigações Federais, Estaduais e Municipais</a></h3>';
        break;

        case "per-dcomp-ipi":
        $arq = "perDcompIpi.php";
        $title = $nomeDaEmpresa." | PER - DCOMP - IPI";
        $description = "PER DCOMP IPI - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence PER - DCOMP - IPI</a></h3>';
        break;

        case "sped-contabil":
        $arq = "spedContabil.php";
        $title = $nomeDaEmpresa." | SPED Contábil";
        $description = "SPED Contábil - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence SPED Contábil</a></h3>';
        break;

        case "sped-fiscal":
        $arq = "spedFiscal.php";
        $title = $nomeDaEmpresa." | SPED Fiscal";
        $description = "SPED Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence SPED Fiscal</a></h3>';
        break;

        case "sped-fiscal-k":
        $arq = "spedFiscalK.php";
        $title = $nomeDaEmpresa." | SPED Fiscal - Bloco K";
        $description = "Garanta a Entrega do Bloco K no Prazo - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_fiscal.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence SPED Fiscal - Bloco K</a></h3>';
        break;

        case "sesmt":
        $arq = "sesmt.php";
        $title = $nomeDaEmpresa." | SESMT";
        $description = "Serviços Especializados em Engenharia de Segurança e em Medicina do Trabalho - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_hcm.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence SESMT</a></h3>';
        break;

        case "ponto-eletronico-web":
        $arq = "pontoEletronicoWeb.php";
        $title = $nomeDaEmpresa." | Ponto Eletrônico Web";
        $description = "Ponto Eletrônico Web - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_hcm.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Ponto Eletrônico Web</a></h3>';
        break;

        case "solucao-folha-pagamento":
        $arq = "solucaoFolhaPagamento.php";
        $title = $nomeDaEmpresa." | Solução folha de pagamento";
        $description = "Sistema completo para gestão de pessoas - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_hcm.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Solução folha de pagamento</a></h3>';
        break;

        case "e-social":
        $arq = "eSocial.php";
        $title = $nomeDaEmpresa." | e-Social";
        $description = "A compliance fiscal é a solução completa para o e-social - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_hcm.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence e-Social</a></h3>';
        break;

        case "e-social-faq":
        $arq = "eSocialFaq.php";
        $title = $nomeDaEmpresa." | e-Social | FAQ";
        $description = "FAQ e-Social - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_hcm.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence e-Social | FAQ</a></h3>';
        break;

        case "bpo-contabil-e-fiscal":
        $arq = "bpoContabilFiscal.php";
        $title = $nomeDaEmpresa." | BPO Contábil e Fiscal";
        $description = "Outsourcing Contábil e Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_serv_esp.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence BPO Contábil e Fiscal</a></h3>';
        break;

        case "geracao-de-obrigacoes-acessorias":
        $arq = "geracaoObrigacoesAcessorias.php";
        $title = $nomeDaEmpresa." | Geração e validação do SPED";
        $description = "Geração e validação do SPED Contribuições – SPED Fiscal - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_serv_esp.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Geração e validação do SPED</a></h3>';
        break;

        case "recuperacao-de-xml":
        $arq = "recuperacaoXml.php";
        $title = $nomeDaEmpresa." | Recuperação de XML";
        $description = "Esteja em Conformidade - Confidence IT Services | Sistema de gestão, Sistema de compras, Sistema de gestão online, Sistema de gestão de pessoas, Sistema de gestão para pequenas empresas, Sistema de automação comercial e muito mais.";
        $breadcrumb = $bread_home.' / '.$bread_solucoes.' / '.$bread_serv_esp.' / <li><h3 style="font-size: 20px; line-height: 1.5; margin-bottom: 0;"><a href="'.$linkcanonical.$lang.'/'.$pag.'">Confidence Recuperação de XML</a></h3>';
        break;

        case "blog":
            if(@$id){
                $arq = "blogInterno.php";
                $title = $nomeDaEmpresa." | ".$n['titulo_'.$lang];
                $description = "Conteúdo de confiança e credibilidade para empresários de pequenas, médias e grandes empresas. O Blog da Confidence IT Services traz para você as últimas notícias e dicas referente a área de tecnologia da informação.";
                $breadcrumb = '';
			} else {
                $arq = "blog.php";
                $title = $nomeDaEmpresa." | Blog";
                $description = "Conteúdo de confiança e credibilidade para empresários de pequenas, médias e grandes empresas. O Blog da Confidence IT Services traz para você as últimas notícias e dicas referente a área de tecnologia da informação.";
                $breadcrumb = '';
			}
        break;

        default:
        $arq = "home.php";
        $title = $nomeDaEmpresa;
        $description = "A Confidence IT Services é uma empresa que oferece produtos e serviços na área de tecnologia da informação, tais como, sistema de gestão, sistema de compras, sistema de gestão online, sistema de gestão de pessoas, sistema de gestão para pequenas empresas, sistema de automação comercial e muito mais";
        $breadcrumb = '';
    }
}
else{
    $arq = "home.php";
    $title = $nomeDaEmpresa;
    $description = "A Confidence IT Services é uma empresa que oferece produtos e serviços na área de tecnologia da informação, tais como, sistema de gestão, sistema de compras, sistema de gestão online, sistema de gestão de pessoas, sistema de gestão para pequenas empresas, sistema de automação comercial e muito mais";
    $breadcrumb = '';
}

// breadcrumb das páginas
if($arq != "home.php"){
    $titleBread = str_replace("Confidence IT |", "", "$title");
}
