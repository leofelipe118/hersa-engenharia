<?php

namespace App\Models;


use CRO\Model\Table;

class Notifica extends Table
{
    protected $tableSH = "registro";
    protected $tableMSG = "msg_topicos";

    public function verifSH()
    {
        $query = "SELECT id_registro FROM {$this->tableSH} WHERE finalizado = 'a' AND id_usuario = '".$_SESSION[DB_DATABASE]['id_usuario']."' ";
        $ver = mysqli_fetch_assoc( $this->db->query($query) );
        return $ver['id_registro'] ? true : false;
    }

    public function verifMsg()
    {
        $query = "SELECT nome, assunto FROM msgcontato WHERE alerta = 's' ORDER BY data";
        return  $this->db->query($query);
    }

}