<?php
namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class ProjetosController extends Action
{
    public function listar()
    {
        $dados = Container::getModel("Projetos");

        $this->view->pesquisar = "";
        $filtro = "";
        if(@$_POST['pesquisar']) {
            $this->view->pesquisar = seguranca($_POST['pesquisar']);
            $palavra = tira_acento( filtro( "string", $this->view->pesquisar ) );
            $filtro = " AND titulo_pt LIKE '%".$palavra."%' ";
        }

        $this->view->url = Bootstrap::getUrl();
        $buscaUrl = @explode("/", $this->view->url );
        $id_retorno = @array_pop( $buscaUrl );
        $pagina = is_numeric($id_retorno) ? $id_retorno : 1;
        $this->view->temPag = is_numeric($id_retorno) ? true : false;

        //verifica se tem retorno de insert
        $verif = @array_pop( $buscaUrl );
        if($verif == "ok" || $verif == "erro")
            $this->view->retorno = $verif;

        $this->view->pagina = $pagina;
        $this->view->limite = 25;
        $this->view->inicio = ( $this->view->pagina * $this->view->limite ) - $this->view->limite;
        $this->view->resul = $dados->total( $filtro );
        $this->view->totalPaginas = ceil( $this->view->resul / $this->view->limite );

        $this->view->header = "Projetos";
		$this->view->headerSmall = "Listagem de Projetos";
        $this->view->dados = $dados->listar( $filtro, $this->view->inicio, $this->view->limite );
        $this->render("listar");
    }

    public function status()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Projetos");
        $this->view->dados = $dados->alteraStatus($form['status'],$form['id'],"id");
        return false;
    }

    public function checkUrl()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Projetos");
        echo $chk = $dados->checkUrl($form['url'],$form['id']);
        return false;
    }

    public function geraUrl()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Projetos");
        $chk = $dados->geraUrl($form['titulo'],$form['id']);
        return false;
    }

    public function cadastrar()
    {
        $temID = @array_pop( @explode("/", Bootstrap::getUrl() ) );
        $dados = Container::getModel("Projetos");
        $this->view->header = "Projetos";
		$this->view->headerSmall = "Cadastro e Edição de Projetos";
		$this->view->categorias = $dados->busca_categorias();
        if(is_numeric( $temID ) )
		{
            $this->view->dados = $dados->find($temID,"id");
			$this->view->galeria = $dados->buscaGaleria( $temID );
		}
        // if(is_numeric( $temID ) )
        //     $this->view->dados = $dados->find($temID,"id");

        $this->render("cadastrar");
    }

    public function cadastro()
    {   
        $dados = Container::getModel("Projetos");
        $form = retorna_form($_POST);
        error_reporting(E_ALL);
        // mostra_array($_POST);
        // die;
        
        $form['texto_pt-string'] = str_replace("</p><p>", "<br>", $form['texto_pt-string'] );
        $form['texto_en-string'] = str_replace("</p><p>", "<br>", $form['texto_en-string'] );
        $form['texto_es-string'] = str_replace("</p><p>", "<br>", $form['texto_es-string'] );

        if( isset( $_FILES['files'] ) )
            unset( $_FILES['files'] );

		if(isset($_FILES['logo']))
        {
			$tamanho = getimagesize($_FILES['logo']['tmp_name']);
			$tamL = $tamanho[0] > 1920 ? 1920 : $tamanho[0];
			$tamA = $tamanho[1] > 1080 ? 1080 : $tamanho[1];
			$tamThumbL = $tamL > $tamA ? 480 : 270;
			$tamThumbA = $tamL > $tamA ? 270 : 480;
			
			
            $largura 	= array($tamL, $tamThumbL, 650 );
            $altura 	= array($tamA, $tamThumbA, 650 );
            $crop 		= array("","","s");
            $pb 		= array("","","");
            $caminho 	= array("../imagens/projetos/large/","../imagens/projetos/thumb/","../imagens/projetos/crop/");
            $form['foto-string'] = salvaimagem($_FILES["logo"], $largura, $altura, $caminho, $crop,$pb, forma_url( NOME_CLIENTE . "-" . $form['titulo-string'] . date("dmY") ) );
            if( $form['foto-string'] && @$form['fotoOld-cond'] )
            {
                @unlink( "../imagens/projetos/large/".$form['fotoOld-cond'] );
                @unlink( "../imagens/projetos/thumb/".$form['fotoOld-cond'] );
                @unlink( "../imagens/projetos/crop/".$form['fotoOld-cond'] );
            }
        }



        //Targeting Folder

        if($_FILES["video"]["name"])
        {
            $target_file = $_FILES["video"]["name"];
            $videoFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            
            $name_video = forma_url( NOME_CLIENTE . "-" . $form['cliente-string'] . date("dmY") ).'.'.$videoFileType;

            move_uploaded_file($_FILES["video"]["tmp_name"], "../imagens/videos/".$name_video);
            $form['video-string'] = $name_video;
                
        }



        $retorno = $dados->grava($form);
        $urlRet = $retorno ? "ok" : "erro";
        // die;

        redireciona(BASE_SITE."projetos/listar/".$urlRet."/1");
    }

    public function statusGaleria()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Projetos");
        $this->view->dados = $dados->alteraStatusGaleria($form['status'],$form['id'],"id");
        return false;
    }
    public function salvaDescricao()
	{
        $dados = Container::getModel("Projetos");
        $form = retorna_form($_POST);

        echo $dados->salvaDescricao($form);
		return false;
	}

    public function galeria()
	{
        $form = @$_POST ? retorna_form($_POST) : array();
		if( isset($form['id-cond']) )
			$form['id-cond'] = isset($form['id-cond']) ? $form['id-cond'] : "";
		if( isset($form['idioma-cond']) )
			$form['legenda_' . $form['idioma-cond'] . '-string'] = $form['legenda-cond'];
		//mostra_array($form);
		//mostra_array($_FILES);
		//die;
        if(isset($_FILES['file']))
        {
			$tamanho = getimagesize($_FILES['file']['tmp_name']);
			$tamL = $tamanho[0] > 1920 ? 1920 : $tamanho[0];
			$tamA = $tamanho[1] > 1080 ? 1080 : $tamanho[1];
			$tamCrop = $tamL > $tamA ? $tamA : $tamL;
            $largura 	= array($tamL, 480, $tamCrop );
            $altura 	= array($tamA, 220, $tamCrop );
            $crop 		= array("", "", "s");
            $pb 		= array("", "", "");
            $caminho 	= array("../imagens/projetos/large/","../imagens/projetos/thumb/","../imagens/projetos/crop/");
            $form['foto-string'] = salvaimagem($_FILES["file"], $largura, $altura, $caminho, $crop,$pb, forma_url( NOME_CLIENTE . "-" . $form['nome-cond'] . "-" . date("dmYHis") ) );
        }

        $dados = Container::getModel("Projetos");
        $retorno = $dados->gravaGaleria($form);
		
		return $retorno ? true : false;

	}

    public function excluirImg()
    {
        $form = retorna_form($_POST);
        // @unlink( "../imagens/projetos/large/".$form['imagem'] );
        // @unlink( "../imagens/projetos/thumb/".$form['imagem'] );
        // @unlink( "../imagens/projetos/crop/".$form['imagem'] );
        $dados = Container::getModel("Projetos");
        $dados->excluiFoto($form['id']);
        return false;
    }


    public function excluirVideo()
    {
        $form = retorna_form($_POST);
        @unlink( "../imagens/videos/".$form['video'] );
        $dados = Container::getModel("Projetos");
        $dados->excluiVideo($form['id']);
        return false;
    }

	//Funções relacionadas as categorias
	
    public function categorias()
    {
        $cat = Container::getModel("Projetos");
        $this->view->header = "Projetos";
		$this->view->headerSmall = "Cadastro e Edição de Categorias";
        $this->view->cat = $cat->busca_categorias();
        $this->render("categorias");
    }

	public function addCategoria()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Projetos");
        $this->view->dados = $dados->addCategoria( $form );
        return false;
    }
	
	public function buscaCategorias()
	{
        $cat = Container::getModel("Projetos");
        $this->view->cat = $cat->busca_categorias();
		return $this->view->cat;
	}


}