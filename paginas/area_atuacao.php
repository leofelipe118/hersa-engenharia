
<div class=" areas_atuacao_wrapper">
    <section class="areas_atuacao_titulo">
        <h2>
         <?= $define['areas_atuacao_wrapper-titulo']?>
        </h2>
    </section>

    <section class="areas_atuacao" id="eng_civil">
        <img src="imagens/eng_civil_2.png" alt="">
        <div class="intro">
            <h2 class="titulo"><?= $define['areas_atuacao_wrapper-titulo-#eng-civil']?></h2>
            <h3 class="descricao"><?= $define['areas_atuacao_wrapper-descrica-#eng-civil']?></h3>
        </div>
    </section>
    <section class="areas_atuacao" id="eng_ele">
        <img src="imagens/area_eng_ele.jpg" alt="">
        <div class="introAjusteCurva">
            <h2 class="titulo"><?= $define['areas_atuacao_wrapper-titulo-#eng-ele']?></h2>
            <h3 class="descricao"><?= $define['areas_atuacao_wrapper-descricao-#eng-ele']?></h3>
        </div>
    </section>
    <section class="areas_atuacao" id="eng_hid">
        <img src="imagens/area_eng_hid.jpg" alt="">
        <div class="intro">
            <h2 class="titulo"><?= $define['areas_atuacao_wrapper-titulo-#eng-hid']?></h2>
            <h3 class="descricao"><?= $define['areas_atuacao_wrapper-descricao-#eng-hid']?></h3>
        </div>
    </section>
    <section class="areas_atuacao" id="eng_mec">
        <img src="imagens/area_eng_mec.jpg" alt="">
        <div class="intro">
            <h2 class="titulo"><?= $define['areas_atuacao_wrapper-titulo-#eng_mec']?></h2>
            <h3 class="descricao"><?= $define['areas_atuacao_wrapper-descricao-#eng_mec']?>
            </h3>
        </div>
    </section>
    <section class="areas_atuacao" id="sis_esp">
        <img src="imagens/area_sis_esp.jpg" alt="">
        <div class="introAjusteCurva">
            <h2 class="titulo"><?= $define['areas_atuacao_wrapper-titulo-#sis_esp']?>
            </h2>
            <h3 class="descricao"><?= $define['areas_atuacao_wrapper-descricao-#sis_esp']?></h3>
       </div>
    </section>
    <section class="areas_atuacao" id="sis_con">
        <img src="imagens/concessoes.png" alt="">
        <div class="intro">
            <h2 class="titulo"><?= $define['areas_atuacao_wrapper-titulo-#sis_con']?></h2>
            <h3 class="descricao"><?= $define['areas_atuacao_wrapper-descricao-#sis_con']?></h3>
        </div>
    </section>
    <div class="pagination">
        
    </div>
</div>