<section class="projetos_background">
   
    
</section>

<div class="projetos_nav">
    
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm">Data Center e Missão Crítica</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" class="color">Hospitais e Instituições de Saúde</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds">Defesa e Segurança</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t"class="color">Transportes</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=mf">Manutenção e Faciaties</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/"class="color">Diversos</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?">Concessões</a>
 
    
</div>

<div class=" projetos_wrapper">

    <div class="text_projetos">
        <h2>Conheça <strong>nossos projetos<strong></h2>
    </div>

    <?php
    $lado = 'right';
    if($arrNoticias):
        foreach ($arrNoticias as $banner => $b):
            if($lado=='right'){
                $lado = 'left';
            } else {
                $lado = 'right';
            }
        ?>
        <section class="projetos" >
            <?php if($lado=='left'): ?>
                <img src="imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>" alt="">
            <?php endif; ?>
            <div class="intro<?=$lado=='right'?'Right':''?>">
                <h2 class="titulo"><?=html_entity_decode($b['titulo_'.$lang]?$b['titulo_'.$lang]:$b['titulo_pt'], ENT_QUOTES)?></h2>
                <h3 class="descricao"><?=html_entity_decode($b['texto_'.$lang]?$b['texto_'.$lang]:$b['texto_pt'], ENT_QUOTES)?></h3>
                <a href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$b['url']?>/<?=$b['id']?>" class="ir_ver_mais">Ver mais<i><img src="imagens/back_gray.svg" alt=""></i></a>
            </div>
            <?php if($lado=='right'): ?>
                <img src="imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>" alt="">
            <?php endif; ?>
        </section>

    <?php 
        endforeach;
    endif; ?>


    <div class="pagination">
        <?php if($totalPaginas==1):else:?>
            <div class="pagination-arrow">
                <a <?=$op?($op==1?'':'href="'.$linkcanonical.$lang.'/projetos/'.(@$op-1).'"'):""?>>
                    <img class="left" src="imagens/arrow-pagination.svg" alt="" srcset="">
                </a>
            </div>
            <?php for ($i = 1; $i <= $totalPaginas; $i++) { ?>
                <a class="pagination-item<?=$op && $op==$i?" active":(!$op && $i==1?" active":"")?>" href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$i?>"><?=$i?></a>
            <?php } ?>
            <div class="pagination-arrow">
                <a <?=$op?($op==@$totalPaginas?'':'href="'.$linkcanonical.$lang.'/projetos/'.(@$op+1).'"'):'href="'.$linkcanonical.$lang.'/projetos/2"'?>>
                    <img src="imagens/arrow-pagination.svg" alt="" srcset="">
                </a>
            </div>
        <?php endif; ?>
        
         
      
    </div>
 
</div>