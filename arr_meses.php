﻿<?php
$arr_meses = array(
	"nome" =>
	array(
		"01" => "Janeiro",
		"02" => "Fevereiro",
		"03" => "Mar&ccedil;o",
		"04" => "Abril",
		"05" => "Maio",
		"06" => "Junho",
		"07" => "Julho",
		"08" => "Agosto",
		"09" => "Setembro",
		"10" => "Outubro",
		"11" => "Novembro",
		"12" => "Dezembro"
	),"abrev" =>
	array(
		"01" => "Jan",
		"02" => "Fev",
		"03" => "Mar",
		"04" => "Abr",
		"05" => "Mai",
		"06" => "Jun",
		"07" => "Jul",
		"08" => "Ago",
		"09" => "Set",
		"10" => "Out",
		"11" => "Nov",
		"12" => "Dez"
	),"abrev2" =>
	array(
		"01" => "J",
		"02" => "F",
		"03" => "M",
		"04" => "A",
		"05" => "M",
		"06" => "J",
		"07" => "J",
		"08" => "A",
		"09" => "S",
		"10" => "O",
		"11" => "N",
		"12" => "D"
	)
);

$arr_meses_pt = array(
	"nome" =>
	array(
		"01" => "Janeiro",
		"02" => "Fevereiro",
		"03" => "Mar&ccedil;o",
		"04" => "Abril",
		"05" => "Maio",
		"06" => "Junho",
		"07" => "Julho",
		"08" => "Agosto",
		"09" => "Setembro",
		"10" => "Outubro",
		"11" => "Novembro",
		"12" => "Dezembro"
	),"abrev" =>
	array(
		"01" => "Jan",
		"02" => "Fev",
		"03" => "Mar",
		"04" => "Abr",
		"05" => "Mai",
		"06" => "Jun",
		"07" => "Jul",
		"08" => "Ago",
		"09" => "Set",
		"10" => "Out",
		"11" => "Nov",
		"12" => "Dez"
	),"abrev2" =>
	array(
		"01" => "J",
		"02" => "F",
		"03" => "M",
		"04" => "A",
		"05" => "M",
		"06" => "J",
		"07" => "J",
		"08" => "A",
		"09" => "S",
		"10" => "O",
		"11" => "N",
		"12" => "D"
	)
);

$arr_meses_en = array(
	"nome" =>
	array(
		"01" => "January",
		"02" => "February",
		"03" => "March",
		"04" => "April",
		"05" => "May",
		"06" => "June",
		"07" => "July",
		"08" => "August",
		"09" => "September",
		"10" => "October",
		"11" => "November",
		"12" => "December"
	),"abrev" =>
	array(
		"01" => "Jan",
		"02" => "Feb",
		"03" => "Mar",
		"04" => "Apr",
		"05" => "May",
		"06" => "Jun",
		"07" => "Jul",
		"08" => "Aug",
		"09" => "Sep",
		"10" => "Oct",
		"11" => "Nov",
		"12" => "Dec"
	),"abrev2" =>
	array(
		"01" => "J",
		"02" => "F",
		"03" => "M",
		"04" => "A",
		"05" => "M",
		"06" => "J",
		"07" => "J",
		"08" => "A",
		"09" => "S",
		"10" => "O",
		"11" => "N",
		"12" => "D"
	)
);

$arr_meses_es = array(
	"nome" =>
	array(
		"01" => "Enero",
		"02" => "Febrero",
		"03" => "Marzo",
		"04" => "Abril",
		"05" => "Mayo",
		"06" => "Junio",
		"07" => "Julio",
		"08" => "Agosto",
		"09" => "Septiembre",
		"10" => "Octubre",
		"11" => "Noviembre",
		"12" => "Diciembre"
	),"abrev" =>
	array(
		"01" => "Ene",
		"02" => "Feb",
		"03" => "Mar",
		"04" => "Abr",
		"05" => "May",
		"06" => "Jun",
		"07" => "Jul",
		"08" => "Ago",
		"09" => "Sep",
		"10" => "Oct",
		"11" => "Nov",
		"12" => "Dic"
	),"abrev2" =>
	array(
		"01" => "E",
		"02" => "F",
		"03" => "M",
		"04" => "A",
		"05" => "M",
		"06" => "J",
		"07" => "J",
		"08" => "A",
		"09" => "S",
		"10" => "O",
		"11" => "N",
		"12" => "D"
	)
);

$arr_dia_semana = array(
	"nomeg" =>
	array(
		"0" => "Domingo",
		"1" => "Segunda-Feira",
		"2" => "Ter&ccedil;a-Feira",
		"3" => "Quarta-Feira",
		"4" => "Quinta-Feira",
		"5" => "Sexta-Feira",
		"6" => "S&aacute;bado"
	),"nome" =>
	array(
		"0" => "Domingo",
		"1" => "Segunda",
		"2" => "Ter&ccedil;a",
		"3" => "Quarta",
		"4" => "Quinta",
		"5" => "Sexta",
		"6" => "S&aacute;bado"
	),"abrev" =>
	array(
		"0" => "Dom",
		"1" => "Seg",
		"2" => "Ter",
		"3" => "Qua",
		"4" => "Qui",
		"5" => "Sex",
		"6" => "Sab",
	),"abrev2" =>
	array(
		"0" => "D",
		"1" => "S",
		"2" => "T",
		"3" => "Q",
		"4" => "Q",
		"5" => "S",
		"6" => "S",
	)
);

$arr_dia_semana_pt = array(
	"nomeg" =>
	array(
		"0" => "Domingo",
		"1" => "Segunda-Feira",
		"2" => "Ter&ccedil;a-Feira",
		"3" => "Quarta-Feira",
		"4" => "Quinta-Feira",
		"5" => "Sexta-Feira",
		"6" => "S&aacute;bado"
	),"nome" =>
	array(
		"0" => "Domingo",
		"1" => "Segunda",
		"2" => "Ter&ccedil;a",
		"3" => "Quarta",
		"4" => "Quinta",
		"5" => "Sexta",
		"6" => "S&aacute;bado"
	),"abrev" =>
	array(
		"0" => "Dom",
		"1" => "Seg",
		"2" => "Ter",
		"3" => "Qua",
		"4" => "Qui",
		"5" => "Sex",
		"6" => "Sab",
	),"abrev2" =>
	array(
		"0" => "D",
		"1" => "S",
		"2" => "T",
		"3" => "Q",
		"4" => "Q",
		"5" => "S",
		"6" => "S",
	)
);

$arr_dia_semana_en = array(
	"nomeg" =>
	array(
		"0" => "Sunday",
		"1" => "Monday",
		"2" => "Tuesday",
		"3" => "Wednesday",
		"4" => "Thursday",
		"5" => "Friday",
		"6" => "Saturday"
	),"nome" =>
	array(
		"0" => "Sunday",
		"1" => "Monday",
		"2" => "Tuesday",
		"3" => "Wednesday",
		"4" => "Thursday",
		"5" => "Friday",
		"6" => "Saturday"
	),"abrev" =>
	array(
		"0" => "Sun",
		"1" => "Mon",
		"2" => "Tue",
		"3" => "Wed",
		"4" => "Thu",
		"5" => "Fri",
		"6" => "Sat",
	),"abrev2" =>
	array(
		"0" => "S",
		"1" => "M",
		"2" => "T",
		"3" => "W",
		"4" => "T",
		"5" => "F",
		"6" => "S",
	)
);

$arr_dia_semana_es = array(
	"nomeg" =>
	array(
		"0" => "Domingo",
		"1" => "Lunes",
		"2" => "Martes",
		"3" => "Mi&eacute;rcoles",
		"4" => "Jueves",
		"5" => "Viernes",
		"6" => "S&aacute;bado"
	),"nome" =>
	array(
		"0" => "Domingo",
		"1" => "Lunes",
		"2" => "Martes",
		"3" => "Mi&eacute;rcoles",
		"4" => "Jueves",
		"5" => "Viernes",
		"6" => "S&aacute;bado"
	),"abrev" =>
	array(
		"0" => "Dom",
		"1" => "Lun",
		"2" => "Mar",
		"3" => "Mie",
		"4" => "Jue",
		"5" => "Vie",
		"6" => "Sab",
	),"abrev2" =>
	array(
		"0" => "D",
		"1" => "L",
		"2" => "M",
		"3" => "M",
		"4" => "J",
		"5" => "V",
		"6" => "S",
	)
);

// A data vem no formato de banco - Y-m-d
/*
function formataData ($dataDB, $arr_meses)
{
	$d = @explode("-",$dataDB);
	
	return($d[2]." de ".$arr_meses["nome"][ $d[1] ]." de ".$d[0]);
}
*/
// A data vem no formato de banco - Y-m-d
function formataData ($dataDB, $arr_meses, $lang)
{
	$d = @explode("-",$dataDB);
	if($lang == "pt" || $lang == "es")
		return($d[2]." de ".$arr_meses["nome"][ $d[1] ]." de ".$d[0]);
	elseif($lang == "en")
	{
		$dia = intval( $d[2] );
		$sufixo = "th";
		if($dia == 1)
			$sufixo = "st";
		elseif($dia == 2)
			$sufixo = "nd";
		elseif($dia == 3)
			$sufixo = "rd";
		
		return( $arr_meses["nome"][ $d[1] ] . " " . $dia . $sufixo . ", " . $d[0] );
	}
}
function formataDataAbrev ($dataDB, $arr_meses, $lang)
{
	$d = @explode("-",$dataDB);
	if($lang == "pt" || $lang == "es")
		return($d[2]." ".$arr_meses["abrev"][ $d[1] ]." ".$d[0]);
	elseif($lang == "en")
	{
		$dia = intval( $d[2] );
		$sufixo = "th";
		if($dia == 1)
			$sufixo = "st";
		elseif($dia == 2)
			$sufixo = "nd";
		elseif($dia == 3)
			$sufixo = "rd";
		
		return( $arr_meses["abrev"][ $d[1] ] . " " . $dia . $sufixo . ", " . $d[0] );
	}
}
?>