<section class="obras_topo">
    <div class="introducao">
        <h2 class="titulo"><?= $define['obras-introducao-titulo']?>
            <strong>n<?= $define['obras-introducao-titulo-strong']?>
            </strong>

            <?= $define['obras-introducao-titulo-parte2']?>
        </h2>
        <span class="descricao"><?= $define['obras-introducao-descricao']?>
            <img src="imagens/arrow_lado.svg" alt=""></span>
    </div>
    <div class="filtros">
        <p class="intro"><?= $define['obras-filtros-intro']?>
        </p>
        <div class="campos">
            <div class="filtro_categoria filtros_int">
                <span class="item_filtro categoria"><?= $define['obras-campos']?>
                    <i><img src="imagens/icon_arrow_filtro.svg" alt="" class="seta"></i></span>
                <div class="filtros_disponiveis">
                    <span class="filtro" value=""><?= $define['obras-campos-filtro-concessoes']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras-campos-filtro-Data_Center']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras-campos-filtro-Defesa-Seg']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras-campos-filtro-Diversos']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras-campos-filtro-Hospitais']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras-campos-filtro-Manutencao']?>
                    </span>
                    <span class="filtro" value="">
                        <?= $define['obras-campos-filtro-Transportes']?>
                    </span>
                </div>
            </div>

            <div class="filtro_status filtros_int">
                <span class="item_filtro status"><?= $define['obras-campos-filtro_status']?>
                    <i><img src="imagens/icon_arrow_filtro.svg" alt="" class="seta"></i></span>
                <div class="filtros_disponiveis">
                    <span class="filtro" value=""><?= $define['obras-campos-filtro_status-top']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras-campos-filtro_status-bottom']?>
                    </span>
                </div>
            </div>
            <div class="filtro_cliente filtros_int">
                <span class="item_filtro cliente"><?= $define['obras-campos-filtro_cliente']?>
                    <i><img src="imagens/icon_arrow_filtro.svg" alt="" class="seta"></i></span>
            </div>
        </div>
    </div>

</section>


<section class="obras_projetos">
    <div class="projeto" id="">
        <img src="imagens/projetos_camargo.jpg" alt="">
        <div class="infos">
            <h3 class="titulo">A<?= $define['obras-obras_projeto-infos-titulo']?></h3>
            <p class="descricao">R<?= $define['obras-obras_projeto-infos-descricao']?>
</p>
            <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais">Ver mais <i><img
                        src="imagens/arrow_vermais.svg" alt=""></i></a>
        </div>
    </div>
    <div class="projeto" id="">
        <img src="imagens/projetos_banrisul.jpg" alt="">
        <div class="infos">
            <h3 class="titulo"><?= $define['obras-obras_projeto-infos-titulo-first']?>
            </h3>
            <p class="descricao"><?= $define['obras-obras_projeto-infos-descricao-first']?>
            </p>
            <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais"><?= $define['obras-obras_projeto-verMais']?>
                <i><img src="imagens/arrow_vermais.svg" alt=""></i></a>
        </div>
    </div>
    <div class="projeto" id="">
        <img src="imagens/projetos_senai.jpg" alt="">
        <div class="infos">
            <h3 class="titulo"><?= $define['obras-obras_projeto-infos-titulo-second']?>
            </h3>
            <p class="descricao"><?= $define['obras-obras_projeto-infos-descricao-second']?>
            </p>
            <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais"><?= $define['obras-obras_projeto-verMais']?>
                <i><img src="imagens/arrow_vermais.svg" alt=""></i></a>
        </div>
    </div>
    <div class="projeto" id="">
        <img src="imagens/projetos_banco_brasil.jpg" alt="">
        <div class="infos">
            <h3 class="titulo"><?= $define['obras-obras_projeto-infos-titulo-third']?>
            </h3>
            <p class="descricao"><?= $define['obras-obras_projeto-infos-descricao-third']?>
            </p>
            <a href="<?=$linkcanonical?><?=$lang?>/obras/1" class="vermais"><?= $define['obras-obras_projeto-verMais']?>
                <i><img src="imagens/arrow_vermais.svg" alt=""></i></a>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $(".filtros_int").click(function (e) {
            var classe = $(this).attr("class");

            if (classe.indexOf('ativo') >= 0) {
                $(this).find(".seta").removeClass("ativo");
                $(this).find(".filtros_disponiveis").removeClass("ativo");
                $(this).removeClass("ativo");
            } else {
                $(this).find(".seta").addClass("ativo");
                $(this).find(".filtros_disponiveis").addClass("ativo");
                $(this).addClass("ativo");
            }
        });
    })
</script>