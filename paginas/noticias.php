<?php
// mostra_array($arrNoticias);
// die;
?>
<section id="banner_noticias">
    <div class="swiper-container noticias_slides">
        <div class="swiper-wrapper banner">
            <?php foreach ($arrNoticias2 as $noticia => $n): ?>
                <div class="swiper-slide">
                    <a href="<?=$linkcanonical?><?=$lang?>/noticias/<?=$n['url']?>/<?=$n['id']?>">
                        <div class="item_banner" style="background-image: url(imagens/posts/large/<?=$n['foto']?$n['foto']:''?>);">
                            <div class="infos">
                                <h3 class="titulo"><?=html_entity_decode($n['titulo_'.$lang]?$n['titulo_'.$lang]:$n['titulo_pt'], ENT_QUOTES)?></h3>
                                <p class="descricao">

                                    <?php 
                                        $escreve2 = html_entity_decode( trim($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt']), ENT_QUOTES);
                                        $texto2 = str_replace("<a", "<p", $escreve2);
                                        $texto2 = str_replace("</a", "</p", $texto2);
                                        // echo $texto2;
                                        echo strlen($texto2) >= 120 ? substr($texto2, 0, 120). '...' : $texto2;
                                    ?>
                                    
                                    <?//=html_entity_decode($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt'], ENT_QUOTES)?>
                                
                                </p>
                                <span class="data"><?=explode("-",$n['data'])[2]." ".$arr_meses_pt['abrev'][explode("-",$n['data'])[1]].", ".explode("-",$n['data'])[0]?></span>
                            </div>
                        </div>
                    </a>
                </div>  
            <?php endforeach; ?>




            <!-- <div class="swiper-slide">
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1">
                    <div class="item_banner" style="background-image: url(imagens/img_banner_1.jpg);">
                        <div class="infos">
                            <h3 class="titulo">Nova Sede da ARSESP</h3>
                            <p class="descricao">A Hersa Engenharia marcou presença na publicação anual da revista O EMPREITEIRO - "500 Grandes da Construção - Ranking da Engenharia Brasileira"</p>
                            <span class="data">28 jun, 2019</span>
                        </div>
                    </div>
                </a>
            </div>  
            <div class="swiper-slide">
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1">
                    <div class="item_banner" style="background-image: url(imagens/img_banner_2.jpg);">
                        <div class="infos">
                            <h3 class="titulo">500 Grandes da Construção</h3>
                            <p class="descricao">A Hersa Engenharia marcou presença na publicação anual da revista O EMPREITEIRO - "500 Grandes da Construção - Ranking da Engenharia Brasileira"</p>
                            <span class="data">5 set, 2019</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="swiper-slide">
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1">
                    <div class="item_banner" style="background-image: url(imagens/img_banner_3.jpg);">
                        <div class="infos">
                            <h3 class="titulo">Condomínio Torre Jequitibá</h3>
                            <p class="descricao">A Hersa Engenharia marcou presença na publicação anual da revista O EMPREITEIRO - "500 Grandes da Construção - Ranking da Engenharia Brasileira"</p>
                            <span class="data">15 ago, 2019</span>
                        </div>
                    </div>
                </a>
            </div> -->
        </div>
        <div class="slide-prev"><img src="imagens/prev_banner.svg" alt=""></div>
        <div class="slide-next"><img src="imagens/next_banner.svg" alt=""></div>
    </div>
</section>
<section class="home_noticias pag_noticia">
    <h1 class="titulo"><?= $define['noticias-home_noticias-titulo-linha1']?>
    <br> <?= $define['noticias-home_noticias-titulo-linha2']?>

 <strong><?= $define['noticias-home_noticias-titulo-linha2-strong']?>

</strong></h1>
    <div class="noticias">
        <?php foreach ($arrNoticias as $noticia => $n): ?>
            <div class="noticia">
                <img src="imagens/posts/large/<?=$n['foto']?$n['foto']:''?>" alt="" class="img_noticia">
                <div class="infos">
                    <span class="data"><?=explode("-",$n['data'])[2]." ".$arr_meses_pt['abrev'][explode("-",$n['data'])[1]].", ".explode("-",$n['data'])[0]?></span>
                    <h3 class="titulo"><?//=html_entity_decode($n['titulo_'.$lang]?$n['titulo_'.$lang]:$n['titulo_pt'], ENT_QUOTES)?></h3>
                    
                    <h4 class="descricao">
                        <?php 
                            $escreve = html_entity_decode( trim($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt']), ENT_QUOTES);
                            $escreve = str_replace('<br style="box-sizing: border-box; color: rgb(71, 71, 71); font-family: "PT Sans"; font-size: 15px;">',"",$escreve);
                            $escreve = str_replace('<p style="box-sizing: border-box; color: rgb(71, 71, 71); line-height: 1.5em; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; font-family: " pt="" sans";="" font-size:="" 15px;"="">',"",$escreve);
                            $escreve = str_replace('<br style="box-sizing: border-box; color: rgb(112, 112, 112); font-family: WorkSans-Light; font-size: 20.49px; background-color: rgb(235, 235, 235); outline: none !important;">',"",$escreve);
                            echo substr("{$escreve}", 0, 220)."...";
                        ?>
                    </h4>
                    
                    <a href="<?=$linkcanonical?><?=$lang?>/noticias/<?=$n['url']?>/<?=$n['id']?>"><span class="ler_mais"><?= $define['home-home_noticias-ler_mais']?></span></a>
                </div>
            </div>
        <?php endforeach; ?>







        <!-- <div class="noticia">
            <img src="imagens/noticia_1.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">16 abr, 2019</span>
                <h3 class="titulo">Inauguração da Nova Subestação de Energia Elétrica</h3>
                <h4 class="descricao">A Hersa concluiu este mês a obra de modernização da rede aérea, cabine de medição, usina de geração de energia e...</h4>
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1"><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>
        <div class="noticia">
            <img src="imagens/noticia_2.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">14 jan, 2019</span>
                <h3 class="titulo">Refrotif AC Camargo Câncer Center</h3>
                <h4 class="descricao">Assinamos este mês o contrato para retrofit do Edifício Castro Alves em São Paulo/SP visando a implantação de uma nova unidade de atendimento do AC Camargo Câncer Center, com área total de 5.105,00 m².</h4>
                <a href=""><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>
        <div class="noticia">
            <img src="imagens/noticia_3.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">6 jan, 2019</span>
                <h3 class="titulo">Datacenter do Banrisul</h3>
                <h4 class="descricao">Em janeiro/2019 iniciamos a obra de construção do Datacenter do Banrisul em Porto Alegre/RS, com área total de 3.700,00 m².</h4>
                <a href=""><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>   
        <div class="noticia">
            <img src="imagens/noticia_3.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">6 jan, 2019</span>
                <h3 class="titulo">Datacenter do Banrisul</h3>
                <h4 class="descricao">Em janeiro/2019 iniciamos a obra de construção do Datacenter do Banrisul em Porto Alegre/RS, com área total de 3.700,00 m².</h4>
                <a href=""><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>      
        <div class="noticia">
            <img src="imagens/noticia_3.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">6 jan, 2019</span>
                <h3 class="titulo">Datacenter do Banrisul</h3>
                <h4 class="descricao">Em janeiro/2019 iniciamos a obra de construção do Datacenter do Banrisul em Porto Alegre/RS, com área total de 3.700,00 m².</h4>
                <a href=""><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>      
        <div class="noticia">
            <img src="imagens/noticia_3.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">6 jan, 2019</span>
                <h3 class="titulo">Datacenter do Banrisul</h3>
                <h4 class="descricao">Em janeiro/2019 iniciamos a obra de construção do Datacenter do Banrisul em Porto Alegre/RS, com área total de 3.700,00 m².</h4>
                <a href=""><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>            -->
    </div>
    
    <div class="pagination">
        <?php if($totalPaginas==1):else:?>
            <div class="pagination-arrow">
                <a <?=$op?($op==1?'':'href="'.$linkcanonical.$lang.'/noticias/'.(@$op-1).'"'):""?>>
                    <img class="left" src="imagens/arrow-pagination.svg" alt="" srcset="">
                </a>
            </div>
            <?php for ($i = 1; $i <= $totalPaginas; $i++) { ?>
                <a class="pagination-item<?=$op && $op==$i?" active":(!$op && $i==1?" active":"")?>" href="<?=$linkcanonical?><?=$lang?>/noticias/<?=$i?>"><?=$i?></a>
            <?php } ?>
            <div class="pagination-arrow">
                <a <?=$op?($op==@$totalPaginas?'':'href="'.$linkcanonical.$lang.'/noticias/'.(@$op+1).'"'):'href="'.$linkcanonical.$lang.'/noticias/2"'?>>
                    <img src="imagens/arrow-pagination.svg" alt="" srcset="">
                </a>
            </div>
        <?php endif; ?>
    </div>  
</section>