<div class="wrapper_a_hersa_engenharia">
    <section class="a_hersa_engenharia sobre">
        <h4 class="up_title"> <?= $define['sobre-wrapper_a_hersa_engenharia-up_title']?>
        </h4>
        <h2 class="titulo"><?= $define['sobre-wrapper_a_hersa_engenharia-titulo']?>
        </h2>
        <p class="descricao"><?= $define['sobre-wrapper_a_hersa_engenharia-descricao-1p']?>
        </p>
        <p class="descricao"><?= $define['sobre-wrapper_a_hersa_engenharia-descricao-2p']?>
        </p>
        <p class="descricao"><?= $define['sobre-wrapper_a_hersa_engenharia-descricao-3p']?>
        </p>
    </section>
</div>
<section class="a_hersa_saiba_mais">
    <div class="intro">
        <img src="imagens/logo_branco.svg" alt="" class="logo">
        <h3 class="descricao"><?= $define['sobre-a_hersa_saiba_mais-intro-descricao']?>
            <strong><?= $define['sobre-a_hersa_saiba_mais-intro-descricao-strong']?>
            </strong> <?= $define['sobre-a_hersa_saiba_mais-intro-descricao-part2']?>
        </h3>
    </div>
    <!-- <div id="player"></div> -->
    <!-- <iframe id="video_institucional_hersa" width="560" height="315" src="https://www.youtube.com/embed/C47g-hiwuYA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
    <!-- <img id="remove_apresenta_video" src="imagens/engenheiro_a_hersa.jpg" alt=""> -->
    <!-- <video id=""  src="video/hersa_institucional.mp4"></video> -->
    <video width="50%" height="100%" poster='imagens/fundo_falso.png'
        style="background: transparent url(imagens/engenheiro_a_hersa.jpg) 50% 50% / cover no-repeat; cursor: pointer;"
        controls>
        <source src="video/hersa_institucional.mp4" type="video/mp4">

    </video>
</section>
<section class="a_hersa_colaboradores">
    <h2 class="titulo"><?= $define['sobre-a_hersa_colaboradores-titulo']?>
    </h2>
    <div class="colaboradores">
        <img src="imagens/icon_colaboradores.svg" alt="Colaboradores - Hersa">
        <span><?= $define['sobre-a_hersa_colaboradores-colaboradores-number']?>
        </span>
        <span class="colaboradores_span"><?= $define['sobre-a_hersa_colaboradores-colaboradores-span']?>
        </span>
    </div>

    <div class="missao_visao_valores">
        <div class="item">
            <img src="imagens/bg_missao.jpg" alt="">
            <h3 class="titulo"><?= $define['sobre-missao_visao_valores-missao-titulo']?>
            </h3>
            <div class="infos">
                <span class="descricao"><?= $define['sobre-missao_visao_valores-missao-descricao']?>
                </span>
            </div>
        </div>
        <div class="item visao">
            <h3 class="titulo"><?= $define['sobre-missao_visao_valores-visao-titulo']?>
            </h3>
            <div class="infos">
                <span class="descricao"><?= $define['sobre-missao_visao_valores-visao-descricao']?>
                </span>
                <div class="linha"></div>
                <span><?= $define['sobre-missao_visao_valores-visao-descricao-linha-span-1']?>
                </span>
                <span><?= $define['sobre-missao_visao_valores-visao-descricao-linha-span-2']?>
                </span>
                <span>
                    <?= $define['sobre-missao_visao_valores-visao-descricao-linha-span-3']?>
                </span>
                </ul>
            </div>
        </div>
        <div class="item">
            <img src="imagens/bg_valores.jpg" alt="">
            <h3 class="titulo"> <?= $define['sobre-missao_visao_valores-valores-titulo']?>
            </h3>
            <div class="infos">
                <span class="descricao"><?= $define['sobre-missao_visao_valores-valores-descricao-1']?>
                </span>
                <span class="descricao"><?= $define['sobre-missao_visao_valores-valores-descricao-2']?>
                </span>
                <span class="descricao"><?= $define['sobre-missao_visao_valores-valores-descricao-3']?>
                </span>
                <span class="descricao"><?= $define['sobre-missao_visao_valores-valores-descricao-4']?>
                </span>
                <span class="descricao"><?= $define['sobre-missao_visao_valores-valores-descricao-5']?>
                </span>
            </div>
        </div>
    </div>
</section>

<script>
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            videoId: 'C47g-hiwuYA'
        });
    }
</script>