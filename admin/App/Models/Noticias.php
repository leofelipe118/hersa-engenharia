<?php

namespace App\Models;

use CRO\Model\Table;

class Noticias extends Table
{
    protected $table = "noticias";

    public function total($filtro = "")
    {
        $query = "SELECT id FROM {$this->table} WHERE 1 ".$filtro;
        return mysqli_num_rows( $this->db->query($query) );
    }

    public function listar($filtro,$inicio,$limite)
    {
        $query = "SELECT id, titulo_pt, titulo_en, titulo_es, data, DATE_FORMAT(data, '%d/%m/%Y') as data_correta, status FROM {$this->table} WHERE 1 ".$filtro." ORDER BY data DESC LIMIT $inicio,$limite ";
        return $this->db->query($query);
    }

    public function excluiFoto($id)
    {
        $query = "UPDATE {$this->table} SET foto = NULL WHERE id = {$id}";
        return $this->db->query($query);
    }
	
	public function checkUrl( $url, $id )
	{
		$geraUrl = forma_url($url);
		$cond = $id ? "AND id != {$id}" : "";
		$query = "SELECT id FROM {$this->table} WHERE url = '{$geraUrl}' {$cond}";
		return( $this->db->query($query)->num_rows );
	}

	public function geraUrl( $titulo, $id )
	{
		$geraUrl = forma_url($titulo);
		$this->novaUrl($geraUrl,$id);
		return false;
	}
	
	public function novaUrl( $titulo, $id, $passo = 0 )
	{
		$cond = $id ? "AND id != {$id}" : "";
		if($passo)
		{
			$teste = $titulo . "-" . $passo;
			$query = "SELECT id FROM {$this->table} WHERE url = '{$teste}' {$cond}";
			if( $this->db->query($query)->num_rows )
				$this->novaUrl($titulo,$id,$passo++);
			else
				echo $teste;
		}
		else
		{
			$query = "SELECT id FROM {$this->table} WHERE url = '{$titulo}' {$cond}";
			if( $this->db->query($query)->num_rows )
			{
				if($id)
				{
					$teste = $titulo . "-" . $id;
					$query = "SELECT id FROM {$this->table} WHERE url = '{$teste}' {$cond}";
					if( $this->db->query($query)->num_rows )
						$this->novaUrl($titulo,$id,1);
					else
						echo  $teste;
				}
				else
					$this->novaUrl($titulo,$id,1);				
			}
			else
				echo  $titulo;
		}
	}

	//Funções relacionadas as categorias
	public function busca_categorias()
    {
        $query = "SELECT * FROM categorias ORDER BY categoria";
        return $this->db->query($query);
    }

    public function addCategoria($form)
    {
		if( $form['id-cond'] )
			$sql = executa($this->db, $form, "categorias", "edita", "id = '".$form['id-cond']."'");
		else
			$sql = executa($this->db, $form, "categorias", "inserir", "");

        return $sql ? "ok" : "erro";
    }

}