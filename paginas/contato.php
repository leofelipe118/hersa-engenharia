<div class="areas_atuacao_wrapper contato-bg">
    <section id="contato">

        <div class="infos">
            <h2 class="text_contato"> <?= $define ['contato-#contato-infos-text_contato']?>
            </h2>

            <div class="info telefone">
                <img src="imagens/icon_tel.svg" alt="" class="logo">
                <span><a href="tel:+551126034178"><?= $define ['contato-#contato-infos-telefone']?>
                    </a></span>
            </div>
            <div class="info email">
                <img src="imagens/icon_email.svg" alt="" class="logo">
                <span><a href="mailto:contato@hersa.com.br"><?= $define['contato-#contato-infos-email']?>
                    </a></span>
            </div>
            <div class="info endereco">
                <img src="imagens/icon_map.svg" alt="" class="logo">
                <span><a href="https://goo.gl/maps/TWEpV6oYehUdSTHx9"><?= $define['contato-#contato-infos-endereco']?>
                        <br> <?= $define['contato-#contato-infos-endereco-cep']?>
                    </a></span>
            </div>
        </div>


        <div class="form_flutuante">
            <h1 class="titulo"><?= $define['contato-#contato-form_flutuante-titulo']?></h1>
            <!-- <form action="" id="form_contato"> -->
            <form id="form_contato" action="https://benettoncomunicacao.com.br/amostra/hersa_email/acoes.php?op=contato"
                method="post" enctype="multipart/form-data">
                <input type="text" name="nome" id="nome"
                    placeholder=<?= $define['contato-#contato-form-input-#nome-placeholder']?>>
                <!-- <input type="text" name="email" id="email" placeholder="E-mail"> -->
                <input type="tel" autocomplete="off"
                    placeholder=<?= $define['contato-#contato-form-input-#telefone-placeholder']?> name="telefone"
                    maxlength="15" onkeyup="mascara(this, mtel);" class="required">
                <textarea name="mensagem" id="mensagem" rows="10" placeholder="Mensagem"></textarea>
                <div class="botao">
                    <input id="checkbox1" class="check_contato_verifica checkbox" type="checkbox">
                    <label for="checkbox1"></label> <span><?= $define['contato-#contato-form_flutuante-label']?>

                    </span>

                    <button id="dispara_contato" type="button"
                        class="enviar_form"><?= $define['contato-#contato-form_flutuante-button']?>
                        <i class="seta"><img src="imagens/back.svg" alt="" class="icon"></i></button>
                </div>
            </form>
        </div>
    </section>
</div>


<div class="wrapper-carreira">
    <h2><?= $define['contato-trabalhe_conosco_top-bg-titulo-strong']?>
    </h2>
    <section id="carreiras">
        <div class="infos">
            <img src="imagens/carreiras_left.jpg" alt="" class="img-carreira-left" srcset="">
        </div>
        <div class="form_flutuante">
            <h1 class="titulo">
                <?= $define['contato-form_trabalhe_conosco-descricao']?><?= $define['contato-form_trabalhe_conosco-descricao-strong']?>
                </strong> </h1>
            <!-- <form action="" id="form_carreiras"> -->
            <form id="form_carreiras"
                action="https://benettoncomunicacao.com.br/amostra/hersa_email/acoes.php?op=carreiras" method="post"
                enctype="multipart/form-data">
                <div class="carreiras-field-wrapper">
                    <div class="carreiras-field">
                        <label for="nome"> <?= $define['contato-form-input-#nome-placeholder']?>
                        </label>
                        <input type="text" name="nome" id="nome" placeholder="">
                    </div>
                    <div class="carreiras-field">
                        <label for="email"> <?= $define['contato-form-input-#email-placeholder']?>
                        </label>
                        <input type="text" name="email" id="email" placeholder="">
                    </div>
                    <div class="carreiras-field">
                        <label for="telefone"> <?= $define['contato-form-input-#telefone-placeholder']?>
                        </label>
                        <input type="tel" autocomplete="off" name="telefone" id="telefone" maxlength="15"
                            onkeyup="mascara(this, mtel);" class="required">
                    </div>
                </div>
                <div class="botao">
                    <div class="btn-one">
                        <div class="opacity">
                            <input type="file" name="anexo" id="anexar-carreira" for="btn-anexar">
                        </div>
                        <div class="wrapper_btn-anexar">
                            <btn class="btn-anexar" for="btn-anexar">
                                <?= $define['contato-form-botoes-label']?>

                            </btn>
                            <p><?= $define['contato-form-botoes-label-small']?>
                            </p>
                        </div>
                        <input id="checkbox2" class="check_carreiras_verifica checkbox" type="checkbox">
                        <label for="checkbox2"></label>
                        <span class="span-checkbox"><?= $define['contato-#contato-form_flutuante-label']?>
                        </span>
                    </div>
                    <div class="btn-two">
                        <button id="dispara_carreiras" type="button" class="enviar_form">
                            <?= $define['contato-form-botoes-enviar_form']?>

                            <i class="seta">
                                <img src="imagens/back.svg" alt="" class="icon">
                            </i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

<?php
if($mobile == true){
    ?>
        <script type="text/javascript">
            document.querySelector("#form_carreiras #nome").placeholder="Nome";
            document.querySelector("#form_carreiras #email").placeholder="Email";
            document.querySelector("#form_carreiras #telefone").placeholder="Telefone";
        </script>

    <?php
    }
    ?>
