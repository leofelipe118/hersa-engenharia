<section class="a_hersa_top atuacao clientes">

    <!-- <div class="scroll_down">
        <b></b>
        <span>SCROLL DOW</span>
    </div> -->
</section>
<section id="clientes_hersa">
    <?php if($arrCli['dc']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Data-Center']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['dc'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/datacenter01.svg" alt="" class="logo_cliente">
            <img src="imagens/datacenter02.svg" alt="" class="logo_cliente">
            <img src="imagens/datacenter03.svg" alt="" class="logo_cliente">
            <img src="imagens/datacenter04.svg" alt="" class="logo_cliente">
            <img src="imagens/datacenter05.svg" alt="" class="logo_cliente">
            <img src="imagens/datacenter06.svg" alt="" class="logo_cliente">  
            <img src="imagens/datacenter07.svg" alt="" class="logo_cliente">   -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['h']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Hospitais']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['h'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/hospitais01.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais02.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais03.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais04.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais05.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais06.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais07.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais08.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais09.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais10.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais11.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais12.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais13.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais14.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais15.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais16.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais17.svg" alt="" class="logo_cliente">
            <img src="imagens/hospitais18.svg" alt="" class="logo_cliente"> -->

        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['d']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Defesa-Seg']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['d'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['t']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Transportes']?></h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['t'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['mf']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Manutencao-Facilities']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['mf'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['i']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Ensino']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['i'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['o']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Orgaos-Judiciais']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['o'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['p']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Prefeituras']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['p'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['c']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Conc-Servicos']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['c'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['s']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Seg-Diversos']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['s'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
    <?php if($arrCli['cc']): ?>
    <div class="container_clientes">
        <h2 class="titulo_clientes"><?= $define['clientes-title-Concessoes']?>
        </h2>
        <div class="logos_clientes">
            <?php foreach ($arrCli['cc'] as $cliente => $c): ?>
            <img src="imagens/clientes/<?=$c['logo']?$c['logo']:''?>" alt="" class="logo_cliente">
            <?php endforeach; ?>
            <!-- <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente">
            <img src="imagens/clientes_caixa.svg" alt="" class="logo_cliente"> -->
        </div>
        <!-- <img src="imagens/clientes_hosp.jpg" alt="" class="all_clientes"> -->
    </div>
    <?php endif; ?>
</section>