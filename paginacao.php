
<?php
if ($resul > 0 && $totalPaginas > 1)
{
    $prev = $pagina - 1;
    if($prev < 1)
        $prev = 1;

    $prox = $pagina + 1;
    if($prox > $totalPaginas)
        $prox = $totalPaginas;

    $de = ($limite * $pagina) - ($limite - 1);
    $ate = $limite * $pagina;

	$link = isset($lang) ? $lang . "/" : "";
	$link .= $_GET['pag'];
 	?>
	<div class="paginacao">
		<ul>
			<?php
			$contador = 1;
			for($i = 1; $i <= $totalPaginas; $i++):
				if(
					(
						$i == $pagina ||
						$i == ($pagina - 1) ||
						$i == ($pagina + 1) ||
						$i == ($pagina + 2) ||
						$pagina <= 2 ||
						$i == ($totalPaginas - 3) ||
						$i == ($totalPaginas - 2) || 
						$i == ($totalPaginas)
					)
					&& $contador <= 4
				):
					$classe = $pagina == $i ? "ativo" : "";
					$href = $pagina == $i ? "" : 'href="' . $link . "/" . $i . '"';?>
					<li>
						<a <?= $href?>  class="<?=$classe?> <?=$pag == "obras-e-projetos" ? "obras" : "";?>"><?=$i?></a>
					</li>
					<?php $contador++;
				endif;
				if($contador > 4)
					break;
			endfor;?>
		</ul>
	</div>
<?php
}?>