<section>
    <div class="bg-circles">
        <div class="circle1"><img src="imagens/circle1.svg" title="Confidence it services"
                alt="Confidence it services" /></div>
        <div class="circle2"><img src="imagens/circle2.svg" title="Confidence it services"
                alt="Confidence it services" /></div>
        <div class="circle3"><img src="imagens/circle3.svg" title="Confidence it services"
                alt="Confidence it services" /></div>
    </div>
    <div class="container section-lg position-relative section-first">
        <h1 class="h4 mb-5 font-weight-light">
            <?= $define['blog-title-section-1']?><b><?= $define['blog-title-section-bold']?></b>
            <?= $define['blog-title-section-2']?></h1>
        <div class="row mx-lg-n4">
            <?php 
            foreach($arrBlog as $c):
                $texto = converteTexto($c['texto_'.$lang]);
                $texto = str_replace("<h3>", "</p><h3>", $texto);
                $texto = str_replace("</h3>", "</h3><p>", $texto);
                $texto = str_replace("<h2>", "</p><h2>", $texto);
                $texto = str_replace("</h2>", "</h2><p>", $texto);
                ?>
            <article class="col-md-6 col-lg-4 px-lg-4 mb-5" style="cursor: pointer;"
                onclick="location.href = '<?=$linkcanonical?><?=$lang?>/blog/<?=$c['url']?>/<?=$c['id']?>' ">
                <div class="blog-list-img"><img src="imagens/posts/crop/<?=$c['foto'];?>"
                        srcset="imagens/posts/crop/<?=$c['foto'];?> 2x, imagens/posts/crop/<?=$c['foto'];?> 1x"
                        loading="lazy" title="<?=$c['titulo_'.$lang];?>" alt="<?=$c['titulo_'.$lang];?>" /></div>
                <div class="mx-3 my-4">
                    <h2 class="h5 font-weight-semi"><?=$c['titulo_'.$lang];?></h2>
                    <h3 class="blog_descricao"><?=mb_substr(str_replace(["&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&acirc;","&ecirc;","&ocirc;","&atilde;","&otilde;","&ccedil;",],["á","é","í","ó","ú","â","ê","ô","ã","õ","ç"],
                            $c['subtitulo_'.$lang] ? $c['subtitulo_'.$lang] : (
                                $c['subtitulo_pt'] ? $c['subtitulo_pt'] : (
                                    $c['titulo_'.$lang] ? $c['titulo_'.$lang] : $c['titulo_pt']
                                    ))), 0, 213);?>...</h3>
                    <div class="row mt-3 align-items-center">
                        <div class="col-auto"><img class="rounded mw-100" src="imagens/confidence-o.png"
                                srcset="imagens/confidence-o@2x.png 2x, imagens/confidence-o.png 1x" loading="lazy"
                                alt="" /></div>
                        <div class="col">
                            <small><?= $define['blog-title-Confidence-IT']?><br /><?= formataDataAbrev($c['data'], $arr_meses, $lang);?></small>
                        </div>
                    </div>
                </div>
            </article>
            <?php
            endforeach;
            ?>
        </div>
        <?php if($totalPaginas==1):else:?>
        <div class="pagination">
            <div class="pagination-arrow"><a
                    <?=$op?($op==1?'':'href="'.$linkcanonical.$lang.'/blog/'.(@$op-1).'"'):""?>><img
                        src="imagens/icon-arrow-left-l.svg" title="Confidence it services"
                        alt="Confidence it services"></a></div>
            <?php for ($i = 1; $i <= $totalPaginas; $i++) { ?>
            <a class="pagination-item<?=$op && $op==$i?" active":(!$op && $i==1?" active":"")?>"
                href="<?=$linkcanonical?><?=$lang?>/blog/<?=$i?>"><?=$i?></a>
            <?php } ?>
            <div class="pagination-arrow"><a
                    <?=$op?($op==@$totalPaginas?'':'href="'.$linkcanonical.$lang.'/blog/'.(@$op+1).'"'):'href="'.$linkcanonical.$lang.'/blog/2"'?>><img
                        src="imagens/icon-arrow-right-l.svg" title="Confidence it services"
                        alt="Confidence it services"></a></div>
            <div class="pt-3 ml-4 color-secondary50"><?=@$totalPaginas?> páginas</div>
        </div>
        <?php endif; ?>
    </div>
</section>

<section class="bg-primary round-top-left">
    <div class="container text-center section-lg">
        <div class="h3 color-w50 mb-4"><?= $define['blog-bg-primary-container-h3']?>
        </div>
        <form class="row" action="" method="POST">
            <div class="col-lg-6 offset-lg-3">
                <input type="text" name="" class="input-xl w-100 mb-3" placeholder="Seu nome" />
                <input type="email" name="" class="input-xl w-100 mb-3" placeholder="Seu e-mail" />
                <button type="submit" class="button button-secondary mt-4 h5 sbm-btn">
                    <?= $define['blog-bg-primary-container-button']?></button>
            </div>
        </form>
    </div>
</section>