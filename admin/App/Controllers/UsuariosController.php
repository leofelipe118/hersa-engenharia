<?php
namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class UsuariosController extends Action
{

    public function listar()
    {
        $lista = Container::getModel("Usuarios");
        $this->view->header = "Usuários";
        $this->view->headerSmall = "Listar Usuários";
        $this->view->lista = $lista->fetchAll( " nivel, nome " );

        //Verifica se tem retorno na URL
        $url = Bootstrap::getUrl();
        $id_retorno = @array_pop( @explode("/", $url ) );
        if( is_numeric($id_retorno) )
            $this->view->retorno  = $id_retorno == 1 ? "ok" : "erro";

        $this->render("listar");
    }

    public function cadastrar()
    {
        $lista = Container::getModel("Usuarios");
        $this->view->header = "Usuários";

        $url = Bootstrap::getUrl();
        $id = @array_pop( @explode("/", $url ) );
        $this->view->id  = 0;
		$this->view->permissao = array();
        if( is_numeric($id) )
        {
            $this->view->id = $id;
            $this->view->headerSmall = "Edição de Usuário";
            $this->view->lista = $lista->find($id);
            $this->view->permissao = @explode( "|", $this->view->lista['permissoes'] );
        }
        else
            $this->view->headerSmall = "Cadastro de novo  Usuário";

        $this->render("cadastrar");
    }

    public function cadastro()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Usuarios");
		
        //verifica se é novo usuario e "gera" a senha
        if( !$form['id-cond'] )
        {
            //$senha = geraSenha(8,true,true,false);
			$senha = $form['senha-string'];
            $form['senha-string'] = md5($senha);
        }

        //Verifica se já existe email cadastrado
        $verifica = $dados->find($form['email-string'], "email");
        if($verifica['id']){
            if(!$form['id-cond']){
                redireciona(BASE_SITE."usuarios/cadastrar?erro");
                die;
            }elseif($verifica['id'] != $form['id-cond']){
                redireciona(BASE_SITE."usuarios/cadastrar/".$form['id-cond']."?erro");
                die;
            }
        }
        $form['permissoes-string'] = "";
        if( $form['nivel-string'] == 'c' )
            $form['permissoes-string'] = @implode( "|", $form['permissoes-cond'] );

        $retorno = $dados->grava($form);
        $urlRet = $retorno ? "1" : "0";

        if($urlRet && !$form['id-cond'] )//envia e-mail ao usuario
            $dados->enviaEmail($form['nome-string'], $form['email-string'], $senha);

        redireciona(BASE_SITE."usuarios/listar/".$urlRet);
    }

    public function alteraDados()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Usuarios");
        if($form['senha1-cond'] && $form['senha1-cond'] === $form['senha2-cond'])
            $form['senha-string'] = md5($form['senha1-cond']);

        $retorno = $dados->grava($form);
        $urlRet = $retorno ? "1" : "0";

        redireciona(BASE_SITE."usuarios/meus-dados/".$urlRet);
    }

    public function meusDados()
    {
        $lista = Container::getModel("Usuarios");
        $this->view->header = "Usuários";
        $this->view->headerSmall = "Meus Dados";
        $this->view->lista = $lista->find($_SESSION[DB_DATABASE]['id_usuario']);

        //Verifica se tem retorno na URL
        $url = Bootstrap::getUrl();
        $id_retorno = @array_pop( @explode("/", $url ) );
        if( is_numeric($id_retorno) )
            $this->view->retorno  = $id_retorno == 1 ? "ok" : "erro";

        $this->render("meusDados");
    }

}