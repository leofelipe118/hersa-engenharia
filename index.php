<?php
session_start();
$pag = @$_GET['pag'];
$op = @$_GET['op'];
$id = @$_GET['id'];
$lang = @$_GET['lang'];

$protocolo = $_SERVER['SERVER_PORT'] == "443" ? "https" : "http";
$linkcanonical = $protocolo . "://".$_SERVER['HTTP_HOST'] . @array_shift( @explode( "index.php", $_SERVER['SCRIPT_NAME'] ) );


if(@$lang == "pt" || @$lang == "en" || @$lang == "es")

{
  
    if(@$lang == "admin"){?>

<script>
    window.location = '<?= $linkcanonical?>admin';
</script>

<?php

    }

    else

    {

        $_SESSION['lang'] = $lang;

        include('lang/'.$lang.'.php');

    }

}

else

{

    include('lang/pt.php');

    $lang = 'pt';

    $_SESSION['lang'] = $lang;

}


include("integracao.php");
include("arr_meses.php");
include("model.php");

include('paginas.php');
include('paginas/svg.php');
include("paginas/verif_mobile.php");

$linkcanonical_real = $linkcanonical.$lang.'/';
if(@$pag)
    $linkcanonical_real .= @$pag;
if(@$op)
    $linkcanonical_real .= @$op;
if(@$_GET['id'])
    $linkcanonical_real .= @$_GET['id'];


$basesite = $linkcanonical;

$linkfacebook = "";
$linktwitter = "#";
$linkLinkedin = "";
$linkinstagram = "#";

?>

<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8" lang="pt-br"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9" lang="pt-br"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="pt-br">
<!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title><?php echo @$title;?></title>
    <meta name="description" content="<?php echo @$description;?>" />
    <meta name="Author" content="Benetton Comunicação" />
    <meta name="robots" content="index, follow">

    <link rel="canonical" href="<?php echo $linkcanonical_real;?>" />

    <base href="<?=$basesite?>">
    <!-- mobile settings -->
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo @$title;?>" />
    <meta property="og:description" content="<?php echo @$description;?>" />
    <meta property="og:url" content="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" />
    <meta property="og:site_name" content="<?php echo $nomeDaEmpresa;?>" />
    <meta property="article:author" content="https://benettoncomunicacao.com.br/" />
    <meta property="article:tag" content="" />
    <meta property="og:image"
        content="<?php echo $linkcanonical; echo isset($imagemFace) ? $imagemFace : "imagens/foto_face.jpg";?>" />
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="<?php echo $nomeDaEmpresa;?>">
    <meta name="twitter:title" content="<?php echo $nomeDaEmpresa;?> - <?php echo $nomeDaEmpresa;?>">

    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage"
        content="<?php echo $linkcanonical;?>admin/App/Views/includes/imagens/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- FAVICON -->

    <!-- Css -->
    <link rel="stylesheet" type="text/css" href="js/swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$linkcanonical;?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=$linkcanonical;?>sass/estilos.css">
    <link rel="stylesheet" href="../package/swiper-bundle.min.css">
    <script src="js/jquery/jquery-2.1.1.min.js"></script>

    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "https://schema.org/LocalBusiness",
            "author": "<?=$nomeDaEmpresa;?>",
            "publisher": "<?=$nomeDaEmpresa;?>",
            "headline": "<?php echo $title;?>",
            "datePublished": "<?php echo date("
            Y - m - d ")."
            T08: 00: 00 Z ";?>",
            "dateModified": "<?php echo date("
            Y - m - d ")."
            T08: 00: 00 Z ";?>",
            "mainEntityOfPage": "<?php echo "
            https: //".$_SERVER['HTTP_HOST'] . @array_shift(@explode("index.php", $_SERVER['SCRIPT_NAME']));?>",
                "image": [
                    "<?php echo "
                    https: //".$_SERVER['HTTP_HOST'] . @array_shift(@explode("index.php", $_SERVER['SCRIPT_NAME']));?>imagens/logo_email.png"
                ]
        }
    </script>

    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->

</head>

<body itemscope itemtype="https://schema.org/LocalBusiness" class="bg-light">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHDJPBQ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="header-space"></div>
    <header id="main-header" class="sticky top">
        <div class="row align-items-center">
            <div class="header-left col-7 col-sm-auto">
                <div class="header_menu_top">
                    <!-- <span class= "menu-hamburguer-img"> <i><img src="imagens/icon-menu.svg" alt=""></i></span> -->
                    <div class="menu-wrap">
                        <input id="menu-hamburguer" type="checkbox" />

                        <label for="menu-hamburguer">
                            <div class="menu">
                                <span class="hamburguer"></span>
                            </div>
                        </label>
                    </div>

                    <a href="<?=$linkcanonical?><?=$lang?>" class="logo-header"><img class="mw-100"
                            src="imagens/logo-hersa.svg" title="Hersa Engenharia" alt="Hersa Engenharia" /></a>
                    <a href="<?=$linkcanonical?><?=$lang?>/home"
                        class="link_footer <?= $pag == 'home' ? 'ativo' : '' ?>"><?= $define['index-header-menu-home']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/sobre"
                        class="link_footer <?= $pag == 'sobre' ? 'ativo': ''?>"><?= $define['index-header-menu-institucional']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao"
                        class="link_footer <?= $pag == 'areas-de-atuacao' ? 'ativo': ''?>"><?= $define['index-header-menu-areas-atuacao']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos"
                        class="link_footer <?= $pag == 'projetos' ? 'ativo': ''?>"><?= $define['index-header-menu-projetos']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/clientes"
                        class="link_footer <?= $pag == 'clientes' ? 'ativo': ''?>"><?= $define['index-header-menu-clientes']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/qsms"
                        class="link_footer <?= $pag == 'qsms' ? 'ativo': ''?>"><?= $define['index-header-menu-qsms']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/noticias"
                        class="link_footer <?= $pag == 'noticias' ? 'ativo': ''?>"><?= $define['index-header-menu-noticias']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/compliance"
                        class="link_footer <?= $pag == 'compliance' ? 'ativo': ''?>"><?= $define['index-header-menu-compliance']?>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/contato"
                        class="link_footer <?= $pag == 'contato' ? 'ativo': ''?>"><?= $define['index-header-menu-contato']?>
                    </a>

                    <span class="moblie_logo_header"> <i><img src="imagens/moblie_logo_header.svg" alt=""></i></span>

                    <div class="flags">

                        <form action="<?=$linkcanonical?><?=$lang?>/projetos/pesquisa" method="get" id="form_busca"
                            class="icon_search">
                            <input type="search" name="pesquisa" id="busca">
                            <button type="submit">
                                <img src="imagens/search.svg" alt="">
                            </button>
                        </form>
                        

                        <span class="icon_nav">
                            <a href="<?=$linkcanonical?>pt/home" style="border:none"> 
                                <i><img src="imagens/flag-brazil.svg" alt=""></i>
                            </a>
                        </span>

                        <span class="icon_nav">
                            <a href="<?=$linkcanonical?>en/home" style="border:none">
                                <i><img src="imagens/flag-usa.svg" alt=""></i>
                            </a>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        </div>
    </header>

    <div class="menu_hamb_op">
        <ul class="lista">

            <a href="<?=$linkcanonical?><?=$lang?>">
                <li><a href="<?=$linkcanonical?><?=$lang?>/home"
                        class="link_footer <?= $pag == 'home' ? 'ativo' : '' ?>"><?= $define['index-header-menu-hamburguer-home']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/sobre"
                        class="link_footer <?= $pag == 'sobre' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-institucional']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao"
                        class="link_footer <?= $pag == 'areas-de-atuacao' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-areas-atuacao']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/projetos"
                        class="link_footer <?= $pag == 'projetos' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-projetos']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/clientes"
                        class="link_footer <?= $pag == 'clientes' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-clientes']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/qsms"
                        class="link_footer <?= $pag == 'qsms' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-qsms']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/noticias"
                        class="link_footer <?= $pag == 'noticias' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-noticias']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/compliance"
                        class="link_footer <?= $pag == 'compliance' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-compliance']?>
                    </a></li>
                <li><a href="<?=$linkcanonical?><?=$lang?>/contato"
                        class="link_footer <?= $pag == 'contato' ? 'ativo': ''?>"><?= $define['index-header-menu-hamburguer-contato']?>
                    </a></li>
        </ul>
        <div class="flags">

            <span class="icon_nav">
                <a href="<?=$linkcanonical?>pt/home" style="border:none">
                    <i><img src="imagens/flag-brazil.svg" alt=""></i>
                </a>
            </span>

            <span class="icon_nav">
                <a href="<?=$linkcanonical?>en/home" style="border:none">
                    <i><img src="imagens/flag-usa.svg" alt=""></i>
                </a>
            </span>
         

        </div>
    </div>

    <nav id="main-menu">
        <div class="top_menu">
            <img class="icon-close icon_top_flut" src="imagens/icon-close.svg" width="30" height="30"
                title="Hersa Engenharia" alt="Hersa Engenharia" onclick="toggleMenu()">
            <span><?= $lang=="pt" ? "PT" : ($lang=="en" ? "EN" : "ES") ?> </span>
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="12" viewBox="0 0 25 12">
                <g id="on_off" data-name="on off" transform="translate(-1815 -56)">
                    <g id="Rectangle_1" data-name="Rectangle 1" transform="translate(1815 56)" fill="none" stroke="#fff"
                        stroke-width="2">
                        <rect width="25" height="12" rx="6" stroke="none" />
                        <rect x="1" y="1" width="23" height="10" rx="5" fill="none" />
                    </g>
                    <circle id="Ellipse_1" data-name="Ellipse 1" cx="2" cy="2" r="2" transform="translate(1831 60)"
                        fill="#fff" />
                </g>
            </svg>
        </div>

        <section class="lvl1">
            <div class="scroll-wrap">
                <a href="<?=$linkcanonical?><?=$lang?>" class="item"><?= $define['index--section-lvl1-home']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/sobre"
                    class="item"><?= $define['index--section-lvl1-institucional']?>
                </a>
                <div id="menu-btn-soluc" class="item has-sub" onclick="openSubMenu('m2-solucoes',2, this)">
                    <?= $define['index--section-lvl1-areas-atuacao']?>
                </div>
                <a href="<?=$linkcanonical?><?=$lang?>/obras" class="item"><?= $define['index--section-lvl1-obras']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/obras"
                    class="item"><?= $define['index--section-lvl1-concessoes']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/clientes"
                    class="item"><?= $define['index--section-lvl1-clientes']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/qsms" class="item"><?= $define['index--section-lvl1-qsms']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/noticias"
                    class="item"><?= $define['index--section-lvl1-noticias']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/trabalhe-conosco"
                    class="item"><?= $define['index--section-lvl1-trabalhe-conosco']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/compliance"
                    class="item"><?= $define['index--section-lvl1-compliance']?>
                </a>
                <a href="<?=$linkcanonical?><?=$lang?>/contato"
                    class="item"><?= $define['index--section-lvl1-contato']?>
                </a>

            </div>
        </section>

        <section class="lvl2" id="m2-solucoes">
            <div class="custom-scroll">
                <div class="content-wrapper">
                    <div class="content">
                        <div class="subtitle color-primary mt-0"><small>
                                <?= $define['index--section-lvl2-title-#']?>
                            </small></div>
                        <div class="item">
                            <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao#eng_civil"
                                class="h5"><?= $define['index--section-lvl2-subtitle-#eng-civil']?>
                            </a><br />
                            <small class="color-w50">
                                <?= $define['index--section-lvl2-description-#eng-civil']?>
                            </small>
                        </div>
                        <div class="item">
                            <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao#eng_ele" class="h5">
                                <?= $define['index-section-lvl2-subtitle-#eng-ele']?>
                            </a><br />
                            <small class="color-w50"><?= $define['index-section-lvl2-description-#eng-ele']?>
                            </small>
                        </div>
                        <div class="item">
                            <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao#eng_hid" class="h5">
                                <?= $define['index-section-lvl2-subtitle-#eng-hid']?>
                            </a><br />
                            <small class="color-w50"><?= $define['index-section-lvl2-description-#eng-hid']?>
                            </small>
                        </div>
                        <div class="item">
                            <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao#eng_mec" class="h5">
                                <?= $define['index-section-lvl2-subtitle-#eng-mec']?>
                            </a><br />
                            <small class="color-w50">
                                <?= $define['index-section-lvl2-description-#eng-mec']?>
                            </small>
                        </div>
                        <div class="item">
                            <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao"
                                class="h5"><?= $define['index-section-lvl2-subtitle-#sis_esp']?>
                            </a><br />
                            <small class="color-w50"><?= $define['index-section-lvl2-description-#sis_esp']?>
                            </small>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <!-- <section class="lvl3" id="m3-comp-fiscal">
        <div class="menu-back-btn button button-round button-sm button-outline-primary" onclick="console.log(1);closeSubMenu(3)"><img src="imagens/icon-arrow-down-l.svg" width="15" title="Hersa Engenharia" alt="Hersa Engenharia"></div>
        <div class="custom-scroll">
            <div class="content-wrapper">
                <div class="content">
                    <a href="<?=$linkcanonical?><?=$lang?>/calculadora-fiscal" class="item">Calculadora fiscal</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/calendario-fiscal" class="item">Calendário fiscal</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/ciap-e-dirf" class="item">CIAP e DIRF</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/ecf" class="item">ECF</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/e-credac" class="item">e-CredAc CAT83</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/efd-contribuicoes" class="item">EFD Contribuições</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/efd-reinf" class="item">EFD Reinf</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/efd-reinf-faq" class="item">FAQ - EFD Reinf</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/fcont" class="item">FCONT</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/impostos-retidos" class="item">Impostos Retidos</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/integracao-oracle-cloud" class="item">Integração Oracle ERP Cloud</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/inteligencia-fiscal" class="item">Inteligência Fiscal</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/obrigacoes" class="item">Obrigações Federais, Estaduais e Municipais</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/per-dcomp-ipi" class="item">PER - DCOMP IPI</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/sped-contabil" class="item">SPED Contábil</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/sped-fiscal" class="item">SPED Fiscal</a>
                    <a href="<?=$linkcanonical?><?=$lang?>/sped-fiscal-k" class="item">SPED Fiscal - Bloco K</a>
                </div>
            </div>
        </div>
    </section>

    <section class="lvl3" id="m3-comp-hcm">
        <div class="menu-back-btn button button-round button-sm button-outline-primary" onclick="closeSubMenu(3)"><img src="imagens/icon-arrow-down-l.svg" width="15" title="Hersa Engenharia" alt="Hersa Engenharia"></div>
            <div class="scroll-wrap">
            <a href="<?=$linkcanonical?><?=$lang?>/sesmt" class="item">SESMT</a>
            <a href="<?=$linkcanonical?><?=$lang?>/ponto-eletronico-web" class="item">Ponto Eletrônico</a>
            <a href="<?=$linkcanonical?><?=$lang?>/solucao-folha-pagamento" class="item">Solução Folha de Pagamento</a>
            <a href="<?=$linkcanonical?><?=$lang?>/e-social" class="item">e-Social</a>
            <a href="<?=$linkcanonical?><?=$lang?>/e-social-faq" class="item">FAQ e-Social</a>
        </div>
    </section>

    <section class="lvl3" id="m3-serv-especializados">
        <div class="scroll-wrap">
            <div class="menu-back-btn button button-round button-sm button-outline-primary" onclick="closeSubMenu(3)"><img src="imagens/icon-arrow-down-l.svg" width="15" title="Hersa Engenharia" alt="Hersa Engenharia"></div>
            <a href="<?=$linkcanonical?><?=$lang?>/bpo-contabil-e-fiscal" class="item">BPO Contábil e Fiscal</a>
            <a href="<?=$linkcanonical?><?=$lang?>/geracao-de-obrigacoes-acessorias" class="item">Geração de Obrigações Acessórias</a>
            <a href="<?=$linkcanonical?><?=$lang?>/recuperacao-de-xml" class="item">Recuperação de XML</a>
        </div>
    </section> -->

    </nav>

    <?php include('paginas/'.$arq);?>

    <?php if($_SESSION['cookies'] == 'true'){} else { ?>
    <div id="modal-cookies" class="modal-container mostrar">
        <div class="modal-box">
            <p>
                <?= $define['index-modal-cookies-p']?>
                <a href="<?=$linkcanonical?>manuais/privacidade.pdf"><?= $define['index-modal-cookies-ahref']?>
                </a> <?= $define['index-modal-cookies-p2']?>
            </p>
            <button id="modal-btn"><?= $define['index-modal-cookies-button']?>
            </button>
        </div>
    </div>
    <?php } ?>


    <footer id="footer" class="rodape">
        <div class="a_hersa_footer institucional_footer">
            <img src="imagens/logo_branco.svg" alt="">
            <span><?= $define['index-footer-span']?>
            </span>
        </div>
        <div class="a_hersa_footer menu_footer">
            <h3><?= $define['index-footer-menu-menu']?>
            </h3>
            <!-- <a href="<?=$linkcanonical?><?=$lang?>/home" class="link_footer">Home</a> -->
            <a href="<?=$linkcanonical?><?=$lang?>/sobre"
                class="link_footer"><?= $define['index-footer-menu-institucional']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/areas-de-atuacao"
                class="link_footer"><?= $define['index-footer-menu-areas-atuacao']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos"
                class="link_footer"><?= $define['index-footer-menu-projetos']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/clientes"
                class="link_footer"><?= $define['index-footer-menu-clientes']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/qsms" class="link_footer"><?= $define['index-footer-menu-qsms']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/noticias"
                class="link_footer"><?= $define['index-footer-menu-noticias']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/compliance"
                class="link_footer"><?= $define['index-footer-menu-compliance']?>
            </a>
            <a href="<?=$linkcanonical?><?=$lang?>/contato"
                class="link_footer"><?= $define['index-footer-menu-contato']?>
            </a>
        </div>
        <div class="a_hersa_footer contatos_footer">
            <h3><?= $define['index-footer-contatos_footer-h3']?>
            </h3>
            <span class="icon_footer"><i><img src="imagens/email.svg"
                        alt=""></i><?= $define['index-footer-contatos_footer-span-email']?>
            </span>
            <span class="icon_footer"><i><img src="imagens/call.svg"
                        alt=""></i><?= $define['index-footer-contatos_footer-span-telefone']?>
            </span>
            <span class="icon_footer contact_location"><i><img src="imagens/location.svg"
                        alt=""></i><?= $define['index-footer-contatos_footer-span-rua']?>

                <br> <?= $define['index-footer-contatos_footer-span-bairro']?>
                <br> <?= $define['index-footer-contatos_footer-span-cidade']?>
            </span>
        </div>
        <div class="a_hersa_footer redes_sociais_footer">
            <h3><?= $define['index-footer-redes_sociais-h3']?>
            </h3>
            <div class="logos_sociais">
                <a href="https://www.linkedin.com/company/hersa-engenharia/" target="_blank" rel="noopener noreferrer"><img src="imagens/icon_footer_in.svg"
                        alt=""></a>
                <a href="http://" target="_blank" rel="noopener noreferrer"><img src="imagens/icon_footer_insta.svg"
                        class="logo-instagram" alt=""></a>
                <!-- <a href="http://" target="_blank" rel="noopener noreferrer"><img src="imagens/icon_footer_face.svg" alt=""></a> -->
            </div>
        </div>

    </footer>

    <div class="rodape_bottom">
        <a href="<?=$linkcanonical?>manuais/privacidade.pdf" target="_blank" rel="noopener noreferrer"><img
                src="imagens/logo_benetton.svg" alt=""></a>
        <span><?= $define['index-footer-rodape_bottom-Pol-Priv']?>
        </span>
    </div>

    <link href="https://fonts.googleapis.com/css?family=Lexend+Deca|Open+Sans:300,400,600,700&display=swap"
        rel="stylesheet">
    <script src="js/scripts.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;700;800;900&display=swap"
        rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@glidejs/glide@3.4.1/dist/css/glide.core.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/@glidejs/glide@3.4.1/dist/glide.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous">
    </script>


    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <!--<script type="text/javascript" src="js/smothscroll/smothscroll.js"></script>-->
    <script type="text/javascript" src="js/skrollr/skrollr.min.js"></script>
    <script type="Text/javascript" src="js/wow/wow.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <script type="text/javascript" src="js/jquery.waypoints.min.js"></script>

    <script type="text/javascript" src="js/swiper/dist/js/swiper.min.js"></script>

    <script type="text/javascript" src="js/fancybox/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="js/scroll-site.js"></script>

    <script type="text/javascript" src="js/funcoes_site.js"></script>

    <script>
        var a = document.getElementsByClassName('glide')
        if (a.length > 0) {
            for (let i = 0; i < a.length; i++) {
                new Glide('#' + a[i].id).mount()
            }
        }
    </script>

    <script type="text/javascript">
        $(function () {
            $("#modal-btn").click(function () {
                const modal = document.getElementById("modal-cookies");
                modal.classList.remove("mostrar");
                $.post("paginas/acoes.php?op=cookies", {
                        cookies: "true"
                    })
                    .done(function (data) {
                        console.log(data)
                    });
            });
        });

        <?php
        if ($mobile == false) {
            ?>
            wow = new WOW({
                mobile: false
            })
            wow.init();

            var s = skrollr.init(); 
            <?php
        } ?>

    </script>


    <script type="text/javascript" async
        src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/d001b9cb-4750-4ba0-8b84-73a8e5ac27ec-loader.js">
    </script>

</body>

</html>