<div class="linear-background">
    <section class="a_hersa_top compliance">
        <h2 class="titulo-compliance"><strong><?= $define['compliance-linear-background-titulo-compliance-strong']?>
            </strong><br><?= $define['compliance-linear-background-titulo-compliance']?>
        </h2>
    </section>
</div>



<section id="infos_compliance">
    <div class="infos">
        <p class="descricao"><?= $define['compliance-infos_compliance-descricao']?>

            <br><br>

            <?= $define['compliance-infos_compliance-descricao-parte2']?>

        </p>
        <div class="docs">

            <!-- INTRODUÇÃO E EXPLICAÇÃO DOS PRINCÍPIOS -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/introducao-principios.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;"
                        class="nome_doc"><?= $define['compliance-texto_arquivos-introducao-principios']?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-introducao-principios-Kbps']?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

            <!-- CÓDIGO DE ÉTICA E CONDUTA -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/codigo-de-etica.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;"
                        class="nome_doc"><?= $define['compliance-texto_arquivos-codigo-de-etica']?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-codigo-de-etica-Kbps']?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

            <!-- POLÍTICA ANTICORRUPÇÃO -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/anticorrupcao.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;" class="nome_doc"><?= $define['compliance-texto_arquivos-anticorrupcao']?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-anticorrupcao-Kbps']?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

            <!-- POLÍTICA DE CONFLITO DE INTERESSES -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/conflito-interesses.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;"
                        class="nome_doc"><?= $define['compliance-texto_arquivos-conflito-interesses']?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-conflito-interesses-Kbps']?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

            <!-- POLÍTICA SOBRE BRINDES, PRESENTES, VIAGENS E HOSPITALIDADES  -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/politicas-brindes.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;" class="nome_doc">
                        <?= $define['compliance-texto_arquivos-politicas-brindes']?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-politicas-brindes-Kbps']?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

            <!-- POLÍTICA DE CONTRATAÇÃO DE TERCEIROS -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/contratacao-terceiros.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;"
                        class="nome_doc"><?= $define['compliance-texto_arquivos-contratacao-terceiros'] ?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-contratacao-terceiros-Kbps'] ?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

            <!-- POLÍTICA DE DOAÇÕES E PATROCÍNIOS -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/doacoes.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;" class="nome_doc"><?= $define['compliance-texto_arquivos-doacoes'] ?>
                    </span>
                    <span style="width: 20%;"
                        class="tamanho_doc"><?= $define['compliance-texto_arquivos-doacoes-Kbps'] ?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>


            <!-- POLÍTICA DE PRIVACIDADE -->
            <li class="texto_arquivos">
                <a href="<?=$linkcanonical?>manuais/privacidade.pdf" target="_blank">
                    <img src="imagens/drive-pdf.svg" alt="">
                    <span style="width: 60%;" class="nome_doc"><?= $define['compliance-texto_arquivos-privacidade'] ?>
                    </span>
                    <span style="width: 20%;" class="tamanho_doc">
                        <?= $define ['compliance-texto_arquivos-privacidade-Kbps']?>
                    </span>
                    <img src="imagens/pdf_seta.svg" alt="" class="icon_pdf_seta">
                </a>
            </li>

        </div>
        <p class="descricao"><?= $define['compliance-infos_compliance-footer-descriacao']?>
            <strong><?= $define['compliance-infos_compliance-footer-descriacao-email']?></strong></p>
    </div>
    <img src="imagens/compliance_right<?=$mobile?'_mobile':''?>.png" alt="" id="img_flutuante">
</section>