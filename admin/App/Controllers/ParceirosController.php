<?php
namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class ParceirosController extends Action
{
    public function listar()
    {
        $dados = Container::getModel("Parceiros");

        $this->view->pesquisar = "";
        $filtro = "";
        if(@$_POST['pesquisar']) {
            $this->view->pesquisar = seguranca($_POST['pesquisar']);
            $palavra = tira_acento( filtro( "string", $this->view->pesquisar ) );
            $filtro = " AND ( HTML_UnEncode(nome) LIKE '%".$palavra."%' ) ";
        }

        $this->view->url = Bootstrap::getUrl();
        $buscaUrl = @explode("/", $this->view->url );
        $id_retorno = @array_pop( $buscaUrl );
        $pagina = is_numeric($id_retorno) ? $id_retorno : 1;
        $this->view->temPag = is_numeric($id_retorno) ? true : false;

        //verifica se tem retorno de insert
        $verif = @array_pop( $buscaUrl );
        if($verif == "ok" || $verif == "erro")
            $this->view->retorno = $verif;
		
        $this->view->pagina = $pagina;
        $this->view->limite = 25;
        $this->view->inicio = ( $this->view->pagina * $this->view->limite ) - $this->view->limite;
        $this->view->resul = $dados->total( $filtro );
        $this->view->totalPaginas = ceil( $this->view->resul / $this->view->limite );

        $this->view->header = "Clientes";
		$this->view->headerSmall = "Listagem de Clientes";
        $this->view->dados = $dados->listar( $filtro, $this->view->inicio, $this->view->limite );
        $this->render("listar");
    }

    public function status()
    {
        $form = retorna_form($_POST);
        $dados = Container::getModel("Parceiros");
        $this->view->dados = $dados->alteraStatus($form['status'],$form['id'],"id");
        return false;
    }

    public function cadastrar()
    {
        $temID = @array_pop( @explode("/", Bootstrap::getUrl() ) );
        $dados = Container::getModel("Parceiros");
        $this->view->header = "Clientes";
		$this->view->headerSmall = "Cadastro e Edição de Clientes";
		$this->view->dados = array();
        if(is_numeric( $temID ) )
            $this->view->dados = $dados->find($temID,"id");
		

        $this->render("cadastrar");
    }
	
    public function cadastro()
    {
        $form = retorna_form($_POST);
		//mostra_array($form);mostra_array($_FILES);die;
        if(isset($_FILES['logo']))
        {
			$tamanho = getimagesize($_FILES['logo']['tmp_name']);
			$l = $tamanho[0] > 500 ? 500 : $tamanho[0];
			$a = $tamanho[1] > 500 ? 500 : $tamanho[1];
            $largura 	= array($l, $l);
            $altura 	= array($a, $a);
            $crop 		= array("","");
            $pb 		= array("","s");
            $caminho 	= array("../imagens/clientes/","../imagens/clientes/pb/");
            $form['logo-string'] = salvaimagem($_FILES["logo"], $largura, $altura, $caminho, $crop,$pb, forma_url( NOME_CLIENTE . "-" . $form['nome-string'] ) );
        }

        $dados = Container::getModel("Parceiros");
        $retorno = $dados->grava($form,"id");
        $urlRet = $retorno ? "ok" : "erro";

        redireciona(BASE_SITE."clientes/listar/".$urlRet."/1");
    }

    public function excluirImg()
    {
        $form = retorna_form($_POST);
	 	$base = str_replace("\\", "/", str_replace( "admin\\" . __NAMESPACE__, "", __DIR__ ) );
		$base .= "imagens/clientes/";
        unlink( $base . $form['imagem'] );
        unlink( $base . "pb/" . $form['imagem'] );
        $dados = Container::getModel("Parceiros");
        $dados->excluiFoto($form['id']);
        return false;
    }



}