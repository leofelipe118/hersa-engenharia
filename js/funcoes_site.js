

$(function () {

    var largurajan = $(window).width();
    var alturajan = $(window).height();
    var coluna = (largurajan + 17) / 12;
    var url = location.href;

    if (url.indexOf('/en') >= 0) {
        var lang = "en";
    } else if (url.indexOf('/es') >= 0) {
        var lang = "es";
    } else {
        var lang = "pt";
    }

    console.log(lang);

    $("#anexar-carreira").change(function(){
        var txtmodal = "Arquivo anexado com sucesso!";
        $.fancybox.open("<div id='janela_aviso'>"+txtmodal+"</div>");
    }); 

    $("#remove_apresenta_video").click(function(){
        $(this).addClass("esconde");
        player.playVideo();
    });
    $("#dispara_carreiras").click(function (){
        var verifica_carreiras = $(".check_carreiras_verifica").is(":checked");
        if(verifica_carreiras){
            $("#form_carreiras").submit();
        } else {
            if(lang == 'pt'){textomodal = '<p>Desculpe! Você precisa concordar com os termos para prosseguir.</p>'} else if(lang == 'en'){textomodal = '<p>Desculpe! Você precisa concordar com os termos para prosseguir.</p>'} else if(lang == 'es'){textomodal = '<p>Desculpe! Você precisa concordar com os termos para prosseguir.</p>'};
            var txtmodal = textomodal;
            $.fancybox.open("<div id='janela_aviso'>"+txtmodal+"</div>");
        }
    });
    $("#dispara_contato").click(function (){
        var verifica_contato = $(".check_contato_verifica").is(":checked");
        if(verifica_contato){
            $("#form_contato").submit();
        } else {
            if(lang == 'pt'){textomodal = '<p>Desculpe! Você precisa concordar com os termos para prosseguir.</p>'} else if(lang == 'en'){textomodal = '<p>Desculpe! Você precisa concordar com os termos para prosseguir.</p>'} else if(lang == 'es'){textomodal = '<p>Desculpe! Você precisa concordar com os termos para prosseguir.</p>'};
            var txtmodal = textomodal;
            $.fancybox.open("<div id='janela_aviso'>"+txtmodal+"</div>");
        }
    });

    if (lang == "en")
        var botaoFechar = "Fechar";
    else
        var botaoFechar = "Fechar";


    $(".bola").click(function () {

        var info = $(this).data("info");
        var slide = $(this).data("slide");

        $(".bola").removeClass("ativo");
        $(".circulo ." + info).addClass("ativo");

        mySwiper2.slideTo(slide, 1200, false);

    })

    if (largurajan <= 1199) {
        var pos1 = null;
        var pos2 = null;
        pos1 = $(window).scrollTop();
        $(window).scroll(function () {
            pos2 = $(window).scrollTop();
            if (pos1 < pos2 && pos1 > 80) {
                // $("nav").removeClass("ativo");
                $(".btn_menu_mob").removeClass("ativo");
                $("nav").addClass("pre_ativo");
                $(".markers_mobile .map").addClass("esconde");
                $(".btn_menu_mob").addClass("pre_ativo");

                pos1 = $(window).scrollTop();
                // console.log('desceu');
            } else {
                // console.log(pos1);

                if (pos1 < 180) {
                    // console.log('topo');
                    // $("nav").removeClass("ativo");
                    $(".btn_menu_mob").removeClass("ativo");
                    $("nav").removeClass("pre_ativo");
                    $(".btn_menu_mob").removeClass("pre_ativo");
                    $(".markers_mobile .map").removeClass("esconde");
                }
                else {
                    // $("nav").addClass("ativo");
                    $(".btn_menu_mob").addClass("ativo");
                    $("nav").addClass("pre_ativo");
                    $(".btn_menu_mob").removeClass("pre_ativo");
                    $(".markers_mobile .map").addClass("esconde");
                }


                pos1 = $(window).scrollTop();
            }
        });
        $(".especialidades .interno .item p").hide();

        $(".especialidades .interno .item").click(function () {
            let ativo = $(this).hasClass('ativo');
            if (ativo) {
                $(".especialidades .interno .item").removeClass('ativo');
                $(this).removeClass('ativo');
                $(".especialidades .interno .item p").hide('slow');
                $(this).find('p').hide('slow');
            } else {
                $(".especialidades .interno .item").removeClass('ativo');
                $(this).addClass('ativo');
                $(".especialidades .interno .item p").hide('slow');
                $(this).find('p').show('slow');
            }
        });

        $(".topo_categoria li .bloco_mobile_interno").hide();

        $(".topo_categoria li .click_mob").click(function () {
            var verif = $(this).parent().find(".bloco_mobile_interno").is(":visible");
            if (verif == true) {
                $(this).parent().find(".bloco_mobile_interno").hide("slow");
                $(".topo_categoria li").removeClass("ativo");
            } else {
                $(this).parent().addClass("ativo");
                $(".topo_categoria li .bloco_mobile_interno").hide();
                $(this).parent().find(".bloco_mobile_interno").show("slow");
            }
        });
    } else {
        var pos1 = null;
        var pos2 = null;
        pos1 = $(window).scrollTop();
        $(window).scroll(function () {
            pos2 = $(window).scrollTop();
            if (pos1 < pos2 && pos1 > 80) {
                $("nav").removeClass("ativo");
                $(".btn_menu_mob").removeClass("ativo");
                $("nav").addClass("pre_ativo");
                $(".btn_menu_mob").addClass("pre_ativo");

                pos1 = $(window).scrollTop();
            } else {

                if (pos1 < 80) {
                    $("nav").removeClass("ativo");
                    $(".btn_menu_mob").removeClass("ativo");
                    $("nav").removeClass("pre_ativo");
                    $(".btn_menu_mob").removeClass("pre_ativo");
                }
                else {
                    $("nav").addClass("ativo");
                    $(".btn_menu_mob").addClass("ativo");
                    $("nav").removeClass("pre_ativo");
                    $(".btn_menu_mob").removeClass("pre_ativo");
                }


                pos1 = $(window).scrollTop();
            }
        });

        $(".nossos_servicos .topo_categoria li").click(function () {
            var val = $(this).data("chama");
            $(".nossos_servicos .apresenta").addClass("esconde");
            $(".nossos_servicos #" + val).removeClass("esconde");
        });

        $(".nossos_servicos .topo_categoria li:first-of-type").addClass("ativo");
        $(".nossos_servicos .topo_categoria li").click(function () {
            $(".nossos_servicos .topo_categoria li").removeClass("ativo");
            $(this).addClass("ativo");
        });
    }

    $(".btn_menu_mob").click(function () {
        $(".menu_mobile").addClass("ativo");
    });

    $(".btn_close_mob").click(function () {
        $(".menu_mobile").removeClass("ativo");
    });

    $("nav .interno .logo").click(function () {
        $("html, body").stop().scrollTo(".topo", 1000);
    });

    $(".anc_especialidades").click(function () {
        $("html, body").stop().scrollTo(".especialidades", 1000);
    });

    $(".btn_down").click(function () {
        $("html, body").stop().scrollTo(".especialidades", 1000);
    });

    $(".anc_pacotes").click(function () {
        $("html, body").stop().scrollTo(".pacotes", 1000);
    });

    $(".anc_profissionais").click(function () {
        $("html, body").stop().scrollTo(".profissionais", 1000);
    });

    $(".anc_onde_estamos").click(function () {
        $("html, body").stop().scrollTo(".mapa", 1000);
    });

    $(".anc_fale_conosco").click(function () {
        $("html, body").stop().scrollTo(".contato", 1000);
    });

    $(".slideToOrc").click(function () {
        $("html, body").stop().scrollTo(".orcamento", 1000);
    });

    
    $(".menu-wrap .menu").click(function () {
        let verif = $(this).hasClass("ativo");
        if (verif) {
            $(this).removeClass("ativo");
            $(".menu_hamb_op").removeClass("ativo");
        } else {
            $(this).addClass("ativo");
            $(".menu_hamb_op").addClass("ativo");
        }
    });

    $(".newsletter_li_concordo li input").click(function () {
        $(this).parent().find("span .out").toggleClass("ativo");
        if ($('#arquivoDemo').is(':checked')) {
            $("#arquivoDemo").prop('checked', true);
            $("#arquivoDemo").attr('value', 'aceito');
        }
        else {
            $("#arquivoDemo").prop('checked', false);
            $("#arquivoDemo").attr('value', '');
        }
    });

    var swiper = new Swiper('.banner_home', {
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    var swiper = new Swiper('.projetos_home_slides', {
        loop: true,
        autoHeight: true,
        navigation: {
            nextEl: '.paginacao .next',
            prevEl: '.paginacao .prev',
        },
    });

    var swiper = new Swiper('.GaleriaProjetos', {
        loop: false,
        autoHeight: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    


    var mySwiperProfissionais = new Swiper('.prof_items', {
        navigation: {
            nextEl: '.prof_next',
            prevEl: '.prof_prev',
        },
        pagination: {
            el: '.paginacao_banner',
            clickable: true
        },
        spaceBetween: 30,
        slidesPerView: 4,
        speed: 1800,
        scrollbarDraggable: true,
        preventClicks: true,
        centeredSlides: false,
        loop: false,
        freeMode: false,
        freeModeMomentum: true,
        freeModeSticky: true,
        breakpoints: {
            1199: {
                spaceBetween: 10,
                slidesPerView: 1
            }
        }
    });

    var mySwiperCarrouselBanner2 = new Swiper('.noticias_slides', {
        navigation: {
            nextEl: '.slide-next',
            prevEl: '.slide-prev',
        },
        spaceBetween: 0,
        slidesPerView: 3,
        speed: 1800,
        autoplay: {
            delay: 5000,
        },
        scrollbarDraggable: true,
        preventClicks: true,
        centeredSlides: false,
        loop: false,
        freeMode: false,
        freeModeMomentum: true,
        freeModeSticky: true,
        breakpoints: {
            1199: {
                parallax: false,
                slidesPerView: 3,
                slidesPerGroup: 1
            },
            767: {
                spaceBetween: 30,
                slidesPerView: 1,
                speed: 1800,
                autoplay: {
                    delay: 5000,
                },
                scrollbarDraggable: true,
                preventClicks: true,
                centeredSlides: true,
                loop: false,
                freeMode: false,
                freeModeMomentum: true,
                freeModeSticky: true,
            }
        }
    });

    $(".nossos_servicos .apresenta").addClass("esconde");
    $(".nossos_servicos .apresenta:first-of-type").removeClass("esconde");



    var mySwiperCarrouselBanner3 = new Swiper('.bloco_clientes', {
        navigation: {
            nextEl: '.next_cliente',
            prevEl: '.prev_cliente',
        },
        pagination: {
            el: '.paginacao_cliente',
            clickable: true
        },
        spaceBetween: 30,
        slidesPerView: 3,
        
        speed: 1800,
        autoplay: {
            delay: 5000,
        },
        scrollbarDraggable: true,
        preventClicks: true,
        centeredSlides: false,
        loop: false,
        freeMode: false,
        freeModeMomentum: true,
        freeModeSticky: true,
        breakpoints: {
            1199: {
                parallax: false,
                slidesPerView: 1,
            },
            767: {
                spaceBetween: 30,
          
                slidesPerView: 1,
                speed: 1800,
                autoplay: {
                    delay: 5000,
                },
                scrollbarDraggable: true,
                preventClicks: true,
                centeredSlides: true,
                loop: false,
                freeMode: false,
                freeModeMomentum: true,
                freeModeSticky: true,
            }
        }
    });

    // var mySwiperCarrouselBanner4 = new Swiper('.galeriahtj', {
    //     navigation: {
    //         nextEl: '.next_foto',
    //         prevEl: '.prev_foto',
    //     },
    //     pagination: {
    //         el: '.paginacao_foto',
    //         clickable: true 
    //     },
    //     spaceBetween: 30,
    //     slidesPerView: 1,
    //     speed: 1800,
    //     autoplay: false,
    //     scrollbarDraggable: true,
    //     preventClicks: true,
    //     centeredSlides: true,
    //     loop: false,
    //     freeMode: false,
    //     freeModeMomentum: true,
    //     freeModeSticky: true,
    //     breakpoints: {
    //         1199: {
    //             parallax: false,
    //             slidesPerView: 1
    //         },
    //         767: {
    //             spaceBetween: 0,
    //             slidesPerView: 1,
    //             speed: 1800,
    //             autoplay: true,
    //             scrollbarDraggable: true,
    //             preventClicks: true,
    //             centeredSlides: true,
    //             loop: false,
    //             freeMode: false,
    //             freeModeMomentum: true,
    //             freeModeSticky: true,
    //         }
    //     }
    // });


    /////////////////////////// VALIDAÇÕES DO FORMULÁRIO /////////////////////
    $("input[name='telefone']").blur(function () {
        var e = $(this).val().length;
        14 > e && $(this).val("");
    });
    $("input[name='celular']").blur(function () {
        var e = $(this).val().length;
        14 > e && $(this).val("");
    });

    $("#formcontato .campo input").focus(function () {
        $(this).next().next().css("opacity", "1");
        $(this).parent().addClass("ativa_campo");
    });

    $("#formcontato .campo input").blur(function () {
        if ($(this).val() != "") {
            $(this).parent().addClass("ativa_campo");
            $(this).next().next().css("opacity", "1");
        } else {
            $(this).parent().removeClass("ativa_campo");
            $(this).next().next().css("opacity", "0");
        }
    });

    $("#formcontato .campo input.required").blur(function () {
        if ($(this).val() != "") {
            $(this).parent().removeClass("error");
            $(this).removeClass("required");
            $(this).parent().addClass("ativa_campo");
            $(this).next().next().css("opacity", "1");
        } else {
            $(this).parent().addClass("error");
            $(this).addClass("required");
            $(this).parent().removeClass("ativa_campo");
            $(this).next().next().css("opacity", "0");
        }
    });

    $("#formcontato .campomensagem textarea").focus(function () {
        $(this).parent().addClass("ativa_campo");
    });

    $("#formcontato .campomensagem textarea").blur(function () {
        if ($(this).val() != "") {
            $(this).removeClass("required");
            $(this).parent().removeClass("error");
            $(this).parent().addClass("ativa_campo");
        } else {
            $(this).addClass("required");
            $(this).parent().addClass("error");
            $(this).parent().removeClass("ativa_campo");
        }
    });


    var validador = 0;

    $("#formcontato .btn_envia").click(function () {
        $('#formcontato .campo .required').each(function () {
            if ($(this).val() == "") {
                $(this).parent().addClass('error');
                validador++;
            }
            else
                $(this).parent().removeClass('error');
        });

        $('#formcontato .campomensagem .required').each(function () {
            if ($(this).val() == "") {
                $(this).parent().addClass('error');
                validador++;
            }
            else
                $(this).parent().removeClass('error');
        });

        if (validador > 0) {
            validador = 0;
            textomodal = 'Ops!<br>Você deve preencher corretamente <br>todos os campos obrigatórios marcados em vermelho.';
            if (lang == "en")
                textomodal = 'Ops!<br>Você deve preencher corretamente <br>todos os campos obrigatórios marcados em vermelho.';
            if (lang == "es")
                textomodal = 'Ops!<br>Você deve preencher corretamente <br>todos os campos obrigatórios marcados em vermelho.';

            var txtmodal = "<p>" + textomodal + "</p>";
            $.fancybox.open("<div id='janela_aviso'>" + txtmodal + "</div>");
            return false;
        }
        else
            $("#formcontato").submit();
        return false;
    });

    var verificaemail;
    $("#formcontato input[name='email']").keyup(function () {
        if ($(this).val() != "")
            verificaemail = is_email($(this).val());
    });

    $(".prof_items .main .item").click(function () {
        let id = $(this).attr("id");
        let conteudo = $("#" + id).html();
        $.fancybox.open("<div id='janela_aviso'>" + conteudo + "</div>");
    });

    $(".btn_open_drop").parent().find('.texto_pacote').hide("slow");

    $(".btn_open_drop").click(function () {
        let ativo = $(this).hasClass('ativo');
        if (ativo) {
            $(this).removeClass('ativo');
            $(this).parent().find('.texto_pacote').hide("slow");
        } else {
            $(".btn_open_drop").removeClass('ativo');
            $(".btn_open_drop").parent().find('.texto_pacote').hide("slow");
            $(this).addClass('ativo');
            $(this).parent().find('.texto_pacote').show("slow");
        }
        // console.log(ativo);
    });

    $("input[name='telefone']").blur(function () {
        if ($(this).val().length < 14)
            $(this).val("");
    });


    var textomodal = "";
    if (url.indexOf('emailok') >= 0) {
        if (lang == 'pt') { textomodal = '<p>Seu e-mail foi enviado!<br> Em breve entraremos em contato com você.</p>' } else if (lang == 'en') { textomodal = '<p>Your email has been sent!<br> Soon, we will contact you.</p>' } else if (lang == 'es') { textomodal = '<p>¡Su e-mail ha sido enviado!<br> En breve entraremos en contacto con usted.</p>' };
        var txtmodal = textomodal;
        $.fancybox.open("<div id='janela_aviso'>" + txtmodal + "</div>");
    }
    if (url.indexOf('emailerro') >= 0) {
        if (lang == 'pt') { textomodal = '<p>Desculpe houve um erro ao enviar seu e-mail.<br> Por favor entre em contato conosco<br> pelo telefone 19 3363-5967</p>' } else if (lang == 'en') { textomodal = '<p>Sorry there was an error while sending your email.<br> Please contact us<br> by phone 19 3499-1555</p>' } else if (lang == 'es') { textomodal = '<p>Lo sentimos, un error al enviar su e-mail.<br> Por favor, póngase en contacto con nosotros<br> al teléfono 19 3499-1555</p>' };
        var txtmodal = textomodal;
        $.fancybox.open("<div id='janela_aviso'>" + txtmodal + "</div>");
    }

});



// removeModal('modal-cookies');

const button = document.getElementById('modal-btn');
if(button){
    button.addEventListener("click", () => {
    
        const modal = document.getElementById("modal-cookies");
        modal.classList.remove("mostrar");
    
    });
}

// var swiper = new Swiper('#principais-clientes-swiper', {
//     navigation: {
//       nextEl: '.swiper-button-next',
//       prevEl: '.swiper-button-prev',
//     },
//   });


var mySwiperCarrouselBanner3 = new Swiper('#principais-clientes-swiper', {
    navigation: {
        nextEl: '.next_cli',
        prevEl: '.prev_cli',
    },
    pagination: {
        el: '.paginacao_cliente',
        clickable: true
    },
    spaceBetween: 30,
    slidesPerView: 7,
    slidesPerGroup: 1,
    speed: 1800,
    autoplay: {
        delay: 5000,
    },
    scrollbarDraggable: true,
    preventClicks: true,
    centeredSlides: false,
    loop: false,
    freeMode: false,
    freeModeMomentum: true,
    freeModeSticky: true,
    breakpoints: {
        1199: {
            parallax: false,
            slidesPerView: 3,
            autoplay: {
                delay: 5000,
            },
           
        },
        767: {
            spaceBetween: 10,
            slidesPerView: 3,
            speed: 1800,
            autoplay: {
                delay: 5000,
            },
            scrollbarDraggable: true,
            preventClicks: true,
            centeredSlides: true,
            loop: false,
            freeMode: false,
            freeModeMomentum: true,
            freeModeSticky: true,
        }
    }
});


// ===*Select*===
const selected = document.querySelector(".selected");
const optionsContainer = document.querySelector(".options-container");

const optionsList = document.querySelectorAll(".option");

if(selected){
    selected.addEventListener("click", () => {
        optionsContainer.classList.toggle("active");
    });
    
    optionsList.forEach(o => {
        o.addEventListener("click", () => {
          selected.innerHTML = o.querySelector("label").innerHTML;
          optionsContainer.classList.remove("active");
        });
    });
}

// ===*Fim Select*===
