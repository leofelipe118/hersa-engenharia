<?php
// mostra_array($arrProj);
?>
<section class="home_1">

    <div class="swiper-container banner_home">
        <div class="swiper-wrapper">
            <?php
            if($arrBan):
                foreach ($arrBan as $banner => $b):
                ?>
                <div class="swiper-slide banner">
                    <img src="imagens/banners/large/<?=$b['foto']?$b['foto']:'img_banner.jpg'?>" alt="" class="img_banner fadeInLeft wow" data-wow-delay="0.6s" data-wow-duration="2s">
                    <div class="texto_banner">
                        <h3 class="titulo fadeInUp wow"><?=htmlspecialchars_decode($b['titulo_'.$lang]?$b['titulo_'.$lang]:$b['titulo_pt'], ENT_QUOTES)?></h3>
                        <span class="descricao fadeInUp wow"><?=htmlspecialchars_decode($b['subtitulo_'.$lang]?$b['subtitulo_'.$lang]:$b['subtitulo_pt'], ENT_QUOTES)?></span>
                    </div>
                </div>
            <?php 
                endforeach;
            else: 
            ?>
                <div class="swiper-slide banner">
                    <img src="imagens/img_banner.jpg" alt="" class="img_banner fadeInLeft wow" data-wow-delay="0.6s" data-wow-duration="2s">
                    <div class="texto_banner">
                        <h3 class="titulo fadeInUp wow"><strong>Construindo</strong> soluções para empresas há mais de <strong>25 anos</strong></h3>
                        <span class="descricao fadeInUp wow">Contamos com profissionais diferenciados e uma equipe técnica focada na qualidade e excelência na execução de obras e projetos de engenharia.</span>
                    </div>
                </div>
            <?php endif; ?>
            <!-- <div class="swiper-slide banner">
                <img src="imagens/banner_home_2.png" alt="" class="img_banner">
                <div class="texto_banner_2">
                    <div class="title-wrapper">
                        <h3 class="titulo">
                            Estamos <strong>em todo Brasil</strong> e levamos o nosso DNA para 
                            <strong>grandes projetos</strong> e em diversas áreas de atuação
                        </h3>
                    </div>
                </div>
            </div>
            <div class="swiper-slide banner">
                <img src="imagens/banner_home_3.png" alt="" class="img_banner">
               
                
            </div>
            <div class="swiper-slide banner">
                <img src="imagens/banner_home_4.png" alt="" class="img_banner">
               
            </div> -->
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
         <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
    <!-- <div class="banner">
        <img src="imagens/img_banner.jpg" alt="" class="img_banner">
        <div class="texto_banner">
            <h3 class="titulo"><strong>Construindo</strong> soluções para empresas há mais de <strong>25 anos</strong></h3>
            <span class="descricao">Contamos com profissionais diferenciados e equipe técnica focada na qualidade e excelência na execução de obras e projetos de engenharia.</span>
        </div>
    </div> -->
</section>
<section class="home_2">
    <h2 class="titulo fadeInUp wow" data-wow-delay="0.2s" data-wow-duration="2s">ÁREAS DE ATUAÇÃO</h2>
    <div class="areas_atuacao">
        <div class="itens fadeInUp wow" data-wow-duration="2s">
            <img src="imagens/icon_civil.svg" alt="" class="icon_item">
            <span class="descricao">Civil</span>
        </div>
        <img src="imagens/linha_lat_area.svg" alt="" class="fadeInUp wow atuacao_linha_lat_area" data-wow-delay="0.5s" data-wow-duration="1s">
        <div class="itens fadeInUp wow" data-wow-delay="0.5s" data-wow-duration="1.5s">
            <img src="imagens/icon_eletrica.svg" alt="" class="icon_item">
            <span class="descricao">elétrica</span>
        </div>
        <img src="imagens/linha_lat_area.svg" alt="" class="fadeInUp wow atuacao_linha_lat_area" data-wow-delay="1s" data-wow-duration="1s">
        <div class="itens fadeInUp wow" data-wow-delay="1s" data-wow-duration="1.5s">
            <img src="imagens/icon_hidraulica.svg" alt="" class="icon_item">
            <span class="descricao">hidráulica</span>
        </div>
        <img src="imagens/linha_lat_area.svg" alt="" class="fadeInUp wow atuacao_linha_lat_area" data-wow-delay="1.5s" data-wow-duration="1s">
        <div class="itens fadeInUp wow" data-wow-delay="1.5s" data-wow-duration="1.5s">
            <img src="imagens/icon_mec.svg" alt="" class="icon_item">
            <span class="descricao">mecânica</span>
        </div>
        <img src="imagens/linha_lat_area.svg" alt="" class="fadeInUp wow atuacao_linha_lat_area" data-wow-delay="2s" data-wow-duration="1s">
        <div class="itens fadeInUp wow" data-wow-delay="2s" data-wow-duration="1.5s">
            <img src="imagens/icon_system.svg" alt="" class="icon_item">
            <span class="descricao">sistemas</span>
        </div>
    </div>
    <a href="<?=$linkcanonical?><?=$lang?>/obras" class="fadeInLeft wow" data-wow-delay="2s" data-wow-duration="2s">Conheça nossas obras <i><img src="imagens/back.svg" alt=""></i></a>
</section>
<!-- <div class="linhaMaior"></div> -->

<section class="home_projetos">
    <div class="swiper-container projetos_home_slides">
        <div class="swiper-wrapper">

        <?php
            if($arrProj):
                foreach ($arrProj as $projeto => $p):
                ?>
                <div class="swiper-slide banner">
                    <img src="imagens/projetos/large/<?=$p['foto']?$p['foto']:''?>" alt="" class="img_projetos">
                    <div class="intro">
                        <h2 class="titulo">Acompanhe os nossos principais <strong>projetos</strong></h2>
                        <span class="descricao_top">Acompanhe os principais projetos desenvolvidos pela Hersa Engenharia</span>
                    <div class="info">
                            <h2 class="titulo"><?=html_entity_decode($p['titulo_'.$lang]?$p['titulo_'.$lang]:$p['titulo_pt'], ENT_QUOTES)?></h2>
                            <span class="descricao"><?=html_entity_decode($p['texto_'.$lang]?$p['texto_'.$lang]:$p['texto_pt'], ENT_QUOTES)?></span>
                            <a href="<?=$linkcanonical?><?=$lang?>/obras">Ver todos <i><img src="imagens/arrow_projetos_home.svg" alt=""></i></a>
                    </div>
                    </div>
                    <div class="paginacao">
                        <img src="imagens/paginacao.svg" alt="" class="prev">
                        <img src="imagens/paginacao.svg" alt="" class="next">
                    </div>

                </div>











                <!-- <div class="swiper-slide banner">
                    <img src="imagens/banners/large/<?=$b['foto']?$b['foto']:'img_banner.jpg'?>" alt="" class="img_banner fadeInLeft wow" data-wow-delay="0.6s" data-wow-duration="2s">
                    <div class="texto_banner">
                        <h3 class="titulo fadeInUp wow"><?=html_entity_decode($b['titulo_'.$lang]?$b['titulo_'.$lang]:$b['titulo_pt'], ENT_QUOTES)?></h3>
                        <span class="descricao fadeInUp wow"><?=html_entity_decode($b['subtitulo_'.$lang]?$b['subtitulo_'.$lang]:$b['subtitulo_pt'], ENT_QUOTES)?></span>
                    </div>
                </div> -->
            <?php 
                endforeach;
            endif; 
        ?>
            <!-- <div class="swiper-slide banner">
                <img src="imagens/banner_projetos.jpg" alt="" class="img_projetos">
                <div class="intro">
                    <h2 class="titulo">Acompanhe os nossos principais <strong>projetos</strong></h2>
                    <span class="descricao_top">Acompanhe os principais projetos desenvolvidos pela Hersa Engenharia</span>
                    <div class="info">
                        <h2 class="titulo">Marinha – COGESN</h2>
                        <span class="descricao">Construção dos edifícios de escritórios de projetos com 5 pavimentos e prédio anexo para paióis e depósitos, na Cidade Universitária. Área Total Construída: 2.860,00 m²</span>
                        <a href="<?=$linkcanonical?><?=$lang?>/obras">Ver todos <i><img src="imagens/arrow_projetos_home.svg" alt=""></i></a>
                    </div>
                </div>
                <div class="paginacao">
                    <img src="imagens/paginacao.svg" alt="" class="prev">
                    <img src="imagens/paginacao.svg" alt="" class="next">
                </div>

            </div>
            <div class="swiper-slide banner">
                <img src="imagens/banner_projetos.jpg" alt="" class="img_projetos">
                <div class="intro">
                    <h2 class="titulo">Acompanhe os nossos <strong>principais projetos</strong></h2>
                    <span class="descricao">Acompanhe os principais projetos desenvolvidos pela Hersa Engenharia</span>
                    <div class="info">
                        <h2 class="titulo">Marinha – COGESN</h2>
                        <span class="descricao">Construção dos edifícios de escritórios de projetos com 5 pavimentos e prédio anexo para paióis e depósitos, na Cidade Universitária. Área Total Construída: 2.860,00 m²</span>
                        <a href="<?=$linkcanonical?><?=$lang?>/obras">Ver todos <i><img src="imagens/arrow_projetos_home.svg" alt=""></i></a>
                    </div>
                </div>
                <div class="paginacao">
                    <img src="imagens/paginacao.svg" alt="" class="prev">
                    <img src="imagens/paginacao.svg" alt="" class="next">
                </div>
 
            </div> -->
        </div>
    </div>
</section>

<section class="home_clientes">
    <h2 class="titulo">PRINCIPAIS CLIENTES</h2>
    <div class="linha"></div>
    <span class="descricao">Temos entre os nossos principais clientes algumas das maiores e mais importantes empresas do Brasil</span>
    <!-- <div class="slides">
        <img src="imagens/c-bovespa.svg" alt="" class="img_clientes">
        <img src="imagens/c-caixa.svg" alt="" class="img_clientes">
        <img src="imagens/c-cptm.svg" alt="" class="img_clientes">
        <img src="imagens/c-ford.svg" alt="" class="img_clientes">
        <img src="imagens/c-governosp.svg" alt="" class="img_clientes">
        <img src="imagens/c-halbert.svg" alt="" class="img_clientes">
        <img src="imagens/c-hsirio.svg" alt="" class="img_clientes">
        
    </div> -->
    <div class="principais-clientes-swiper">
        <div class="swiper-container" id="principais-clientes-swiper">
            <div class="swiper-wrapper slides">
                <?php foreach ($arrCli as $cliente => $c): ?>
                    <img src="imagens/clientes/<?=$c['logo']?>" alt="" class="img_clientes swiper-slide">
                <?php endforeach; ?>
                    <!-- <img src="imagens/c-bovespa.svg" alt="" class="img_clientes swiper-slide">
                    <img src="imagens/c-caixa.svg" alt="" class="img_clientes swiper-slide">
                    <img src="imagens/c-cptm.svg" alt="" class="img_clientes swiper-slide">
                    <img src="imagens/c-ford.svg" alt="" class="img_clientes swiper-slide">
                    <img src="imagens/c-governosp.svg" alt="" class="img_clientes swiper-slide">
                    <img src="imagens/c-halbert.svg" alt="" class="img_clientes swiper-slide">
                    <img src="imagens/c-hsirio.svg" alt="" class="img_clientes swiper-slide"> -->
            </div>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next next_cli"></div>
        <div class="swiper-button-prev prev_cli"></div>
    </div>
   

</section>
<section class="home_noticias">
    <h2 class="titulo">NOTÍCIAS</h2>
    <div class="linha"></div>
    <span class="descricao">Fique por dentro de tudo que acontece na Hersa Engenharia</span>
    <div class="noticias">
        
    <?php
        $noticias = @$arrBlog;
        // mostra_array($produtos);
        if($noticias):
            foreach ($noticias as $indice => $noticia):?>
        <div class="noticia">
            <img src="imagens/posts/large/<?=@$noticia['foto']?>" alt="" class="img_noticia">
            <div class="infos">
                <span class="data"><?=explode("-",$noticia['data'])[2]." ".$arr_meses_pt['abrev'][explode("-",$noticia['data'])[1]].", ".explode("-",$noticia['data'])[0]?></span>
                <h3 class="titulo"><?= @$noticia['titulo_' . $lang] ? html_entity_decode( trim($noticia['titulo_' . $lang]), ENT_QUOTES) : html_entity_decode( trim($noticia['titulo_pt']), ENT_QUOTES); ?></h3>
                <h4 class="descricao"><?= @$noticia['texto_' . $lang] ? html_entity_decode( trim($noticia['texto_' . $lang]), ENT_QUOTES) : html_entity_decode( trim($noticia['texto_pt']), ENT_QUOTES); ?></h4>
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/<?=@$noticia['url']?>"><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>
        <?php
            endforeach;
        endif;
        ?>
        <!-- <div class="noticia">
            <img src="imagens/noticia_2.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">14 jan, 2019</span>
                <h3 class="titulo">Refrotif AC Camargo Câncer Center</h3>
                <h4 class="descricao">Assinamos este mês o contrato para retrofit do Edifício Castro Alves em São Paulo/SP visando a implantação de uma nova unidade de atendimento do AC Camargo Câncer Center, com área total de 5.105,00 m².</h4>
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1"><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>
        <div class="noticia">
            <img src="imagens/noticia_3.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">6 jan, 2019</span>
                <h3 class="titulo">Datacenter do Banrisul</h3>
                <h4 class="descricao">Em janeiro/2019 iniciamos a obra de construção do Datacenter do Banrisul em Porto Alegre/RS, com área total de 3.700,00 m².</h4>
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1"><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>     
        <div class="noticia">
            <img src="imagens/noticia_3.jpg" alt="" class="img_noticia">
            <div class="infos">
                <span class="data">6 jan, 2019</span>
                <h3 class="titulo">Datacenter do Banrisul</h3>
                <h4 class="descricao">Em janeiro/2019 iniciamos a obra de construção do Datacenter do Banrisul em Porto Alegre/RS, com área total de 3.700,00 m².</h4>
                <a href="<?=$linkcanonical?><?=$lang?>/noticias/1"><span class="ler_mais">Ler mais +</span></a>
            </div>
        </div>      -->
    </div>
   
   
    <a href="<?=$linkcanonical?><?=$lang?>/noticias" class="ir_noticias">Mais notícias <i><img src="imagens/back.svg" alt=""></i></a>
</section>
     

<div id="modal-cookies" class="modal-container mostrar">
    <div class="modal-box fadeInDown wow" data-wow-delay="0.5s" data-wow-duration="1s">
        <p>Para melhorar a sua experiência de navegação, utilizamos de cookies, entre outras tecnologias. De acordo com a nossa Política de Privacidade, ao continuar navegando, você aceita estas condições. Acesse nossa <a href="">Política de Privacidade</a> e confira como tratamos os dados pessoais na Hersa.</p>
        <button id="modal-btn">Aceitar</button>
    </div>

</div>

<script src="../js/funcoes_site.js"></script> 
