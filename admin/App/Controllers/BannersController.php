<?php
namespace App\Controllers;

use CRO\Controller\Action;
use CRO\DI\Container;
use CRO\Init\Bootstrap;

class BannersController extends Action
{
    public function listar()
    {
        $lista = Container::getModel("Banners");
        $this->view->header = "Banners";
        $this->view->headerSmall = "Listagem e ordenação dos Banners";
        $url = Bootstrap::getUrl();
        $tipourl = strpos(  $_SESSION[DB_DATABASE]['rotaUrl'], "/banners/listarRodape" ) !== false ? "r" : (strpos(  $_SESSION[DB_DATABASE]['rotaUrl'], "/bannershome/listar" ) !== false ? "h" : "n");
		$this->view->tipo = $tipourl;
		$this->view->imagens = $lista->fetchWhere( " AND tipo = '" . $this->view->tipo . "' ", " status desc, ordem, id DESC " );

        $this->render("listagem");
    }

    public function cadastrar()

    {
		$arrUrl = @explode("/", Bootstrap::getUrl() );
        $temID = @array_pop( $arrUrl );
        $dados = Container::getModel("Banners");
        $this->view->header = "Banners";
        $this->view->headerSmall = "Cadastro/Edição de Banner";
		$this->view->bannerTipo = "n";//banner normal
        if(is_numeric( $temID ) )
            $this->view->dados = $dados->find($temID);
		
		if( in_array( "bannerFixo", $arrUrl ) || $temID == "bannerFixo" ){
            $this->view->bannerTipo = "f";//banner fixo
            $this->view->dados['id'] = "1";
        }
            
        if( in_array( "bannershome", $arrUrl ) || $temID == "bannershome" )
			$this->view->bannerTipo = "h";//banner fixo

		if( in_array( "cadastrarRodape", $arrUrl ) || $temID == "cadastrarRodape" )
            $this->view->bannerTipo = "r";//banner Rodape
		
        $this->render("cadastrar");
    }
	
    public function cadastro()
    {
        $form = retorna_form($_POST);
		
        //mostra_array($form);
        $form['titulo_pt-string'] = limpaTexto( $form['titulo_pt-string'] );
        $form['titulo_en-string'] = limpaTexto( $form['titulo_en-string'] );
        $form['titulo_es-string'] = limpaTexto( $form['titulo_es-string'] );
        $form['subtitulo_pt-string'] = limpaTexto( $form['subtitulo_pt-string'] );
        $form['subtitulo_en-string'] = limpaTexto( $form['subtitulo_en-string'] );
        $form['subtitulo_es-string'] = limpaTexto( $form['subtitulo_es-string'] );
		if( isset( $form['descricao-string'] ) )
			$form['descricao-string'] = limpaTexto( $form['descricao-string'] );
        if( isset( $_FILES['files'] ) )
            unset( $_FILES['files'] );
        if($form['tipo-string']=="f"){
            $form['descricao-string'] = "";
            $form['status-string'] = "s";
        }

        // mostra_array($form);mostra_array($_FILES);
        // die;

        if(isset($_FILES['logo']))
        {
            $tam = getimagesize( $_FILES['logo']['tmp_name'] );
            $largura 	= array( $tam[0] , 384 );
            $altura 	= array( $tam[1] , 216 );
            $crop 		= array("","");
            $pb 		= $form['tipo-string'] != "r" ? array("","") : array("s","s");
            $caminho 	= array("../imagens/banners/large/","../imagens/banners/thumb/");
            $form['foto-string'] = salvaimagem($_FILES["logo"], $largura, $altura, $caminho, $crop,$pb, forma_url(NOME_CLIENTE) );
        }
        if(isset($_FILES['logo_mobile']))
        {
            $tam = getimagesize( $_FILES['logo_mobile']['tmp_name'] );
            $largura 	= array( $tam[0] , 384 );
            $altura 	= array( $tam[1] , 216 );
            $crop 		= array("","");
            $pb 		= $form['tipo-string'] != "r" ? array("","") : array("s","s");
            $caminho 	= array("../imagens/banners/large/","../imagens/banners/thumb/");
            $form['foto_mobile-string'] = salvaimagem($_FILES["logo_mobile"], $largura, $altura, $caminho, $crop,$pb, forma_url(NOME_CLIENTE) );
        }

        $dados = Container::getModel("Banners");
        $retorno = $dados->grava($form);
        $urlRet = $retorno ? "ok" : "erro";
		
		if( $form['tipo-string'] == "f" )
			redireciona(BASE_SITE."bannersfixo/bannerFixo/" . $urlRet . "/1");
		else
		{
            $pagina = "listar";
            if( $form['tipo-string'] == "r" ){
                $pagina .= "Rodape";
                
                redireciona(BASE_SITE."banners/" . $pagina . "/" . $urlRet . "/1");
            }
            if( $form['tipo-string'] == "h" ){
                redireciona(BASE_SITE."bannershome/listar/" . $urlRet . "/1");
            }
            if( $form['tipo-string'] == "n" ){
                redireciona(BASE_SITE."bannerscarossel/listar/" . $urlRet . "/1");
            }  
        }
        

    }

    public function excluirImg()
    {
        $form = retorna_form($_POST);
        unlink( "../imagens/banners/large/".$form['imagem'] );
        unlink( "../imagens/banners/thumb/".$form['imagem'] );
        $dados = Container::getModel("Banners");
        $dados->excluiFoto($form['id']);
        return false;
    }

    public function status()
    {
        $lista = Container::getModel("Banners");
        $lista->status( retorna_form($_POST) );
    }

    public function ordem()
    {
        $lista = Container::getModel("Banners");
        $lista->ordem( retorna_form($_POST) );
    }

    public function exclui()
    {
        $lista = Container::getModel("Banners");
        $lista->exclui( retorna_form($_POST) );
    }

    public function excluiTodas()
    {
        $lista = Container::getModel("Banners");
        $lista->excluiTodas( retorna_form($_POST) );
    }

}