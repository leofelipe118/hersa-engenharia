<section class="projetos_background">


</section>

<a href="<?=$linkcanonical?><?=$lang?>/projetos" class="icon_backToPage"><img src="imagens/right-arrow.png" alt="voltar"
        srcset=""></a>


<div class="projetos_nav">

    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm" <?= $n['categoria'] == 'dm' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav-DataCenter']?> </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" <?= $n['categoria'] == 'h' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos-projetos_nav-Hospitais']?></a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds" <?= $n['categoria'] == 'ds' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav--Defesa-Seguranca']?></a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t" <?= $n['categoria'] == 't' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos-projetos_nav-Transportes']?></a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m" <?= $n['categoria'] == 'm' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav-Manutencao']?></a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=d" <?= $n['categoria'] == 'd' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos-projetos_nav-Diversos']?></a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c" <?= $n['categoria'] == 'c' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav-Concessoes']?></a>

</div>

<!-- <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm" <?= $n['categoria'] == 'dm' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav-DataCenter']?>
 </a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" <?= $n['categoria'] == 'h' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos-projetos_nav-Hospitais']?>
</a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds" <?= $n['categoria'] == 'ds' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav--Defesa-Seguranca']?>
</a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t" <?= $n['categoria'] == 't' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos-projetos_nav-Transportes']?>
</a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=mf" <?= $n['categoria'] == 'mf' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav-Manutencao']?>
</a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=d" <?= $n['categoria'] == 'd' ? 'class="active color"' : 'class="color"'?>><?= $define['projetos-projetos_nav-Diversos']?>
</a>
    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c" <?= $n['categoria'] == 'c' ? 'class="active"' : ''?>><?= $define['projetos-projetos_nav-Concessoes']?>
</a> -->


<div class=" projetos_wrapper">

    <div class="text_projetos">
        <h2><?= $define['projetos-projetos_wrapper-text_projetos']?>
            <strong><?= $define['projetos-projetos_wrapper-text_projetos-strong']?>
            </strong></h2>
    </div>


    <div class="select">
        <div class="container-select-filter">

            <div class="select-box">
                <div class="options-container">
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm" class="option">
                        <input type="radio" class="radio" id="sdataCenter" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm'"
                            for="sdataCenter"><?= $define['projetos-select-options-DataCenter']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" class="option">
                        <input type="radio" class="radio" id="sHospitais" name="category" />
                        <label onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h'"
                            for="sHospitais"><?= $define['projetos-select-options-Hospitais']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds" class="option">
                        <input type="radio" class="radio" id="sDefesa" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds'"
                            for="sDefesa"><?= $define['projetos-select-options--Defesa-Seguranca']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t" class="option">
                        <input type="radio" class="radio" id="sTransportes" name="category" />
                        <label onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t'"
                            for="sTransportes"><?= $define['projetos-select-options-Transportes']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m" class="option">
                        <input type="radio" class="radio" id="sManutencao" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m'"
                            for="sManutencao"><?= $define['projetos-select-options-Manutencao']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/" class="option">
                        <input type="radio" class="radio" id="sDiversos" name="category" />
                        <label onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/'"
                            for="sDiversos"><?= $define['projetos-select-options-Diversos']?>
                        </label>
                    </a>
                    <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c" class="option">
                        <input type="radio" class="radio" id="sConcessoes" name="category" />
                        <label
                            onclick="javascript:location.href='<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c'"
                            for="sConcessoes"><?= $define['projetos-select-options-Concessoes']?>
                        </label>
                    </a>
                </div>

                <div class="selected">
                    <?= $define['projetos-selected-filtro']?>
                </div>
            </div>
        </div>

    </div>


    <?php
    $lado = 'right';
    if($arrNoticias):
        foreach ($arrNoticias as $banner => $b):
            if($lado=='right'){
                $lado = 'left';
            } else {
                $lado = 'right';
            }
        ?>
    <section class="projetos">
        <?php if($lado=='left'): ?>
        <div class="img_proj"
            style="background-image: url('imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>');">
        </div>
        <!-- <img src="imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>" alt=""> -->

        <?php endif; ?>
        <div class="intro<?=$lado=='right'?'Right':''?>">
            <h2 class="titulo">
                <?=html_entity_decode($b['titulo_'.$lang]?$b['titulo_'.$lang]:$b['titulo_pt'], ENT_QUOTES)?></h2>
            <h3 class="descricao">
                <?=html_entity_decode($b['texto_'.$lang]?$b['texto_'.$lang]:$b['texto_pt'], ENT_QUOTES)?></h3>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$b['url']?>/<?=$b['id']?>" class="ir_ver_mais"><?= $define['projetos-verMais']?>
<i><img src="imagens/back_gray.svg" alt=""></i></a>
        </div>
        <?php if($lado=='right'): ?>
        <div class="img_proj"
            style="background-image: url('imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>');">
        </div>
        <!-- <img src="imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>" alt=""> -->
        <?php endif; ?>
    </section>

    <?php 
        endforeach;
    endif; ?>


    <div class="pagination">
        <?php if($totalPaginas==1):else:?>
        <div class="pagination-arrow">
            <a <?=$op?($op==1?'':'href="'.$linkcanonical.$lang.'/projetos/'.(@$op-1).'"'):""?>>
                <img class="left" src="imagens/arrow-pagination.svg" alt="" srcset="">
            </a>
        </div>
        <?php for ($i = 1; $i <= $totalPaginas; $i++) { ?>
        <a class="pagination-item<?=$op && $op==$i?" active":(!$op && $i==1?" active":"")?>"
            href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$i?>"><?=$i?></a>
        <?php } ?>
        <div class="pagination-arrow">
            <a
                <?=$op?($op==@$totalPaginas?'':'href="'.$linkcanonical.$lang.'/projetos/'.(@$op+1).'"'):'href="'.$linkcanonical.$lang.'/projetos/2"'?>>
                <img src="imagens/arrow-pagination.svg" alt="" srcset="">
            </a>
        </div>
        <?php endif; ?>


        <!-- <script>
            const selected = document.querySelector(".selected");
            const optionsContainer = document.querySelector(".options-container");

            const optionsList = document.querySelectorAll(".option");

            selected.addEventListener("click", () => {
                optionsContainer.classList.toggle("active");
                });

            optionsList.forEach(o => {
                o.addEventListener("click", () => {
                selected.innerHTML = o.querySelector("label").innerHTML;
                optionsContainer.classList.remove("active");
                });
            });
        </script>  -->



    </div>

</div>