<section class="projetos_background">
   
    
</section>

<div class="projetos_nav">
    
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm">Data Center e Missão Crítica</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" class="color">Hospitais e Instituições de Saúde</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds">Defesa e Segurança</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t"class="color">Transportes</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m">Manutenção e Facilities</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/"class="color">Diversos</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c">Concessões</a>
 
    
</div>

<div class=" projetos_wrapper">

    <div class="text_projetos">
        <h2>Conheça <strong>nossos projetos<strong></h2>
    </div>

    <?php
    $lado = 'right';
    if($arrNoticias):
        foreach ($arrNoticias as $banner => $b):
            if($lado=='right'){
                $lado = 'left';
            } else {
                $lado = 'right';
            }
        ?>
        <section class="projetos" >
            <?php if($lado=='left'): ?>
                <img src="imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>" alt="">
            <?php endif; ?>
            <div class="intro<?=$lado=='right'?'Right':''?>">
                <h2 class="titulo"><?=html_entity_decode($b['titulo_'.$lang]?$b['titulo_'.$lang]:$b['titulo_pt'], ENT_QUOTES)?></h2>
                <h3 class="descricao"><?=html_entity_decode($b['texto_'.$lang]?$b['texto_'.$lang]:$b['texto_pt'], ENT_QUOTES)?></h3>
                <a href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$b['url']?>/<?=$b['id']?>" class="ir_ver_mais">Ver mais<i><img src="imagens/back_gray.svg" alt=""></i></a>
            </div>
            <?php if($lado=='right'): ?>
                <img src="imagens/projetos/large/<?=$b['foto']?$b['foto']:'projetos-image-1.png'?>" alt="">
            <?php endif; ?>
        </section>




        <!-- <div class="swiper-slide banner">
            <img src="imagens/banners/large/<?/*=$b['foto']?$b['foto']:'img_banner.jpg'?>" alt="" class="img_banner fadeInLeft wow" data-wow-delay="0.6s" data-wow-duration="2s">
            <div class="texto_banner">
                <h3 class="titulo fadeInUp wow"><?=html_entity_decode($b['titulo_'.$lang]?$b['titulo_'.$lang]:$b['titulo_pt'], ENT_QUOTES)?></h3>
                <span class="descricao fadeInUp wow"><?=html_entity_decode($b['subtitulo_'.$lang]?$b['subtitulo_'.$lang]:$b['subtitulo_pt'], ENT_QUOTES)*/?></span>
            </div>
        </div> -->
    <?php 
        endforeach;
    endif; ?>

    <!-- <section class="projetos" >
        <img src="imagens/projetos-image-1.png" alt="">
        <div class="intro">
            <h2 class="titulo">Banco do Brasil – Datacenter Complexo Verbo Divino</h2>
            <h3 class="descricao">Modernização do Datacenter do Complexo Verbo Divino do Banco do Brasil. Sistema de Geração: 6.000 kVA Sistema de UPS/No break: 2.0000 kVA Área: 2.500,00 m²</h3>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos" class="ir_ver_mais">Ver mais<i><img src="imagens/back_gray.svg" alt=""></i></a>
        </div>
    </section>
    <section class="projetos" >
        <div class="introRight">
            <h2 class="titulo"> BANRISUL – DATACENTER</h2>
            <h3 class="descricao"> Construção do Edifício Data Center do Banrisul na cidade de Porto Alegre/RS incluindo a execução de obras civis, elétricas, mecânicas, lógicas, segurança e automação, equipamentos e PPCI. </h3>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos" class="ir_ver_mais">Ver mais<i><img src="imagens/back_gray.svg" alt=""></i></a>

        </div>
        <img src="imagens/projetos-image-2.png" alt="">
    </section>

    <section class="projetos" >
        <img src="imagens/projetos-image-3.png" alt="">
        <div class="intro">
            <h2 class="titulo">SENAI – Edif. Theobaldo de Nigris</h2>
            <h3 class="descricao">Reforma e ampliação do Edifício Theobaldo de Nigris do SENAI/SP, localizado na Av. Paulista, 750 . Área Total: 29.186,00 m² </h3>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos" class="ir_ver_mais">Ver mais<i><img src="imagens/back_gray.svg" alt=""></i></a>
        </div>
    </section>
 
    <section class="projetos" id="sis_esp">
        <div class="introRight">
            <h2 class="titulo">AC CAMARGO CÂNCER CENTER</h2>
            <h3 class="descricao"> Retrofit do Edifício Castro Alves em São Paulo/SP visando a implantação de uma nova unidade de atendimento do AC Camargo Câncer Center. Área Total de 5.105,00 m²</h3>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos" class="ir_ver_mais">Ver mais<i><img src="imagens/back_gray.svg" alt=""></i></a>
        </div>
        <img src="imagens/projetos-image-4.png" alt="">
    </section> -->

    <div class="pagination">
        <?php if($totalPaginas==1):else:?>
            <div class="pagination-arrow">
                <a <?=$op?($op==1?'':'href="'.$linkcanonical.$lang.'/projetos/'.(@$op-1).'"'):""?>>
                    <img class="left" src="imagens/arrow-pagination.svg" alt="" srcset="">
                </a>
            </div>
            <?php for ($i = 1; $i <= $totalPaginas; $i++) { ?>
                <a class="pagination-item<?=$op && $op==$i?" active":(!$op && $i==1?" active":"")?>" href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$i?>"><?=$i?></a>
            <?php } ?>
            <div class="pagination-arrow">
                <a <?=$op?($op==@$totalPaginas?'':'href="'.$linkcanonical.$lang.'/projetos/'.(@$op+1).'"'):'href="'.$linkcanonical.$lang.'/projetos/2"'?>>
                    <img src="imagens/arrow-pagination.svg" alt="" srcset="">
                </a>
            </div>
        <?php endif; ?>
        
            <!-- <img class="left" src="imagens/arrow-pagination.svg" alt="" srcset="">
      
        <span>01</span>
        <span>02</span>
        <span>03</span>
        <span>04</span>
        <span>05</span>
        <span>06</span>
        <span>07</span>
        
            <img src="imagens/arrow-pagination.svg" alt="" srcset=""> -->
      
    </div>
 
</div>