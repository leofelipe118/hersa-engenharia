<section class="obras_topo">
    <div class="introducao">
        <h2 class="titulo"><?= $define['obras_interno-introducao-titulo']?>
            <strong><?= $define['obras_interno-introducao-titulo-strong']?>
            </strong> <?= $define['obras_interno-introducao-titulo-parte2']?></h2>
        <span class="descricao"><?= $define['obras_interno-introducao-descricao']?> <img src="imagens/arrow_lado.svg"
                alt=""></span>
    </div>
    <div class="filtros">
        <p class="intro"><?= $define['obras_interno-filtros-intro']?>
        </p>
        <div class="campos">
            <div class="filtro_categoria filtros_int">
                <span class="item_filtro categoria"><?= $define['obras_interno-campos']?>
                    <i><img src="imagens/icon_arrow_filtro.svg" alt="" class="seta"></i></span>
                <div class="filtros_disponiveis">
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-concessoes']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-Data_Center']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-Defesa-Seg']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-Diversos']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-Hospitais']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-Manutencao']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro-Transportes']?>
                    </span>
                </div>
            </div>
            <div class="filtro_status filtros_int">
                <span class="item_filtro status"><?= $define['obras_interno-campos-filtro_status']?>
                    <i><img src="imagens/icon_arrow_filtro.svg" alt="" class="seta"></i></span>
                <div class="filtros_disponiveis">
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro_status-top']?>
                    </span>
                    <span class="filtro" value=""><?= $define['obras_interno-campos-filtro_status-bottom']?>
                    </span>
                </div>
            </div>
            <div class="filtro_cliente filtros_int">
                <span class="item_filtro cliente"><?= $define['obras_interno-campos-filtro_cliente']?>
                    <i><img src="imagens/icon_arrow_filtro.svg" alt="" class="seta"></i></span>
            </div>
        </div>
    </div>
</section>

<section id="obra_interno">
    <div class="banner">
        <!-- <img src="imagens/bg_obra_interno.jpg" alt=""> -->
        <div class="img_banner" style="background-image: url('imagens/projetos/large/bg_obra_interno.jpg');">

        </div>
    </div>
    <div class="infos">
        <h2 class="titulo_obra"><?= $define['obras_interno-#obras_interno-banner-info-titulo']?>
        </h2>
        <p class="descricao_obra"><?= $define['obras_interno-#obras_interno-banner-info-descricao']?>
            <br><?= $define['obras_interno-#obras_interno-banner-info-descricao-parte-2']?>
            <br><?= $define['obras_interno-#obras_interno-banner-info-descricao-parte-3']?>
        </p>
        <div class="informacoes_obra status">
            <span><?= $define['obras_interno-#obras_interno-informacoes_obra-status-top']?>
            </span>
            <span><?= $define['obras_interno-#obras_interno-informacoes_obra-status-bottom']?>
            </span>
        </div>
        <div class="informacoes_obra categoria">
            <span><?= $define['obras_interno-#obras_interno-informacoes_obra-categoria-top']?>
            </span>
            <span><?= $define['obras_interno-#obras_interno-informacoes_obra-categoria-bottom']?>
            </span>
        </div>
        <div class="redes_sociais">
            <a href="http://" target="_blank">
                <img src="imagens/icon_linkedin.svg" alt="">
            </a>
            <a href="http://" target="_blank">
                <img src="imagens/icon_face.svg" alt="">
            </a>
            <a href="http://" target="_blank">
                <img src="imagens/icon_insta.svg" alt="">
            </a>
        </div>
    </div>
</section>

<section id="projetos_recomendados">
    <h3 class="titulo"><?= $define['obras_interno-#projetos_recomendados-titulo']?>
    </h3>
    <div class="recomendados">
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <!-- <img src="imagens/obra_int_ministerio.jpg" alt=""> -->
                <div class="img_projeto"
                    style="background-image: url('imagens/projetos/large/obra_int_ministerio.jpg');">

                </div>
                <h3 class="subtitulo"><?= $define['obras_interno-#projetos_recomendados-recomendados-subtitulo']?>
                </h3>
                <span class="descricao"><?= $define['obras_interno-#projetos_recomendados-recomendados-descricao']?>
                </span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <!-- <img src="imagens/obra_int_ciscea.jpg" alt=""> -->
                <div class="img_projeto" style="background-image: url('imagens/projetos/large/obra_int_ciscea.jpg');">

                </div>
                <h3 class="subtitulo"><?= $define['obras_interno-#projetos_recomendados-projeto-subtitulo']?>
                </h3>
                <span class="descricao"><?= $define['obras_interno-#projetos_recomendados-projeto-descricao']?>
                </span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <!-- <img src="imagens/obra_int_marinha.jpg" alt=""> -->
                <div class="img_projeto" style="background-image: url('imagens/projetos/large/obra_int_marinha.jpg');">

                </div>
                <h3 class="subtitulo"><?= $define['obras_interno-#projetos_recomendados-projeto2-subtitulo']?>
                </h3>
                <span class="descricao"><?= $define['obras_interno-#projetos_recomendados-projeto2-descricao']?>
                </span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <!-- <img src="imagens/obra_int_cnh.jpg" alt=""> -->
                <div class="img_projeto" style="background-image: url('imagens/projetos/large/obra_int_cnh.jpg');">

                </div>
                <h3 class="subtitulo"><?= $define['obras_interno-#projetos_recomendados-projeto3-subtitulo']?>
                </h3>
                <span class="descricao"><?= $define['obras_interno-#projetos_recomendados-projeto3-descricao']?>
                </span>
            </div>
        </a>
    </div>
</section>

<script type="text/javascript">
    $(function () {
        $(".filtros_int").click(function (e) {
            var classe = $(this).attr("class");

            if (classe.indexOf('ativo') >= 0) {
                $(this).find(".seta").removeClass("ativo");
                $(this).find(".filtros_disponiveis").removeClass("ativo");
                $(this).removeClass("ativo");
            } else {
                $(this).find(".seta").addClass("ativo");
                $(this).find(".filtros_disponiveis").addClass("ativo");
                $(this).addClass("ativo");
            }
        });
    })
</script>