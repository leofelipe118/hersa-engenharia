<?php

namespace App\Controllers;


use CRO\Controller\Action;
use CRO\DI\Container;


class NotificaController extends Action
{
    public function sistemaHoras()
    {
        $verif = Container::getModel("Notifica");
        $this->view->verif = $verif->verifSH();
        $this->render("notifica",false);
    }

    public function buscaMsg()
    {
        $verif = Container::getModel("Notifica");
        $this->view->verif = $verif->verifMsg();
        $this->render("mensagens",false);
    }

}