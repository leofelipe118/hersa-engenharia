<!-- <div class="projetos_nav noticias-interno">
    
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=dm">Data Center e Missão Crítica</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=h" class="color">Hospitais e Instituições de Saúde</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=ds">Defesa e Segurança</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=t"class="color">Transportes</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=m">Manutenção e Faciaties</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/" class="color">Diversos</a>
        <a href="<?=$linkcanonical?><?=$lang?>/projetos/busca?busca=c">Concessões</a>
 
    
</div> -->

<!-- <section id="obra_interno">
    <div class="banner">
        <img src="imagens/posts/large/<?=$n['foto']?$n['foto']:'bg_obra_interno.jpg'?>" alt="">
    </div>
    <div class="infos">
        <h2 class="titulo_obra"><?=html_entity_decode($n['titulo_'.$lang]?$n['titulo_'.$lang]:$n['titulo_pt'], ENT_QUOTES)?></h2>
        <p class="descricao_obra"><?=html_entity_decode($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt'], ENT_QUOTES)?></p>

       <p  class="obras-intero-descricao-up">Status: <span class="espacamento"> </span> Obra em Andamento</p>
       <p class="obras-intero-descricao-down">Categorias:<span class="espacamento"></span>Datacenter e Missão Crítica</p>

        <div class="redes_sociais">
            <?php if($n['url_linkedin']): ?>
                <a href="<?=$n['url_linkedin']?>" target="_blank">
                    <img src="imagens/icon_linkedin.svg" alt="">
                </a>
            <?php endif; ?>
            <?php if($n['url_facebook']): ?>
                <a href="<?=$n['url_facebook']?>" target="_blank">
                    <img src="imagens/icon_face.svg" alt="">
                </a>
            <?php endif; ?>
            <?php if($n['url_instagram']): ?>
                <a href="<?=$n['url_instagram']?>" target="_blank">
                    <img src="imagens/icon_insta.svg" alt="">            
                </a>
            <?php endif; ?>
        </div>
    </div>
</section> -->

<section class="noticias_interno_wrapper">
    <div class="noticiais_interno_imagem">
     <img src="imagens/posts/large/<?=$n['foto']?$n['foto']:'bg_obra_interno.jpg'?>" alt="">
     <div class="noticias_interno_bullet">
        <div class="circle"></div>
        <div class="circle"></div>
        <div class="circle"></div> 
     </div>
    </div>
<div class="noticias_interno_bullet"></div>

<div class="noticias_interno_text">

    <h3><?=htmlspecialchars_decode($n['titulo_pt']);?></h3>
    <p><?=htmlspecialchars_decode($n['texto_pt']);?></p>


    <div class="noticiais_interno_redes_sociais">
        <?php if($n['url_linkedin']): ?>
                <a href="<?=$n['url_linkedin']?>" target="_blank">
                    <img src="imagens/icon_linkedin.svg" alt="">
                </a>
            <?php endif; ?>
       
            <?php if($n['url_instagram']): ?>
                <a href="<?=$n['url_instagram']?>" target="_blank">
                    <img src="imagens/icon_insta.svg" alt="">            
                </a>
            <?php endif; ?>
    </div>
</div>


</section>
<section id="projetos_recomendados">
    <h3 class="titulo"><?= $define['noticias_interno-#projetos_recomendados-titulo']?>
 </h3>
    <div class="recomendados">
        <?php foreach ($arrNoticiasRel as $noticias => $n): 
        $texto = htmlspecialchars_decode($n['texto_'.$lang]);
        // $texto = str_replace("<h3>", "</p><h3>", $texto);
        // $texto = str_replace("</h3>", "</h3><p>", $texto);
        // $texto = str_replace("<h2>", "</p><h2>", $texto);
        // $texto = str_replace("</h2>", "</h2><p>", $texto);
            ?>
            <a href="<?=$linkcanonical?><?=$lang?>/projetos/<?=$n['url']?>/<?=$n['id']?>" rel="noopener noreferrer">
                <div class="projeto">
                    <div class="contem_imagem" style="background-image: url('imagens/projetos/large/<?=$n['foto']?$n['foto']:''?>');"></div>
                    <!-- <img src="imagens/projetos/large/<?=$n['foto']?$n['foto']:''?>" alt=""> -->
                    <h3 class="subtitulo"><?=html_entity_decode($n['titulo_'.$lang]?$n['titulo_'.$lang]:$n['titulo_pt'], ENT_QUOTES)?></h3>
                    <!-- <span class="descricao"><?=html_entity_decode($n['texto_'.$lang]?$n['texto_'.$lang]:$n['texto_pt'], ENT_QUOTES)?></span> -->
                    <span class="descricao"><?=$texto?></span>
                </div>
            </a>
        <?php endforeach; ?>
        <!-- <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <img src="imagens/obra_int_ciscea.jpg" alt="">
                <h3 class="subtitulo">CISCEA – Modernização de Sistemas de Energia e Climatização</h3>
                <span class="descricao">Reforma e Modernização dos Sistemas Elétricos e de Climatização em 09 Destacamentos...</span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <img src="imagens/obra_int_marinha.jpg" alt="">
                <h3 class="subtitulo">Marinha – COGESN</h3>
                <span class="descricao">Construção dos edifícios de escritórios de projetos com 5 pavimentos e prédio anexo para paióis e...</span>
            </div>
        </a>
        <a href="<?=$linkcanonical?><?=$lang?>/obras/1" target="_blank" rel="noopener noreferrer">
            <div class="projeto">
                <img src="imagens/obra_int_cnh.jpg" alt="">
                <h3 class="subtitulo">CNH – Centro de Distribuição Sorocaba</h3>
                <span class="descricao">Construção do Prédio da Subestação (SUB2) do Laboratório de Geração de Energia...</span>
            </div>
        </a>     -->
    </div>
</section>

<script type="text/javascript">
    $(function(){
        $(".filtros_int").click(function(e){
            var classe = $(this).attr("class");

            if(classe.indexOf('ativo') >= 0 )
            {
                $(this).find(".seta").removeClass("ativo");
                $(this).find(".filtros_disponiveis").removeClass("ativo");
                $(this).removeClass("ativo");
            }
            else
            {
                $(this).find(".seta").addClass("ativo");
                $(this).find(".filtros_disponiveis").addClass("ativo");
                $(this).addClass("ativo");
            }
        });
    })
</script>

<script src="../js/funcoes_site.js"></script>