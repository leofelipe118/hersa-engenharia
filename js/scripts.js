const scrollNextSection = (event) => {
    scrollToItem(event.target.closest('section').nextElementSibling)
}

const scrollToItem = (item) => {
    var diff=(item.offsetTop-window.scrollY)/8
    if (Math.abs(diff)>1) {
        window.scrollTo(0, (window.scrollY+diff))
        clearTimeout(window._TO)
        window._TO=setTimeout(scrollToItem, 30, item)
    } else {
        window.scrollTo(0, item.offsetTop)
    }
}

const toggleDropdown = (el) => {
    var a = document.getElementById(el);
    if(a.style.height == '0px' || a.style.height == 0)
        a.style.height = `${a.scrollHeight}px`;
    else
        a.style.height = `0px`;
}

const toggleMenu = () => {
    if(document.body.classList.contains('openmenu'))
        document.body.classList.remove('openmenu')
    else
        document.body.classList.add('openmenu')
}

const openSubMenu = (el, lvl, btn) => {
    var a = document.getElementsByClassName("lvl"+lvl)
    if(btn.closest){
        var b = btn.closest(".lvl"+(lvl-1)).querySelectorAll('#main-menu .item')
    }
    for (let i = 0; i < a.length; i++) {
        a[i].classList.remove('open')
    }
    for (let i = 0; i < b.length; i++) {
        b[i].classList.remove('active')
    }
    btn.classList.add('active')
    document.getElementById(el).classList.add('open')
}

const closeSubMenu = (lvl) => {
    // console.log(1);
    var a = document.getElementsByClassName("lvl"+lvl)
    for (let i = 0; i < a.length; i++) {
        a[i].classList.remove('open')
    }
}

const toggleTextBubble = (btn, id) => {
    var a = document.getElementsByClassName('text-bubble')
    var b = document.getElementById(id)
    var c = document.getElementsByClassName('text-bubble-trigger')
    var d = btn.classList.contains('open')
    for (let i = 0; i < a.length; i++) {
        a[i].classList.remove('open')
    }
    for (i = 0; i < c.length; i++) {
        c[i].classList.remove('open')
    }
    if(!d){
        b.querySelector('.text-bubble-h').style.left = (btn.parentElement.parentElement.offsetLeft + (btn.parentElement.parentElement.offsetWidth/2) - 20) + 'px'
        b.classList.add('open')
        btn.classList.add('open')
    }
}

const openTab = (tab, group, btn) => {
    var a = document.getElementsByClassName(group)
    var b = btn.parentElement.getElementsByClassName('tab')
    for (let i = 0; i < a.length; i++) {
        a[i].classList.remove('active')
        b[i].classList.remove('active')
    }
    document.getElementById(tab).classList.add('active')
    btn.classList.add('active')

}

var rollBtns = document.getElementsByClassName('next-section-btn')
for (let i = 0; i < rollBtns.length; i++) {
    rollBtns[i].addEventListener('click', scrollNextSection, false)
}

var maskCnpj = function(o) {
    var key = event.keyCode || event.charCode;
    if(key == 8 || key == 46){
        return;
    } 
    setTimeout(function() {
        v = o.value;
        var r = v.replace(/\D/g, "");
        r = r.replace(/^0/, "");
        if (r.length > 10) {
            r = r.replace(/^(\d{2})(\d{3})?(\d{3})?(\d{4})?(\d{2})?/, "$1.$2.$3/$4-$5");
        }
        if (r != o.value) {
            o.value = r;
        }
    }, 1);
};

var maskPhone = function(o) {
    var key = event.keyCode || event.charCode;
    if(key == 8 || key == 46){
        return;
    } 
    setTimeout(function() {
        v = o.value;
        var r = v.replace(/\D/g, "");
        r = r.replace(/^0/, "");
        if (r.length > 10) {
            r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
        } else if (r.length > 5) {
            r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
        } else if (r.length > 2) {
            r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
        } else {
            r = r.replace(/^(\d*)/, "($1");
        }
        if (r != o.value) {
            o.value = r;
        }
    }, 1);
}

var maskDate = (o) => {
    var v = o.value;
    var key = event.keyCode || event.charCode;
    if(key == 8 || key == 46){
        return;
    } 
    if (v.match(/^\d{2}$/) !== null) {
        o.value = v + '/';
    } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
        o.value = v + '/';
    }
}

var maskCep = (o) => {
    var v = o.value;
    var key = event.keyCode || event.charCode;
    if(key == 8 || key == 46){
        return;
    }   
    if (v.match(/^\d{5}$/) !== null) {
        o.value = v + '-';
    }
}

var lastScrollTop = 0;
var stickMenu = () => {
    var st = window.pageYOffset || document.documentElement.scrollTop;
    if(st < 200){
        document.getElementById("main-header").classList.add('top')
    }else{
        document.getElementById("main-header").classList.remove('top')
    }
    if (st > lastScrollTop && st > 200) {
        document.getElementById("main-header").classList.remove('sticky')
    } else {
        document.getElementById("main-header").classList.add('sticky')
    }
    lastScrollTop = st;
}
stickMenu();
window.addEventListener("scroll", stickMenu, false);

/* ==== custom scrollbar ==== */
var scrollContainer = document.querySelectorAll('.custom-scroll'),
    //scrollContentWrapper = document.querySelector('.custom-scroll .content-wrapper'),
    scrollContent = document.querySelectorAll('.custom-scroll .content'),
    contentPosition = 0,
    scrollerBeingDragged = false,
    //scroller,
    topPosition,
    scrollerHeight;

function calculateScrollerHeight(scrollCont, wrapper) {
    // *Calculation of how tall scroller should be
    var visibleRatio = scrollCont.offsetHeight / wrapper.scrollHeight;
    return visibleRatio * scrollCont.offsetHeight;
}

function moveScroller(evt, scrollCont, wrapper, scroller) {
    // Move Scroll bar to top offset
    var scrollPercentage = evt.target.scrollTop / wrapper.scrollHeight;
    topPosition = scrollPercentage * (scrollCont.offsetHeight - 5); // 5px arbitrary offset so scroll bar doesn't move too far beyond content wrapper bounding box
    scroller.style.top = topPosition + 'px';
}

function startDrag(evt, wrapper) {
    normalizedPosition = evt.pageY;
    contentPosition = wrapper.scrollTop;
    scrollerBeingDragged = true;
}

function stopDrag(evt) {
    scrollerBeingDragged = false;
}

function scrollBarScroll(evt, scrollCont, wrapper) {
    if (scrollerBeingDragged === true) {
        var mouseDifferential = evt.pageY - normalizedPosition;
        var scrollEquivalent = mouseDifferential * (wrapper.scrollHeight / scrollCont.offsetHeight);
        wrapper.scrollTop = contentPosition + scrollEquivalent;
    }
}

function createScroller(scrollCont, wrapper) {
    // *Creates scroller element and appends to '.custom-scroll' div
    // create scroller element
    var scroller = document.createElement("div");
    scroller.className = 'scroller';

    // determine how big scroller should be based on content
    scrollerHeight = calculateScrollerHeight(scrollCont, wrapper);
    
    if (scrollerHeight / scrollCont.offsetHeight < 1){
        // *If there is a need to have scroll bar based on content size
        scroller.style.height = scrollerHeight + 'px';

        // append scroller to scrollContainer div
        scrollCont.appendChild(scroller);
        
        // show scroll path divot
        scrollCont.className += ' showScroll';
        
        // attach related draggable listeners
        scroller.addEventListener('mousedown', function(e){startDrag(e, wrapper)});
        window.addEventListener('mouseup', function(e){stopDrag(e)});
        window.addEventListener('mousemove', function(e){scrollBarScroll(e, scrollCont, wrapper)});
    }
    return scroller;
}

scrollContainer.forEach(function(scrollCont){
    var wrapper = scrollCont.querySelector('.content-wrapper');
    var scroller = createScroller(scrollCont, wrapper);
    wrapper.addEventListener('scroll', function(e){moveScroller(e, scrollCont, wrapper,scroller)});
});
/* ==== custom scrollbar ==== */