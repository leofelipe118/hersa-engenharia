<section class="trabalhe_conosco_top">
    <div class="bg">
        <h2 class="titulo"><strong><?= $define['trabalhe-conosco-trabalhe_conosco_top-bg-titulo-strong']?>
            </strong></h2>
        <div class="filtro"></div>
    </div>
    <div class="form_trabalhe_conosco">
        <img src="imagens/trabalhe_conosco.jpg" alt="">
        <div class="infos_form">
            <h3 class="descricao">P<?= $define['trabalhe-conosco-form_trabalhe_conosco-descricao']?>
                <strong><?= $define['trabalhe-conosco-form_trabalhe_conosco-descricao-strong']?>
                </strong></h3>
            <form action="" id="form_trabalhe_conosco">
                <input type="text" name="nome" id="nome"
                    placeholder=<?= $define['trabalhe-conosco-form-input-#nome-placeholder']?>>
                <input type="text" name="email" id="email"
                    placeholder=<?= $define['trabalhe-conosco-form-input-#email-placeholder']?>>
                <input type="tel" autocomplete="off"
                    placeholder=<?= $define['trabalhe-conosco-form-input-#telefone-placeholder']?> name="telefone"
                    maxlength="15" onkeyup="mascara(this, mtel);" class="required">
                <div class="botoes">
                    <div class="botao">
                        <input type="file" accept=".pdf,.PDF,.doc,.DOC,.docx,.DOCX" name="anexo" class="file_curriculo"
                            id="file-input" required="">
                        <label class="anexar_curriculo"
                            for="file-input"><?= $define['trabalhe-conosco-form-botoes-label']?>
                            <i><img src="imagens/icon_plus.svg" alt="" class="icon"></i></label>
                        <small><small><?= $define['trabalhe-conosco-form-botoes-label-small']?>
                            </small></small>
                    </div>
                    <div class="botao">
                        <button class="enviar_form"><?= $define['trabalhe-conosco-form-botoes-enviar_form']?>
                            <i class="seta"><img src="imagens/back.svg" alt="" class="icon"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>