<?php

$define = array();


// ===*Areas de Atuacao*===
 

$define['areas_atuacao_wrapper-titulo'] = 'ÁREAS DE ATUAÇÃO';
$define['areas_atuacao_wrapper-titulo-#eng-civil'] = 'Engenharia Civil';
$define['areas_atuacao_wrapper-descrica-#eng-civil'] = 'Construções e reformas prediais, comerciais e industriais, retrofit, bases e estruturas para equipamentos e infraestrutura underground.';
$define['areas_atuacao_wrapper-titulo-#eng-ele'] = 'Engenharia Elétrica';
$define['areas_atuacao_wrapper-descricao-#eng-ele'] = 'Subestações Transformadoras de Energia, Sistemas de Geração de Energia, Instalações Elétricas em Média e Baixa Tensão, Sistemas de Eficiência Energética, Iluminação, Aterramento e Proteção contra Descargas Atmosférica – SPDA.';
$define['areas_atuacao_wrapper-titulo-#eng-hid'] = 'Engenharia Hidráulica';
$define['areas_atuacao_wrapper-descricao-#eng-hid'] = 'Redes de água potável, pluvial e quente, centrais de água gelada (CAG), esgoto sanitário, sistemas de combate à incêndio e gás combustível.';
$define['areas_atuacao_wrapper-titulo-#eng_mec'] = 'Engenharia Mecânica';
$define['areas_atuacao_wrapper-descricao-#eng_mec'] = 'Centrais e tubulações de fluidos, gases especiais e ar comprimido, sistemas de tratamento de efluentes industriais e sistemas de ventilação e climatização.';
$define['areas_atuacao_wrapper-titulo-#sis_esp'] = 'Sistemas Especiais';
$define['areas_atuacao_wrapper-descricao-#sis_esp'] = 'Sistemas especiais de missão crítica, automação e supervisão predial, detecção, alarme e combate a incêndios, controle de acesso, CFTV, sonorização, telecomunicações, dados e imagem e cabeamento estruturado.';
$define['areas_atuacao_wrapper-titulo-#sis_con'] = 'Concessões';
$define['areas_atuacao_wrapper-descricao-#sis_con'] = 'Contratos de Concessão de serviços públicos e Parcerias Público Privadas visando a gestão de bens móveis e imóveis de forma racional e eficiente.';

// ===*Blog *===
 
$define['blog-title-section-1'] = 'Conteúdo de';
$define['blog-title-section-bold'] = 'confiança e credibilidade';
$define['blog-title-section-2'] = 'para empresários de pequenas, médias e grandes empresas.';
$define['blog-title-Confidence-IT'] = 'CONFIDENCE IT';
$define['blog-bg-primary-container-h3'] = 'Assine a nossa newsletter e não perca nenhuma novidade! ';
$define['blog-bg-primary-container-button'] = 'Assinar';
// ===*Blog Interno*===
$define['bloginterno-ConfidenceIT'] = 'CONFIDENCE IT';
$define['bloginterno-bg-primary-container-h3'] = 'Assine a nossa newsletter e não perca nenhuma novidade! ';
$define['bloginterno-bg-primary-container-button'] = 'Assinar';
// ===*Clientes*===

$define['clientes-title-Data-Center'] = 'Data Center';
$define['clientes-title-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['clientes-title-Defesa-Seg'] = 'Defesa e Segurança';
$define['clientes-title-Transportes'] = 'Transportes';
$define['clientes-title-Manutencao-Facilities'] = 'Manutenção e Facilities';
$define['clientes-title-Ensino'] = 'Instituições de Ensino';
$define['clientes-title-Orgaos-Judiciais'] = 'Órgãos Judiciais';
$define['clientes-title-Prefeituras'] = 'Prefeituras';
$define['clientes-title-Conc-Servicos'] = 'Concessionárias de Serviços';
$define['clientes-title-Seg-Diversos'] = 'Segmentos Diversos';
$define['clientes-title-Concessoes'] = 'Concessões';

// ===*Compliance*===
$define['compliance-linear-background-titulo-compliance-strong'] = 'Compliance,';
$define['compliance-linear-background-titulo-compliance'] = 'ética e conduta';

$define['compliance-infos_compliance-descricao'] = 'A Hersa Engenharia estabelece o mais alto padrão de conduta para seus colaboradores, tanto nas tratativas dentro de um contexto profissional com clientes e fornecedores, quanto nas atividades relacionadas ao seu segmento de atuação. ';
$define['compliance-infos_compliance-descricao-parte2'] = 'Conheça nossas políticas de compliance.';

$define['compliance-texto_arquivos-introducao-principios'] = 'INTRODUÇÃO E EXPLICAÇÃO DOS PRINCÍPIOS';
$define['compliance-texto_arquivos-introducao-principios-Kbps'] = 'PDF (154kb)';
$define['compliance-texto_arquivos-codigo-de-etica'] = 'CÓDIGO DE ÉTICA E CONDUTA';
$define['compliance-texto_arquivos-codigo-de-etica-Kbps'] = 'PDF (382kb)';
$define['compliance-texto_arquivos-anticorrupcao'] = 'POLÍTICA ANTICORRUPÇÃO';
$define['compliance-texto_arquivos-anticorrupcao-Kbps'] = 'PDF (427kb)';
$define['compliance-texto_arquivos-conflito-interesses'] = 'POLÍTICA DE CONFLITO DE INTERESSES';
$define['compliance-texto_arquivos-conflito-interesses-Kbps'] = 'PDF (580kb)';
$define['compliance-texto_arquivos-politicas-brindes'] = 'POLÍTICA SOBRE BRINDES, PRESENTES, VIAGENS E HOSPITALIDADES';
$define['compliance-texto_arquivos-politicas-brindes-Kbps'] = 'PDF (451kb)';
$define['compliance-texto_arquivos-contratacao-terceiros'] = 'POLÍTICA DE CONTRATAÇÃO DE TERCEIROS';
$define['compliance-texto_arquivos-contratacao-terceiros-Kbps'] = 'PDF (948kb)';
$define['compliance-texto_arquivos-doacoes'] = 'POLÍTICA DE DOAÇÕES E PATROCÍNIOS';
$define['compliance-texto_arquivos-doacoes-Kbps'] = 'PDF (355kb)';
$define['compliance-texto_arquivos-privacidade'] = 'POLÍTICA DE PRIVACIDADE';
$define['compliance-texto_arquivos-privacidade-Kbps'] = 'PDF (210kb)';

$define['compliance-infos_compliance-footer-descriacao'] = 'Entre em contato com o setor de compliance para mais informações:';
$define['compliance-infos_compliance-footer-descriacao-email'] = 'compliance@hersa.com.br';

// ===*Contato*===
$define['contato-#contato-infos-text_contato'] = 'Fale Conosco';
$define['contato-#contato-infos-telefone'] = '55 11 2603 4178';
$define['contato-#contato-infos-email'] = 'contato@hersa.com.br';
$define['contato-#contato-infos-endereco'] = 'Rua Padre Raposo, 497 |  Mooca ';
$define['contato-#contato-infos-endereco-cep'] = 'CEP: 03118-000 | São Paulo/SP ';

$define['contato-#contato-form_flutuante-titulo'] = 'Utilize os campos abaixo para deixar sua mensagem, em breve retornaremos. ';
$define['contato-#contato-form-input-#nome-placeholder'] = 'Nome';
$define['contato-#contato-form-input-#telefone-placeholder'] = 'Telefone';
$define['contato-#contato-form-input-#mensagem-placeholder'] = 'Mensagem';

$define['contato-#contato-form_flutuante-label'] = 'Estou de acordo com a Política e Termos de Privacidade';
$define['contato-#contato-form_flutuante-button'] = 'Enviar';

$define['contato-trabalhe_conosco_top-bg-titulo-strong'] = 'Carreiras';
$define['contato-form_trabalhe_conosco-descricao'] = 'Preencha o formulário abaixo e cadastre-se para uma nova chance de evoluir sua ';
$define['contato-form_trabalhe_conosco-descricao-strong'] = 'carreira profissional';
$define['contato-form-input-#nome-placeholder'] = 'Nome';
$define['contato-form-input-#email-placeholder'] = 'E-mail';
$define['contato-form-input-#telefone-placeholder'] = 'Telefone';
$define['contato-form-botoes-label'] = 'Anexar currículo';
$define['contato-form-botoes-label-small'] = '*Formato pdf ou word';
$define['contato-form-botoes-enviar_form'] = 'Enviar';


// ===*Home*===
	$define['home-banner-titulo'] = ' Construindo';
	$define['home-banner-titulo-space-2'] = 'soluções para empresas há mais de ';
	$define['home-banner-titulo-space-3'] = '25 anos';
	$define['home-banner-descricao'] = 'Contamos com profissionais diferenciados e uma equipe técnica focada na qualidade e excelência na execução de obras e projetos de engenharia.';

	$define['home-home_2-titulo'] = 'ÁREAS DE ATUAÇÃO';
	$define['home-home_2-areas_atuacao-descricao-civil '] = 'Civil';
	$define['home-home_2-areas_atuacao-descricao-eletrica '] = 'elétrica';
	$define['home-home_2-areas_atuacao-descricao-hidraulica '] = 'hidráulica';
	$define['home-home_2-areas_atuacao-descricao-mecanica '] = 'mecânica';
	$define['home-home_2-areas_atuacao-descricao-sistemas '] = 'sistemas';
	$define['home-home_2-ahref '] = 'Conheça nossas obras';

	$define['home-home_projetos-banner-titulo'] = 'Acompanhe os nossos principais';
	$define['home-home_projetos-banner-titulo-strong '] = 'PROJETOS';
	$define['home-home_projetos-banner-descricao_top '] = 'Acompanhe os principais projetos desenvolvidos pela Hersa Engenharia';
	$define['home-home_projetos-intro-titulo '] = 'Acompanhe os nossos principais';
	$define['home-home_projetos-intro-titulo-strong '] = 'projetos';
	$define['home-home_projetos-intro-descricao_top'] = 'Acompanhe os principais projetos desenvolvidos pela Hersa Engenharia';
	$define['home-home_projetos-info-VerTodos'] = 'Ver todos';

	$define['home-home_clientes-titulo'] = 'PRINCIPAIS CLIENTES';
	$define['home-home_clientes-descricao '] = 'Temos entre os nossos principais clientes algumas das maiores e mais importantes empresas do Brasil';

	$define['home-home_noticias-titulo '] = 'NOTÍCIAS';
	$define['home-home_noticias-descricao '] = 'Fique por dentro de tudo que acontece na Hersa Engenharia
	';
	$define['home-home_noticias-ler_mais'] = 'Ler mais +';
	$define['home-home_noticias-ir_noticias'] = 'Mais notícias';



// ===*Index *===

$define['index-header-menu-home'] = 'Home';
$define['index-header-menu-institucional'] = 'Institucional';
$define['index-header-menu-areas-atuacao'] = 'Áreas de atuação';
$define['index-header-menu-projetos'] = 'Projetos';
$define['index-header-menu-clientes'] = 'Clientes';
$define['index-header-menu-qsms'] = 'QSMS';
$define['index-header-menu-noticias'] = 'Notícias';
$define['index-header-menu-compliance'] = 'Compliance';
$define['index-header-menu-contato'] = 'Contato';


$define['index-header-menu-hamburguer-home'] = 'Home';
$define['index-header-menu-hamburguer-institucional'] = 'Institucional';
$define['index-header-menu-hamburguer-areas-atuacao'] = 'Áreas de atuação';
$define['index-header-menu-hamburguer-projetos'] = 'Projetos';
$define['index-header-menu-hamburguer-clientes'] = 'Clientes';
$define['index-header-menu-hamburguer-qsms'] = 'QSMS';
$define['index-header-menu-hamburguer-noticias'] = 'Notícias';
$define['index-header-menu-hamburguer-compliance'] = 'Compliance';
$define['index-header-menu-hamburguer-contato'] = 'Contato';

$define['index-section-lvl1-home'] = 'Home';
$define['index-section-lvl1-institucional'] = 'Institucional';
$define['index-section-lvl1-areas-atuacao'] = 'Áreas de atuação';
$define['index-section-lvl1-obras'] = 'Obras';
$define['index-section-lvl1-concessoes'] = 'Concessões';
$define['index-section-lvl1-clientes'] = 'Clientes';
$define['index-section-lvl1-qsms'] = 'QSMS';
$define['index-section-lvl1-noticias'] = 'Notícias';
$define['index-section-lvl1-trabalhe-conosco'] = 'Trabalhe Conosco';
$define['index-section-lvl1-compliance'] = 'Compliance';
$define['index-section-lvl1-contato'] = 'Contato';

$define['index-section-lvl2-title-#'] = 'Áreas de atuação';
$define['index-section-lvl2-subtitle-#eng-civil'] = 'Engenharia Civil';
$define['index-section-lvl2-description-#eng-civil'] = 'Construções e reformas prediais, comerciais e industriais, retrofit, bases e estruturas para equipamentos e infraestrutura underground.';


$define['index-section-lvl2-subtitle-#eng-ele'] = 'Engenharia Elétrica';
$define['index-section-lvl2-description-#eng-ele'] = 'Subestações Transformadoras de Energia, Sistemas de Geração de Energia, Instalações Elétricas em Média e Baixa Tensão, Sistemas de Eficiência Energética, Iluminação, Aterramento e Proteção contra Descargas Atmosférica – SPDA.';
$define['index-section-lvl2-subtitle-#eng-hid'] = 'Engenharia Hidráulica';
$define['index-section-lvl2-description-#eng-hid'] = 'Redes de água potável, pluvial e quente, centrais de água gelada (CAG), esgoto sanitário, sistemas de combate à incêndio e gás combustível.';
$define['index-section-lvl2-subtitle-#eng-mec'] = 'Engenharia Mecânica';
$define['index-section-lvl2-description-#eng-mec'] = 'Centrais e tubulações de fluidos, gases especiais e ar comprimido, sistemas de tratamento de efluentes industriais e sistemas de ventilação e climatização.';
$define['index-section-lvl2-subtitle-#sis_esp'] = 'Sistemas Especiais';
$define['index-section-lvl2-description-#sis_esp'] = 'Sistemas especiais de missão crítica, automação e supervisão predial, detecção, alarme e combate a incêndios, controle de acesso, CFTV, sonorização, telecomunicações, dados e imagem e cabeamento estruturado.';


$define['index-modal-cookies-p'] = 'Para melhorar a sua experiência de navegação, utilizamos de cookies, entre outras tecnologias. De acordo com a nossa
Política de Privacidade, ao continuar navegando, você aceita estas condições. Acesse nossa ';
$define['index-modal-cookies-ahref'] = 'Política de Privacidade';
$define['index-modal-cookies-p2'] = 'e confira como tratamos os dados pessoais na Hersa.';
$define['index-modal-cookies-button'] = 'Aceitar';



$define['index-footer-span'] = 'Somos uma empresa multidisciplinar que atua nos segmentos de Engenharia Elétrica, Civil, Mecânica, Hidráulica e Sistemas Especiais, formada por profissionais com grande capacidade e experiência nas atividades de gerenciamento e execução de obras públicas e privadas.';


$define['index-footer-menu-menu'] = 'Menu';
$define['index-footer-menu-institucional'] = 'Institucional';
$define['index-footer-menu-areas-atuacao'] = 'Áreas de atuação';
$define['index-footer-menu-projetos'] = 'Projetos';
$define['index-footer-menu-clientes'] = 'Clientes';
$define['index-footer-menu-qsms'] = 'QSMS';
$define['index-footer-menu-noticias'] = 'Notícias';
$define['index-footer-menu-compliance'] = 'Compliance';
$define['index-footer-menu-contato'] = 'Contato';


$define['index-footer-contatos_footer-h3'] = 'Contatos';
$define['index-footer-contatos_footer-span-email'] = 'contato@hersa.com.br';
$define['index-footer-contatos_footer-span-telefone'] = '55 11 2603 4178';
$define['index-footer-contatos_footer-span-rua'] = 'Rua Padre Raposo, 497 ';
$define['index-footer-contatos_footer-span-bairro'] = 'Mooca | CEP 03118-000 ';
$define['index-footer-contatos_footer-span-cidade'] = 'São Paulo/SP';


$define['index-footer-redes_sociais-h3'] = 'Redes sociais';
$define['index-footer-rodape_bottom-Pol-Priv'] = 'Política de privacidade';


// ===*Noticias *===


$define['noticias-home_noticias-titulo-linha1'] = 'Fique por dentro de tudo';
$define['noticias-home_noticias-titulo-linha2'] = 'que acontece na';
$define['noticias-home_noticias-titulo-linha2-strong'] = 'Hersa Engenharia';


// ===*Noticias Interno*===
$define['noticias_interno_text-h3'] = 'Inauguração da Nova Subestação de Energia Elétrica da Base Aérea 
de Santa Maria/RS';

$define['noticias_interno_text-p-1'] = 'Inauguração da Nova Subestação de Energia Elétrica da Base Aérea de Santa Maria/RS';
$define['noticias_interno_text-p-2'] = 'A Hersa concluiu este mês a obra de modernização da rede aérea, cabine de medição, usina de geração de energia e KF da Base Aérea de Santa Maria/RS.';
$define['noticias_interno_text-p-3'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.';
$define['noticias_interno_text-p-4'] = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.';


$define['noticias_interno-#projetos_recomendados-titulo'] = 'Projetos que você pode gostar';

// ===*Obras *===

$define['obras-introducao-titulo'] = 'Conheça';
$define['obras-introducao-titulo-strong'] = 'nossas obras e concessões';
$define['obras-introducao-titulo-parte2'] = 'em andamento';
$define['obras-introducao-descricao'] = 'utilize o filtro ao lado';
$define['obras-filtros-intro'] = 'Filtrar por:';
$define['obras-campos'] = 'Categorias:';

$define['obras-campos-filtro-concessoes'] = 'Concessões';
$define['obras-campos-filtro-Data_Center'] = 'Datacenter e Missão Crítica';
$define['obras-campos-filtro-Defesa-Seg'] = 'Defesa e Segurança';
$define['obras-campos-filtro-Diversos'] = 'Diversos';
$define['obras-campos-filtro-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['obras-campos-filtro-Manutencao'] = 'Manutenção e Facilities';
$define['obras-campos-filtro-Transportes'] = 'Transportes';
$define['obras-campos-filtro_status'] = 'Status';
$define['obras-campos-filtro_status-top'] = 'Obra Concluída';
$define['obras-campos-filtro_status-bottom'] = 'Obra em Andamento';
$define['obras-campos-filtro_cliente'] = 'Cliente';

$define['obras-obras_projeto-infos-titulo'] = 'AC CAMARGO CÂNCER CENTER';
$define['obras-obras_projeto-infos-descricao'] = 'Retrofit do Edifício Castro Alves em São Paulo/SP visando a implantação de uma nova
unidade de atendimento do AC Camargo Câncer Center. Área Total de 5.105,00 m²';

$define['obras-obras_projeto-infos-titulo-first'] = 'BANRISUL – DATACENTER';
$define['obras-obras_projeto-infos-descricao-first'] = 'Construção do Edifício Data Center do Banrisul na cidade de Porto Alegre/RS incluindo a
execução de obras civis, elétricas, mecânicas, lógicas, segurança e automação, equipamentos e PPCI.';
$define['obras-obras_projeto-infos-titulo-second'] = 'SENAI – Edif. Theobaldo de Nigris';
$define['obras-obras_projeto-infos-descricao-second'] = 'Reforma e ampliação do Edifício Theobaldo de Nigris do SENAI/SP, localizado na Av.
Paulista, 750 . Área Total: 29.186,00 m²';
$define['obras-obras_projeto-infos-titulo-third'] = 'Banco do Brasil – Datacenter Complexo Verbo Divino';
$define['obras-obras_projeto-infos-descricao-third'] = 'Modernização do Datacenter do Complexo Verbo Divino do Banco do Brasil. Sistema de
Geração: 6.000 kVA Sistema de UPS/No break: 2.0000 kVA Área: 2.500,00 m²';


$define['obras-obras_projeto-verMais'] = 'Ver mais';

// ===*Obras Interno*===

$define['obras_interno-introducao-titulo'] = 'Conheça';
$define['obras_interno-introducao-titulo-strong'] = 'nossas _interno e concessões';
$define['obras_interno-introducao-titulo-parte2'] = 'em andamento';
$define['obras_interno-introducao-descricao'] = 'utilize o filtro ao lado';
$define['obras_interno-filtros-intro'] = 'Filtrar por:';
$define['obras_interno-campos'] = 'Categoria';

$define['obras_interno-campos-filtro-concessoes'] = 'Concessões';
$define['obras_interno-campos-filtro-Data_Center'] = 'Datacenter e Missão Crítica';
$define['obras_interno-campos-filtro-Defesa-Seg'] = 'Defesa e Segurança';
$define['obras_interno-campos-filtro-Diversos'] = 'Diversos';
$define['obras_interno-campos-filtro-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['obras_interno-campos-filtro-Manutencao'] = 'Manutenção e Facilities';
$define['obras_interno-campos-filtro-Transportes'] = 'Transportes';

$define['obras_interno-campos-filtro_status'] = 'Status';
$define['obras_interno-campos-filtro_status-top'] = 'Obra Concluída';
$define['obras_interno-campos-filtro_status-bottom'] = 'Obra em Andamento';


$define['obras_interno-campos-filtro_cliente'] = 'Cliente';


$define['obras_interno-#obras_interno-banner-info-titulo'] = 'Banco do Brasil – Datacenter Complexo Verbo Divino';
$define['obras_interno-#obras_interno-banner-info-descricao'] = 'Modernização do Datacenter do Complexo Verbo Divino do Banco do Brasil. <br>Sistema de
Geração: 6.000 kVA ';
$define['obras_interno-#obras_interno-banner-info-descricao-parte-2'] = 'Sistema de UPS/No break: 2.0000 kVA';
$define['obras_interno-#obras_interno-banner-info-descricao-parte-3'] = 'Área: 2.500,00 m²';

$define['obras_interno-#obras_interno-informacoes_obra-status-top'] = 'Status:';
$define['obras_interno-#obras_interno-informacoes_obra-status-bottom'] = 'Obra em Andamento';
$define['obras_interno-#obras_interno-informacoes_obra-categoria-top'] = 'Categorias:';
$define['obras_interno-#obras_interno-informacoes_obra-categoria-bottom'] = 'Datacenter e Missão Crítica';

$define['obras_interno-#projetos_recomendados-titulo'] = 'Projetos que você pode gostar';
$define['obras_interno-#projetos_recomendados-recomendados-subtitulo'] = 'Ministério do Esporte';
$define['obras_interno-#projetos_recomendados-recomendados-descricao'] = 'Reforma, Construção, Operação e Manutenção das Instalações do Centro de
Treinamento...';

$define['obras_interno-#projetos_recomendados-projeto-subtitulo'] = 'CISCEA – Modernização de Sistemas de Energia e Climatização';
$define['obras_interno-#projetos_recomendados-projeto-descricao'] = 'Reforma e Modernização dos Sistemas Elétricos e de Climatização em 09
Destacamentos...';

$define['obras_interno-#projetos_recomendados-projeto2-subtitulo'] = 'Marinha – COGESN';
$define['obras_interno-#projetos_recomendados-projeto2-descricao'] = 'Construção dos edifícios de escritórios de projetos com 5 pavimentos e prédio
anexo para paióis e...';

$define['obras_interno-#projetos_recomendados-projeto3-subtitulo'] = 'CNH – Centro de Distribuição Sorocaba';
$define['obras_interno-#projetos_recomendados-projeto3-descricao'] = 'Construção do Prédio da Subestação (SUB2) do Laboratório de Geração de
Energia...';

// ===*Projetos Internos*===

$define['projetos_internos-projetos_nav-DataCenter'] = 'Data Center';
$define['projetos_internos-projetos_nav-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['projetos_internos-projetos_nav--Defesa-Seguranca'] = 'Defesa e Segurança';
$define['projetos_internos-projetos_nav-Transportes'] = 'Transportes';
$define['projetos_internos-projetos_nav-Manutencao'] = 'Manutenção e Facilities';
$define['projetos_internos-projetos_nav-Diversos'] = 'Diversos';
$define['projetos_internos-projetos_nav-Concessoes'] = 'Concessões';

$define['projetos_internos-select-options-DataCenter'] = 'Data Center';
$define['projetos_internos-select-options-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['projetos_internos-select-options--Defesa-Seguranca'] = 'Defesa e Segurança';
$define['projetos_internos-select-options-Transportes'] = 'Transportes';
$define['projetos_internos-select-options-Manutencao'] = 'Manutenção e Facilities';
$define['projetos_internos-select-options-Diversos'] = 'Diversos';
$define['projetos_internos-select-options-Concessoes'] = 'Concessões';
$define['projetos_internos-selected-filtro'] = 'Filtrar por';

$define['projetos_internos-#projetos_recomendados-titulo'] = 'Projetos que você pode gostar';

$define['projetos_internos-#projetos_recomendados-status'] = 'Status:';
$define['projetos_internos-#projetos_recomendados-categoria'] = 'Categorias:';
$define['projetos-interno-galeria'] = 'Confira nossa galeria de fotos';

// ===*Projetos*===

$define['projetos-projetos_nav-DataCenter'] = 'Data Center';
$define['projetos-projetos_nav-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['projetos-projetos_nav--Defesa-Seguranca'] = 'Defesa e Segurança';
$define['projetos-projetos_nav-Transportes'] = 'Transportes';
$define['projetos-projetos_nav-Manutencao'] = 'Manutenção e Facilities';
$define['projetos-projetos_nav-Diversos'] = 'Diversos';
$define['projetos-projetos_nav-Concessoes'] = 'Concessões';

$define['projetos-projetos_wrapper-text_projetos'] = 'Conheça';
$define['projetos-projetos_wrapper-text_projetos-strong'] = 'nossos projetos';

$define['projetos-select-options-DataCenter'] = 'Data Center';
$define['projetos-select-options-Hospitais'] = 'Hospitais e Instituições de Saúde';
$define['projetos-select-options--Defesa-Seguranca'] = 'Defesa e Segurança';
$define['projetos-select-options-Transportes'] = 'Transportes';
$define['projetos-select-options-Manutencao'] = 'Manutenção e Facilities';
$define['projetos-select-options-Diversos'] = 'Diversos';
$define['projetos-select-options-Concessoes'] = 'Concessões';
$define['projetos-selected-filtro'] = 'Filtrar por';

$define['projetos-#projetos_recomendados-titulo'] = 'Projetos que você pode gostar';
$define['projetos-verMais'] = 'Ver mais';


// ===*QSMS*===
$define['qsms-a_hersa_engenharia-title'] = 'Qualidade, Segurança do Trabalho,';
$define['qsms-a_hersa_engenharia-title-line2'] = 'Meio Ambiente e Saúde Ocupacional';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-1'] = 'Qualidade';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-1'] = 'Somos uma empresa comprometida com a qualidade, por isso seguimos na busca da
melhoria contínua desde a certificação inicial da série ISO 9001, realizada em 2004. Atualmente
contamos com um sistema de gestão de qualidade, certificado também na norma ISO 9001:2015.
Enxuto, prático e eficaz, garante que todas as entregas de produtos finais estejam de acordo
com os requisitos aplicáveis, sempre com qualidade, segurança e oferecendo o melhor aos nossos
colaboradores e clientes.';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-2'] = 'Segurança do Trabalho';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-2'] = 'Nossa gestão é alicerçada em ações preventivas continuamente verificadas,
utilizando-se dos métodos mais atuais existentes. Somos uma empresa membro do VISION ZERO e
adotamos abordagens do método desde 2019. Integrando disciplinas de QSMS, garantimos projetos
padronizados e controlados, sem desvios relevantes. Por conta do dinamismo que projetos de
engenharia requerem, utilizamos métodos ágeis de gestão, como o SCRUM, garantindo a celeridade
e foco nos processos, buscando sempre a satisfação de nossos clientes.';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-3'] = 'Meio Ambiente';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-3'] = 'Estamos cientes dos impactos ambientais de nossas operações, portanto,
traçamos ações de modo a reduzi-los ao máximo, com o levantamento e monitoramento dos aspectos
e impactos ambientais envolvidos em cada projeto, destinação adequada dos resíduos gerados,
plano de contingência para emergências ambientais e, assim, garantimos a execução de projetos
sustentáveis. Contamos com experiência na participação de projetos com certificação LEED -
Leadership in Energy and Environmental Design.';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-4'] = 'Saúde Ocupacional';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-4'] = 'Temos grande comprometimento com a saúde ocupacional de nossos colaboradores,
portanto buscamos oferecer um ambiente sadio através de uma política de saúde ocupacional
voltada ao controle e mitigação de riscos ocupacionais, garantindo um ambiente de trabalho
saudável, sem a prevalência de doenças ocupacionais. Acreditamos que a garantia de um ambiente
saudável a todos proporciona uma escalada ainda maior de nossa produtividade.';

$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-titulo-5'] = 'Métodos Ágeis e Inovação';
$define['qsms-qsms_wrapper-obras_prjetos-projeto-info-descricao-5'] = 'Com gestão orientada a processos e pessoas, na vanguarda do setor, utilizamos
métodos ágeis na gestão de QSMS. Estabelecemos nossas estratégias com uso do OKR – Objective
anda Key Results (Objetivos e Resultados Chave) desdobrando as metas estratégicas a todos os
envolvidos, acompanhamos o desdobramento das tarefas via Kanban Eletrônico. O SCRUM garante o
fluxo contínuo ao processo. Esses métodos garantem um sistema de gestão robusto, enxuto e
eficaz.';

// ===*Sobre*===
$define['sobre-wrapper_a_hersa_engenharia-up_title'] = '25 ANOS DE QUALIDADE, SERIEDADE E PROFISSIONALISMO ';
$define['sobre-wrapper_a_hersa_engenharia-titulo'] = 'Hersa Engenharia';
$define['sobre-wrapper_a_hersa_engenharia-descricao-1p'] = 'A Hersa Engenharia foi fundada em 1996 no tradicional bairro da Mooca, em São Paulo, e hoje acumula mais de 800 projetos e obras concluídas em todo território nacional.';
$define['sobre-wrapper_a_hersa_engenharia-descricao-2p'] = 'Somos uma empresa multidisciplinar que atua nos segmentos de Engenharia Elétrica, Civil, Mecânica, Hidráulica e Sistemas Especiais, formada por profissionais com grande capacidade e experiência nas atividades de gerenciamento e execução de obras públicas e privadas.';
$define['sobre-wrapper_a_hersa_engenharia-descricao-3p'] = 'Temos como principal característica a excelência na entrega de nossas obras através de um rigoroso padrão de qualidade, sendo capazes de realizar projetos de alta complexidade técnica. Ao longo de 25 anos de existência, a Hersa Engenharia tornou-se uma das mais admiradas empresas do Brasil.';
$define['sobre-a_hersa_saiba_mais-intro-descricao'] = 'Saiba mais sobre a ';
$define['sobre-a_hersa_saiba_mais-intro-descricao-strong'] = 'Hersa Engenharia';
$define['sobre-a_hersa_saiba_mais-intro-descricao-part2'] = 'assistindo nosso vídeo institucional';

$define['sobre-a_hersa_colaboradores-titulo'] = 'NOSSA EQUIPE';
$define['sobre-a_hersa_colaboradores-colaboradores-number'] = '600';
$define['sobre-a_hersa_colaboradores-colaboradores-span'] = 'colaboradores';

$define['sobre-missao_visao_valores-missao-titulo'] = 'Missão';
$define['sobre-missao_visao_valores-missao-descricao'] = 'Oferecer aos clientes soluções de engenharia, transformando suas necessidades em obras eficazes, executadas e dirigidas por pessoas capacitadas e responsáveis, garantindo assim qualidade e excelência na prestação de serviços. ';

$define['sobre-missao_visao_valores-visao-titulo'] = 'Visão';
$define['sobre-missao_visao_valores-visao-descricao'] = 'Ser referência de qualidade e responsabilidade no mercado de prestação de serviços em obras de engenharia. Chegaremos lá com:';
$define['sobre-missao_visao_valores-visao-descricao-linha-span-1'] = '• Equipe motivada, comprometida e focada em resultados;';
$define['sobre-missao_visao_valores-visao-descricao-linha-span-2'] = '• Política interna e cultura organizacional fortes;';
$define['sobre-missao_visao_valores-visao-descricao-linha-span-3'] = '• Investindo na busca dos melhores profissionais.';


$define['sobre-missao_visao_valores-valores-titulo'] = 'Valores';
$define['sobre-missao_visao_valores-valores-descricao-1'] = '• Comprometimento';
$define['sobre-missao_visao_valores-valores-descricao-2'] = '• Profissionalismo';
$define['sobre-missao_visao_valores-valores-descricao-3'] = '• Iniciativa';
$define['sobre-missao_visao_valores-valores-descricao-4'] = '• Responsabilidade';
$define['sobre-missao_visao_valores-valores-descricao-5'] = '• Ética e transparência';


// ===*Trabalhe Conosco*===

$define['trabalhe-conosco-trabalhe_conosco_top-bg-titulo-strong'] = 'Trabalhe conosco';
$define['trabalhe-conosco-form_trabalhe_conosco-descricao'] = 'Preencha o formulário abaixo e cadastre-se para uma nova chance de evoluir sua ';
$define['trabalhe-conosco-form_trabalhe_conosco-descricao-strong'] = 'carreira profissional';
$define['trabalhe-conosco-form-input-#nome-placeholder'] = 'Nome';
$define['trabalhe-conosco-form-input-#email-placeholder'] = 'E-mail';
$define['trabalhe-conosco-form-input-#telefone-placeholder'] = 'Telefone';
$define['trabalhe-conosco-form-botoes-label'] = 'Anexar currículo';
$define['trabalhe-conosco-form-botoes-label-small'] = '*Formato pdf ou word';
$define['trabalhe-conosco-form-botoes-enviar_form'] = 'Enviar';










