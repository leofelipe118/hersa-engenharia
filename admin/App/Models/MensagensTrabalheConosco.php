<?php
namespace App\Models;


use CRO\Model\Table;

class MensagensTrabalheConosco extends Table
{
    protected $table = "msgtrabalheconosco";

    public function busca_msg($clausula)
    {
        //está na listagem de contatos - altera o status de exibição
        $this->tudoLido();

        $query = "SELECT a.* 
                  FROM {$this->table} AS a 
                  WHERE 1 {$clausula} 
                  ORDER BY a.data DESC";
        return $this->db->query($query);

    }

    public function tudoLido()
    {
        $sql = "UPDATE {$this->table} SET alerta = 'n' WHERE 1";
        $this->db->query($sql);
        return false;
    }

    public function busca_parceiros($clausula)
    {
        $query = "SELECT * FROM parceiros WHERE 1 {$clausula} ORDER BY dt_cadastro DESC";
        return $this->db->query($query);
    }

    public function contPontos($clausula)
    {
        $query = "SELECT COUNT(a.id) AS total, a.ponto_postagem 
            FROM {$this->table} AS a 
            LEFT JOIN pontos AS b ON a.ponto_postagem = b.sigla 
            WHERE 1 {$clausula} 
            GROUP BY a.ponto_postagem 
            ORDER BY a.ponto_postagem";
        return $this->db->query($query);
    }

    public function contMes($clausula)
    {
        $query = "SELECT COUNT(id) AS total, DATE_FORMAT(data, '%d/%m') AS dia FROM {$this->table} WHERE 1 {$clausula} GROUP BY dia ORDER BY dia";
        $resul = $this->db->query($query);
        $array = array();
        foreach ($resul as $r)
            $array[ $r['dia'] ] = $r;

        return $array;
    }

    public function contEstado($clausula)
    {
        $query = "SELECT COUNT(id) AS total, estado FROM {$this->table} WHERE 1 {$clausula} GROUP BY estado ORDER BY estado";
        return $this->db->query($query);
    }

    public function importa( $data )
    {
        /*
        Estrutura do arquivo
        [1] => PARCEIRO
        [2] => CNPJ
        [3] => DT_CADASTRO
        [4] => Endereco
        [5] => Numero
        [6] => Bairro
        [7] => Cidade
        [8] => UF
        [9] => Cep
        [10] => CONTATO
        [11] => TELEFONE
        [12] => EMAIL
        [13] => CONTATO_FINANCEIRO
        [14] => TEL_FINANCEIRO
        [15] => EMAIL_FINANCEIRO
        [16] => Contato_Operacional
        [17] => Telefone_Operacional
        [18] => Email_Operacional
        [19] => CONTATO_COMERCIAL
        [20] => TEl_COMERCIAL
        [21] => EMAIL_COMERCIAL
        [22] => EMAIL_TI
        [23] => SITE_LOJA
        [24] => Situacao_Parceiro
        [25] => Ramo Atividade
        */

        $arr_campos = array(
            1 => "parceiro-string",
            2 => "cnpj-string",
            3 => "dt_cadastro-date",
            4 => "endereco-string",
            5 => "numero-string",
            6 => "bairro-string",
            7 => "cidade-string",
            8 => "uf-string",
            9 => "cep-string",
            10 => "contato-string",
            11 => "telefone-string",
            12 => "email-string",
            13 => "contato_financeiro-string",
            14 => "tel_financeiro-string",
            15 => "email_financeiro-string",
            16 => "contato_operacional-string",
            17 => "telefone_operacional-string",
            18 => "email_operacional-string",
            19 => "contato_comercial-string",
            20 => "tel_comercial-string",
            21 => "email_comercial-string",
            22 => "email_ti-string",
            23 => "site_loja-string",
            24 => "situacao_parceiro-string",
            25 => "ramo_atividade-string"
        );
        $log_erros = "Arquivo aberto com sucesso\r\n";

        $form = array();

        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
        {
            foreach( $data->sheets[0]['cells'][$i] as $ind => $val)
                $form[ $arr_campos[$ind] ] = utf8_encode( $val );

            $sql = $sql = executa($this->db, $form, "parceiros", "inserir", "");
            $log_erros .= "Linha " . $i . " - " . $data->sheets[0]['cells'][$i][1];
            $log_erros .= $sql ? " - Parceiro inserido com sucesso.\r\n" : " - Ocorreu um erro ao inserir o parceiro no banco de dados.\r\n";
            $cnpj = filtro( "num", $data->sheets[0]['cells'][$i][2] );
            if($sql)
            {
                $sqlup = "UPDATE {$this->table} SET integracao = 's' WHERE retornaINT(cnpj) = '".$cnpj."'";
                $this->db->query($sqlup);
            }
        }

        $nomelog = date("d-m-Y-H-i");
        $log = fopen("log/log-" . $nomelog . ".txt","w+");
        fwrite($log, $log_erros);
        fclose($log);

        return "log-" . $nomelog;
    }

}