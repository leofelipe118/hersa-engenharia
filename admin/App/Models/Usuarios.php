<?php

namespace App\Models;


use CRO\Model\Table;

class Usuarios extends Table
{
    protected $table = "usuarios";
	
    public function enviaEmail($nome, $email, $senha)
    {
        require_once "vendor/CRO/BaseAdmin/phpmailer/class.phpmailer.php";
        $mensagem = '<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
					<title>Novo Usu&aacute;rio</title>
					<link rel="stylesheet" href="">
				</head>
				<body>
					<style>
						body{
							background: #eaeced;
						}
					</style>
					<div align="center" style="text-align:center;margin-left:auto;margin-right:auto; width: 580px;">
						<table style="width: 580px;" border="0">
							<tr>
								<td valign="center" style="height: 192px; text-align:center;">
									<h2 style="font-family: arial; font-weight: 300; font-size: 40px; margin:0px; padding:0px;">
										Ol&aacute; <span style="font-weight: bold;">'.filtro("string",@array_shift(@explode(" ",$nome))).'</span>!
									</h2>
									<h2 style="font-family: arial; font-weight: 300; font-size: 18px; margin:0px; padding:0px;">
										Voc&ecirc; est&aacute; cadastrado(a) no Painel Administrativo do site da Direct Log.<br />Para fazer o login utilize a senha abaixo:
									</h2>
								</td>
							</tr>
						</table>
						<table style="width: 580px; margin-bottom:5px;" border="0">
							<tr><td style="text-align: center; height: 228px; line-height: 25px; font-weigth: bold; color: #FFF; font-family: arial; padding-left: 10px; padding-right: 10px;" bgcolor="'.COR_SITE.'"><span style="font-weight: bold;font-size:50px;">'.$senha.'</span></td></tr>
						</table>
						<table style="width: 580px; margin-bottom:25px;" border="0">
							<tr>
								<td style="text-align: center; height: 155px; line-height: 25px; font-weigth: bold; color: #000; font-family: arial; padding-left: 10px; padding-right: 10px;" bgcolor="c3c3c3">
									<span style="font-weight: bold;">Estamos muito felizes em t&ecirc;-lo(a) conosco</span><br>
									<a href="'.BASE_SITE.'" style="color: #000; font-family: arial; text-decoration: none;">Clique aqui para acessar o painel administrativo</a>
								</td>
							</tr>
						</table>
						<table style="width: 580px; margin-bottom:0px; border-bottom: 1px solid #c3c3c3" border="0">
							<tr>
								<td style="text-align: center; height: 150px;" valign="center">
									<a href="'.BASE_SITE.'" target="_blank">
									    <img src="'.BASE_SITE.'App/Views/includes/imagens/logo.png">
									</a>
								</td>
							</tr>
						</table>
					</div>
				</body>
			</html>';

        $grava = fopen("novo_usuario.html","w+");
        fwrite($grava,$mensagem);
        fclose($grava);

        //Envio de email
        $Email = new \PHPMailer();
        $Email->setFrom("contato@directlog.com.br", utf8_decode( "Direct Log" ) );
        $Email->addReplyTo = "";
        $Email->AddAddress($email);
		$Email->addBcc("mateus@benettoncomunicacao.com.br");
        $Email->Subject = utf8_decode("Seja Bem Vindo - Direct Log");
        $Email->MsgHTML($mensagem);
        if( !$Email->Send() )
		{
			$grava = fopen("logerro.html","w+");
			fwrite($grava,'Mailer Error: ' . $Email->ErrorInfo);
			fclose($grava);
		}

        return true;

    }

}